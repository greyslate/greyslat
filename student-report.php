
<?php

include("includes/config-frontend.php");

$objPHPExcel = new PHPExcel();
$cobj = new Student();
$where = " And SM.school_id = '" . $_SESSION['institute_id'] . "' ";
$_POST['task'] = "search";

if (isset($_POST['task']) && $_POST['task'] = "search") {


    if ($_SESSION['medium'] ) {
        $where .= " And SM.medium = '" . $_SESSION['medium'] . "'";
    }
    if ($_SESSION['grade_id']) {
        $where .= " And SM.grade_id = '" . $_SESSION['grade_id'] . "'";
    }
    if ($_SESSION['section_id']) {
        $where .= " And SM.section_id = '" . $_SESSION['section_id'] . "'";
    }


}
$data = $cobj->fetch_all($where);
$i = 0;
$main_arr = array();
if(count($data) > 0){
    foreach($data as $val){
        $main_arr[$i]["roll_no"]      = $val->roll_no;
        $main_arr[$i]["name"]     = $val->fname." " .$val->lname;
        $main_arr[$i]["profile_pic"]      = $val->profile_pic;
        $main_arr[$i]["p_email"]      = $val->p_email;
        $main_arr[$i]["s_email"]      = $val->s_email;
        $main_arr[$i]["address1"]      = $val->address_line1;
        $main_arr[$i]["address2"]      = $val->address_line2;
        $main_arr[$i]["city"]      = $val->city;
        $main_arr[$i]["state"]      = $val->state;
        $main_arr[$i]["p_contact"]      = $val->p_contact;
        $main_arr[$i]["s_contact"]      = $val->s_contact;
        $main_arr[$i]["gender"]      = $val->gender;
        $main_arr[$i]["dob"]    = $val->dob;
        $main_arr[$i]["created_on"] = $val->created_on;
        $main_arr[$i]["updated_on"]      = $val->updated_on;
        $main_arr[$i]["medium"]      = $val->medium;
        $main_arr[$i]["grade_id"]      = $val->standard_name;
        $main_arr[$i]["section_id"]      = $val->section_name;
        $i++;
    }
}


$dataCount = 1;
$objPHPExcel->getActiveSheet()->SetCellValue('A'.$dataCount, "Roll number");
$objPHPExcel->getActiveSheet()->SetCellValue('B'.$dataCount, "Name");
$objPHPExcel->getActiveSheet()->SetCellValue('C'.$dataCount, "Profile pic");
$objPHPExcel->getActiveSheet()->SetCellValue('D'.$dataCount, "Parent email");
$objPHPExcel->getActiveSheet()->SetCellValue('E'.$dataCount, "Studentemail");
$objPHPExcel->getActiveSheet()->SetCellValue('F'.$dataCount, "Address Line 1");
$objPHPExcel->getActiveSheet()->SetCellValue('G'.$dataCount, "Address Line 2");
$objPHPExcel->getActiveSheet()->SetCellValue('H'.$dataCount, "City");
$objPHPExcel->getActiveSheet()->SetCellValue('I'.$dataCount, "State");
$objPHPExcel->getActiveSheet()->SetCellValue('J'.$dataCount, "Parent contact");
$objPHPExcel->getActiveSheet()->SetCellValue('K'.$dataCount, "Student contact");
$objPHPExcel->getActiveSheet()->SetCellValue('L'.$dataCount, "Gender");
$objPHPExcel->getActiveSheet()->SetCellValue('M'.$dataCount, "DOB");
$objPHPExcel->getActiveSheet()->SetCellValue('N'.$dataCount, "Standard");
$objPHPExcel->getActiveSheet()->SetCellValue('O'.$dataCount, "Section");
$objPHPExcel->getActiveSheet()->SetCellValue('P'.$dataCount, "Medium");
$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$dataCount, "Created On");
$objPHPExcel->getActiveSheet()->SetCellValue('R'.$dataCount, "Updated On");


$dataCount = 2;
for($i=0;$i<count($main_arr);$i++){
    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$dataCount, $main_arr[$i]['roll_no']);
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$dataCount, $main_arr[$i]['name']);
    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$dataCount, $main_arr[$i]['profile_pic']);
    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$dataCount, $main_arr[$i]['p_email']);
    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$dataCount, $main_arr[$i]['s_email']);
    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$dataCount, $main_arr[$i]['address1']);
    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$dataCount, $main_arr[$i]['address2']);
    $objPHPExcel->getActiveSheet()->SetCellValue('H'.$dataCount, $main_arr[$i]['city']);
    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$dataCount, $main_arr[$i]['state']);
    $objPHPExcel->getActiveSheet()->SetCellValue('J'.$dataCount, $main_arr[$i]['p_contact']);
    $objPHPExcel->getActiveSheet()->SetCellValue('K'.$dataCount, $main_arr[$i]['s_contact']);
    $objPHPExcel->getActiveSheet()->SetCellValue('L'.$dataCount, $main_arr[$i]['gender']);
    $objPHPExcel->getActiveSheet()->SetCellValue('M'.$dataCount, $main_arr[$i]['dob']);
    $objPHPExcel->getActiveSheet()->SetCellValue('N'.$dataCount, $main_arr[$i]['grade_id']);
    $objPHPExcel->getActiveSheet()->SetCellValue('O'.$dataCount, $main_arr[$i]['section_id']);
    $objPHPExcel->getActiveSheet()->SetCellValue('P'.$dataCount, $main_arr[$i]['medium']);

    $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$dataCount, $main_arr[$i]['created_on']);
    $objPHPExcel->getActiveSheet()->SetCellValue('R'.$dataCount, $main_arr[$i]['updated_on']);
    $dataCount++;
    //pr($objPHPExcel);
}

$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');


header('Content-Disposition: attachment;filename="student-report.xlsx"');

header('Cache-Control: max-age=0');


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

ob_end_clean();
$objWriter->save('php://output');


exit;
?>
