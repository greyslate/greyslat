<?php
include("header.php");
session_start();
$cobj = new Student();
$where = " And SM.school_id = '" . $_SESSION['institute_id'] . "' ";
$data = $cobj->fetch_all($where);
$grade_list = FillCombo($grade_obj->fetch_grade_list(), "id", "standard_name", "");
//$course_list = FillCombo($course_obj->fetch_course_list(), "id", "course_name", "");
$section_list = FillCombo($section_obj->fetch_section_list(), "id", "section_name", "");
//$_SESSION['medium'] = $_REQUEST['medium'];
$_SESSION['grade_id'] = $_REQUEST['grade_id'];
$_SESSION['section_id'] = $_REQUEST['section_id'];
$_SESSION['medium'] = $_REQUEST['medium'];
//echo $_SESSION['medium'];
?>
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box red ">
                        <div class="portlet-title">
                            <div class="caption">
                                Search
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmSearch" id="frmSearch">
                                <div class="form-body">
                                   <?php /*echo $_SESSION['medium'];*/?>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Standard</label>
                                        <div class="col-md-9">
                                            <select class="form-control required" name="grade_id" id="grade_id">
                                                <?php echo $grade_list; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Section </label>
                                        <div class="col-md-9">
                                            <select  name="section_id" id="section_id" class="form-control required select2">
                                                <?php echo $section_list; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Medium</label>
                                        <div class="col-md-9">
                                            <select class="form-control" name="medium" id="medium">
                                                <option value="">---Select---</option>
                                                <option value="English">English</option>
                                                <option value="Gujarati">Gujarati</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn green" name="btnSearch" id="btnSearch" type="submit" value="search" onclick="return Search_Data();"><i class="fa fa-check"></i> Search</button>
                                            <button type="button" type="reset" id="btnReset" class="btn default">Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN PAGE HEADER-->
                        <h3 class="page-title">
                            Manage Student <small>view student</small>
                        </h3>
                        <div class="row">
                            <form id="frmtable" name="frm" method="post"  enctype="multipart/form-data" >
                                <div class="col-md-12">
                                    <div class="right" style="float:right;">

                                        <a href="student-report.php" class="btn red">Export Data</a>
                                    </div>
                                    <br><br><br>

                                    <!-- BEGIN BORDERED TABLE PORTLET-->
                                    <div class="portlet box red">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-cogs"></i>Manage Student
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div id="page_listing">
                                                <table class="table table-bordered table-striped table-condensed flip-content" >
                                                    <thead>
                                                    <tr>
                                                        <th class="chb_col" width="1" >
                                                            <input type="checkbox" name="checkall" onclick="javascript:checkAlluncheckAll(frm);">
                                                        </th>
                                                        <th>
                                                            Roll No
                                                        </th>
                                                        <th>
                                                            Profile Pic
                                                        </th>
                                                        <th width="20%">
                                                            Student Name
                                                        </th>
                                                        <th width="5%">
                                                            Gender
                                                        </th>
                                                        <th width="10%">
                                                            DOB
                                                        </th>
                                                        <th>
                                                            Created On
                                                        </th>
                                                        <th class="center" width="5%" style="text-align: center;">
                                                            Status
                                                        </th>
                                                        <th width="10%" style="text-align: center;">
                                                            Action
                                                        </th>
                                                    </tr>
                                                    </thead>

                                                    <tbody id="content ui-sortable">

                                                    <?php
                                                    if (count($data) == 0) {
                                                        ?>
                                                        <tr><td colspan="8" align="center">No Result found</td></tr>
                                                        <?php
                                                    } else {
                                                        foreach ($data as $val) {
                                                            ?>
                                                            <tr>
                                                                <td class="chb_col">
                                                                    <input type="checkbox" name="id[]" value="<?php echo $val->id; ?>">
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->roll_no;?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    if(!empty($val->profile_pic)){
                                                                        ?>
                                                                        <img src="<?php echo STUDENT_URL.$val->profile_pic; ?>" width="100" />
                                                                        <?php
                                                                    }
                                                                    else{
                                                                        echo "Picture Not Avaialble";
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->fname . " " . $val->lname; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->gender; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->dob; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->created_on; ?>
                                                                </td>
                                                                <td id="td_status_<?php echo $val->id; ?>" style="text-align: center;">

                                                                    <a href="javascript:;" onclick="ChangeStatus('<?php echo $val->status; ?>', '<?php echo $val->roll_no; ?>')"

                                                                       title='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>'>

                                                                        <img  src='<?php echo IMAGE_URL; ?><?php
                                                                        echo ($val->status == 1 ? "ico-1.gif" : "ico-0.gif");
                                                                        ?>

                                                                              ' alt='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>'

                                                                              title='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>' />

                                                                    </a>

                                                                </td>
                                                                <td class="content_actions" style="text-align: center;">
                                                                    <a href="javascript:;" onclick="javascript:RemoveRecord('<?php echo $val->roll_no; ?>')" title='Delete'>
                                                                        <img src="<?php echo IMAGE_URL; ?>ico/trashcan_gray.png" alt="" /></a> <a href="student-edit.php?id=<?php echo $val->roll_no; ?>" class="sepV_a" title="Edit">
                                                                        <img src="<?php echo IMAGE_URL; ?>ico/pencil_gray.png" alt="" />
                                                                    </a>
                                                                    <a href="student-detail.php?id=<?php echo $val->roll_no; ?>" class="sepV_a" title="View Student Details">
                                                                        View Student Details
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                                <?php include("pagination.php"); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END BORDERED TABLE PORTLET-->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
    </div>
    <?php

    include("footer.php");
    ?>
    <script src="<?php echo JS_URL; ?>script/student.js" type="text/javascript" charset="utf-8"></script>

