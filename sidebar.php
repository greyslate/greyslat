<div class="layer-container">
	<div class="menu-layer">
		<ul>
			<li data-open-after="true" class="open">
				<a href="index.php">Dashboard</a>
			</li>
			<li class="has-child">
				<a href="javascript:;">Master Entries</a>
				<ul class="child-menu">
					<li>
						<a href="course-list.php">Manage Courses</a>
					</li>
					<li>
						<a href="grade-list.php">Manage Grade</a>
					</li>
					<li>
						<a href="section-list.php">Manage Section</a>
					</li>
				</ul>
			</li>
			<li class="has-child">
				<a href="javascript:;">Classroom</a>
				<ul class="child-menu">
					<li>
						<a href="class-list.php">Manage Classes</a>
					</li>
					<li>
						<a href="student-list.php">Manage Students</a>
					</li>
					<li>
						<a href="announcement-list.php">Manage Announcement</a>
					</li>
					<li>
						<a href="timetable-list.php">Manage Timetable</a>
					</li>
					<li>
						<a href="syllabus-list.php">Manage Syllabus</a>
					</li>
					<li>
						<a href="examtimetable-list.php">Manage Exam Timetable</a>
					</li>
				</ul>
			</li>
			<li class="has-child">
				<a href="javascript:;">Noticeboard</a>
				<ul class="child-menu">
					<li>
						<a href="event-list.php">Manage Events</a>
					</li>
					<li>
						<a href="notice-list.php">Manage Notice</a>
					</li>
					<li>
						<a href="newsletter-list.php">Manage Newsletter</a>
					</li>
					<li>
						<a href="holiday-list.php">Upload Holiday List File</a>
					</li>
				</ul>
			</li>
			<li class="has-child">
				<a href="javascript:;">Others</a>
				<ul class="child-menu">
					<li>
						<a href="appointment-list.php">Manage Appointment</a>
					</li>
					<li>
						<a href="gallery-list.php">Manage Gallery</a>
					</li>
				</ul>
			</li>
			<li class="has-child">
				<a href="javascript:;">CMS</a>
				<ul class="child-menu">
					<li>
						<a href="cms.php">CMS</a>
					</li>
					<li>
						<a href="principle-desk.php">Principal Desk</a>
					</li>
				</ul>
			</li>
			<li class="has-child">
				<a href="poll-list.php">Poll</a>
			</li>
			<li class="has-child">
				<a href="emp-list.php">Manage Employee</a>
			</li>
			<li class="has-child">
				<a href="parent-list1.php">Manage Parent</a>
			</li>
				<li class="has-child">
				<a href="leave-application-list.php">Manage Leave</a>
			</li>
			<li class="has-child">
				<a href="document-list.php">Manage Document</a>
			</li>
		</ul>
		</div>
	</div>