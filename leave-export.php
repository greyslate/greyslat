<?php

    include("includes/config-frontend.php");
    $objPHPExcel = new PHPExcel();
   
    $sql = "Select * From ".LEAVE_APPLICATION."  order by id desc";


    $db->query($sql);

    $rows = $db->fetch_object();

    $i = 0;
    $main_arr = array();
    if(count($rows) > 0){
        foreach($rows as $val){
            $main_arr[$i]["title"]     = $val->title;
            $main_arr[$i]["reason"]      = $val->description;
            $main_arr[$i]["leave_date"]   = $val->leave_date;
            $main_arr[$i]["posted_on"] = $val->posted_on;
            $i++;
        }
    }


    $rowCount = 1;
    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "Title");
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Reason for leave");
    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Leave date");

    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, "Posted On");


    $rowCount = 2;
    for($i=0;$i<count($main_arr);$i++){
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $main_arr[$i]['title']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $main_arr[$i]['reason']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $main_arr[$i]['leave_date']);

        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $main_arr[$i]['posted_on']);

        $rowCount++;
        //pr($objPHPExcel); 
    }

    header('Content-Type: application/vnd.openxmlformats-   officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.date("d-m-Y").'-leave.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

?>