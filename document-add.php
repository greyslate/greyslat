<?php
include("header.php");

?>
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Add Document <small> add document title, location,date & time</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo SITE_URL; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="event-list.php">Document</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Add Document</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="portlet box red ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Add Document
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmadd" id="frmadd">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                You have some form errors. Please check below.
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Title <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txttitle" name="txttitle" placeholder="Event Title" class="form-control required"/>
                                                </div>
                                            </div>

                                          
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Upload File<span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input type="file" id="file_name" name="file_name" placeholder="please select a file" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Uploaded By <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input id="txtuploadedby" name="txtuploadedby" class="form-control required" placeholder="School ID">
                                                </div>
                                            </div>
                                           
                                            <div class="form-group">
                                            <div class="formRow on_off_activestatus">
                                             <label class="control-label col-md-3">Status</label>
                                            <input class="form-control" name="status" value="1" type="checkbox" id="status" checked="" data-switchery="true"  />
                                         </div>
                                        </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input type="hidden" name="action" value="add">
                                                    <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
                                                    <button type="button" onclick="location.href = 'document-list.php'" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>

<?php
include("footer.php");
?>
<script src="<?php echo JS_URL; ?>script/document.js" type="text/javascript" charset="utf-8"></script>


