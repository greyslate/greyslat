<?php

    include("includes/config-frontend.php");
    $objPHPExcel = new PHPExcel();
    $sql = "Select * From ".PARENT_MASTER." order by id desc";


    $db->query($sql);

    $rows = $db->fetch_object();

    $i = 0;
    $main_arr = array();
    if(count($rows) > 0){
        foreach($rows as $val){


            $main_arr[$i]["name"]           = $val->fname." ".$val->lname;
            $main_arr[$i]["email"]          = $val->email;
            $main_arr[$i]["address"]        = $val->address;
            $main_arr[$i]["phone"]          = $val->Mobile_No;
            $main_arr[$i]["state"]          = $val->state;
            $main_arr[$i]["city"]           = $val->city;
            $main_arr[$i]["stud_name"]      = $val->stud_name;
            $main_arr[$i]["stud_roll_no"]   = $val->stud_roll_no;
            $main_arr[$i]["medium_edu"]     = $val->medium_edu;
            $main_arr[$i]["standard_name"]  = $val->standard_name;
            $main_arr[$i]["section_name"]   = $val->section_name;
            $main_arr[$i]["created_on"]     = $val->created_on;
            $i++;
        }
    }


    $rowCount = 1;
    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "Parent Name");
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Email");
    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Address");
    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, "Phone");
    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, "State");
    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "City");
    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, "Student Name");
    $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, "Student RollNo");
    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, "Medium of Education");
    $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, "Standard");
    $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, "Division");
    $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, "Created On");


    $rowCount = 2;
    for($i=0;$i<count($main_arr);$i++){
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $main_arr[$i]['name']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $main_arr[$i]['email']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $main_arr[$i]['address']);
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $main_arr[$i]['phone']);
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $main_arr[$i]["state"]);
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $main_arr[$i]["city"]);
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $main_arr[$i]["stud_name"]);
        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $main_arr[$i]["stud_roll_no"]);
        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $main_arr[$i]["medium_edu"]);
        $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $main_arr[$i]["section_name"]);
        $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $main_arr[$i]["section_name"]);
        $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $main_arr[$i]['created_on']);

        $rowCount++;
        //pr($objPHPExcel); 
    }

    header('Content-Type: application/vnd.openxmlformats-   officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.date("d-m-Y").'-parent.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

?>