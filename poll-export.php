<?php

    include("includes/config-frontend.php");
    $objPHPExcel = new PHPExcel();
   
    $sql = "Select * From ".POLL_MASTER." order by id desc";
    $db->query($sql);

    $rows = $db->fetch_object();

    $i = 0;
    $main_arr = array();
    if(count($rows) > 0){
        foreach($rows as $val){
            $main_arr[$i]["title"]     = $val->question_title;
           $main_arr[$i]["created_on"] = $val->created_on;
            $i++;
        }
    }


    $rowCount = 1;
    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "Question");
   
    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, "Created On");


    $rowCount = 2;
    for($i=0;$i<count($main_arr);$i++){
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $main_arr[$i]['title']);
       
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $main_arr[$i]['created_on']);

        $rowCount++;
        //pr($objPHPExcel); 
    }

    header('Content-Type: application/vnd.openxmlformats-   officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.date("d-m-Y").'-poll.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

?>