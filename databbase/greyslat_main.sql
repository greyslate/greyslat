-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 16, 2017 at 11:03 AM
-- Server version: 5.6.34
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `greyslat_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcement_master`
--

CREATE TABLE IF NOT EXISTS `announcement_master` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `school_id` bigint(15) NOT NULL,
  `course_id` bigint(15) NOT NULL,
  `grade_id` bigint(15) NOT NULL,
  `section_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `ann_date` date NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `appointment_master`
--

CREATE TABLE IF NOT EXISTS `appointment_master` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `school_id` bigint(15) NOT NULL,
  `student_id` bigint(15) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `app_date` date NOT NULL,
  `app_time` varchar(20) NOT NULL,
  `posted_on` datetime NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `city_master`
--

CREATE TABLE IF NOT EXISTS `city_master` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `state_id` bigint(15) NOT NULL,
  `country_id` bigint(15) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `city_master`
--

INSERT INTO `city_master` (`id`, `state_id`, `country_id`, `city_name`, `status`) VALUES
(1, 1, 1, 'Surat', 1),
(2, 1, 1, 'Ahmedabad', 1),
(3, 1, 1, 'Baroda', 1),
(4, 1, 1, 'Rajkot', 1);

-- --------------------------------------------------------

--
-- Table structure for table `class_detail`
--

CREATE TABLE IF NOT EXISTS `class_detail` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `class_id` bigint(15) NOT NULL,
  `grade_id` bigint(15) NOT NULL,
  `section_id` bigint(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `class_master`
--

CREATE TABLE IF NOT EXISTS `class_master` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) NOT NULL,
  `course_id` bigint(15) NOT NULL,
  `school_id` bigint(15) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cms_master`
--

CREATE TABLE IF NOT EXISTS `cms_master` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'unique id for cms page',
  `school_id` bigint(15) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'cms page name',
  `alias` varchar(255) NOT NULL COMMENT 'alisa to use in the page links',
  `content` text NOT NULL COMMENT 'cms page content',
  `meta_title` text NOT NULL COMMENT 'cms meta title',
  `meta_keyword` text NOT NULL COMMENT 'cms meta kyword',
  `meta_description` text NOT NULL COMMENT 'cms meta description',
  `additional_tag` text NOT NULL,
  `status` int(1) NOT NULL COMMENT '1 - active , 0 - inactive',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'cms page created date',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cms_master`
--

INSERT INTO `cms_master` (`id`, `school_id`, `name`, `alias`, `content`, `meta_title`, `meta_keyword`, `meta_description`, `additional_tag`, `status`, `created_date`) VALUES
(1, 0, 'About Us', 'about-us', '', '', '', '', '', 0, '2015-11-06 04:03:05'),
(2, 0, 'Help', 'help', '', '', '', '', '', 0, '2015-11-06 04:03:05');

-- --------------------------------------------------------

--
-- Table structure for table `country_master`
--

CREATE TABLE IF NOT EXISTS `country_master` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `country_master`
--

INSERT INTO `country_master` (`id`, `country_name`, `status`) VALUES
(1, 'India', 1);

-- --------------------------------------------------------

--
-- Table structure for table `course_master`
--

CREATE TABLE IF NOT EXISTS `course_master` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `school_id` bigint(15) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `emp_master`
--

CREATE TABLE IF NOT EXISTS `emp_master` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `role_id` bigint(15) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `event_master`
--

CREATE TABLE IF NOT EXISTS `event_master` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `school_id` bigint(15) NOT NULL,
  `event_title` varchar(255) NOT NULL,
  `event_desc` text NOT NULL,
  `event_date` date NOT NULL,
  `event_time` varchar(100) NOT NULL,
  `event_location` text NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_detail`
--

CREATE TABLE IF NOT EXISTS `gallery_detail` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `gallery_id` bigint(15) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `uploaded_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_cover_pic` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_master`
--

CREATE TABLE IF NOT EXISTS `gallery_master` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `gallery_title` varchar(255) NOT NULL,
  `gallery_desc` text NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `holiday_master`
--

CREATE TABLE IF NOT EXISTS `holiday_master` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `school_id` bigint(15) NOT NULL,
  `file_path` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `holiday_master`
--

INSERT INTO `holiday_master` (`id`, `school_id`, `file_path`, `file_name`, `updated_on`) VALUES
(1, 0, '', '', '2015-11-01 14:35:07');

-- --------------------------------------------------------

--
-- Table structure for table `leave_application`
--

CREATE TABLE IF NOT EXISTS `leave_application` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `school_id` bigint(15) NOT NULL,
  `student_id` bigint(15) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `leave_date` date NOT NULL,
  `posted_on` datetime NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_master`
--

CREATE TABLE IF NOT EXISTS `newsletter_master` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `school_id` bigint(15) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notice_master`
--

CREATE TABLE IF NOT EXISTS `notice_master` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `school_id` bigint(15) NOT NULL,
  `notice_title` varchar(255) NOT NULL,
  `notice_desc` text NOT NULL,
  `notice_date` date NOT NULL,
  `notice_location` text NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notification_master`
--

CREATE TABLE IF NOT EXISTS `notification_master` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `grade_id` bigint(15) NOT NULL,
  `section_id` bigint(15) NOT NULL,
  `student_id` bigint(15) NOT NULL,
  `notification_text` text NOT NULL,
  `timestamps` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `emp_id` bigint(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `poll_detail`
--

CREATE TABLE IF NOT EXISTS `poll_detail` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `poll_id` bigint(15) NOT NULL,
  `option_title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `poll_master`
--

CREATE TABLE IF NOT EXISTS `poll_master` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `school_id` bigint(15) NOT NULL,
  `question_title` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `poll_report`
--

CREATE TABLE IF NOT EXISTS `poll_report` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `school_id` bigint(15) NOT NULL,
  `device_id` bigint(15) NOT NULL,
  `poll_id` bigint(15) NOT NULL,
  `poll_detail_id` bigint(15) NOT NULL,
  `student_id` bigint(15) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `principle_desk`
--

CREATE TABLE IF NOT EXISTS `principle_desk` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `school_id` bigint(20) NOT NULL,
  `principle_name` varchar(255) NOT NULL,
  `email_id` varchar(150) NOT NULL,
  `phone_no` varchar(20) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `principle_desk`
--

INSERT INTO `principle_desk` (`id`, `school_id`, `principle_name`, `email_id`, `phone_no`, `file_name`, `description`, `updated_on`) VALUES
(1, 0, '', '', '', '', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `section_master`
--

CREATE TABLE IF NOT EXISTS `section_master` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `school_id` bigint(15) NOT NULL,
  `section_name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `standard_master`
--

CREATE TABLE IF NOT EXISTS `standard_master` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `school_id` bigint(15) NOT NULL,
  `standard_name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `state_master`
--

CREATE TABLE IF NOT EXISTS `state_master` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `country_id` bigint(15) NOT NULL,
  `state_name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `state_master`
--

INSERT INTO `state_master` (`id`, `country_id`, `state_name`, `status`) VALUES
(1, 1, 'Gujarat', 1);

-- --------------------------------------------------------

--
-- Table structure for table `student_detail`
--

CREATE TABLE IF NOT EXISTS `student_detail` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `school_id` bigint(15) NOT NULL,
  `student_id` bigint(15) NOT NULL,
  `standard_id` bigint(15) NOT NULL,
  `section_id` bigint(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `student_device`
--

CREATE TABLE IF NOT EXISTS `student_device` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `school_id` bigint(15) NOT NULL,
  `student_id` bigint(15) NOT NULL,
  `device_id` bigint(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `student_master`
--

CREATE TABLE IF NOT EXISTS `student_master` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `school_id` bigint(15) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `mname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `p_email` varchar(255) NOT NULL,
  `s_email` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `p_contact` varchar(20) NOT NULL,
  `s_contact` varchar(20) NOT NULL,
  `dob` varchar(30) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `course_id` bigint(15) NOT NULL,
  `grade_id` bigint(15) NOT NULL,
  `section_id` bigint(15) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `syllabus_master`
--

CREATE TABLE IF NOT EXISTS `syllabus_master` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `school_id` bigint(15) NOT NULL,
  `course_id` bigint(15) NOT NULL,
  `grade_id` bigint(15) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `file_path` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `time_table`
--

CREATE TABLE IF NOT EXISTS `time_table` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `school_id` bigint(15) NOT NULL,
  `course_id` bigint(15) NOT NULL,
  `grade_id` bigint(15) NOT NULL,
  `section_id` bigint(15) NOT NULL,
  `file_path` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `uploaded_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
