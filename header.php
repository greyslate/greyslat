<?php
include("includes/config-frontend.php");

//--- Check if user logged in or not
if (!isset($_SESSION['institute_id']) && $_SESSION['institute_id'] == "") {
    header("location: login.php");
    exit(0);
}

//-- Menu Scripting
$req_url = $_SERVER['SCRIPT_NAME'];

$dashboard_class = '';
$master_class = '';
$class_c = '';
$notice_class = '';
$emp_class = '';
$leave_class = '';
$other_class = '';
$attandance_class = '';

if (strstr($req_url, "/index.php")) {
    $dashboard_class = 'selected';
}


if (strstr($req_url, "/grade-add.php") || strstr($req_url, "/grade-edit.php") ||
    strstr($req_url, "/grade-list.php") || strstr($req_url, "/course-list.php") ||
    strstr($req_url, "/course-add.php") || strstr($req_url, "/course-edit.php") ||
    strstr($req_url, "/section-list.php") || strstr($req_url, "/section-add.php") ||
    strstr($req_url, "/section-edit.php")
) {
    $master_class = 'selected';
}


if (strstr($req_url, "/class-list.php") ||
    strstr($req_url, "/class-edit.php") ||
    strstr($req_url, "/class-list.php") ||
    strstr($req_url, "/announcement-list.php") ||
    strstr($req_url, "/announcement-add.php") ||
    strstr($req_url, "/announcement-edit.php") ||
    strstr($req_url, "/timetable-list.php") ||
    strstr($req_url, "/timetable-add.php") ||
    strstr($req_url, "/timetable-edit.php") ||
    strstr($req_url, "/syllabus-list.php") ||
    strstr($req_url, "/syllabus-add.php") ||
    strstr($req_url, "/syllabus-edit.php")
) {
    $class_c = 'selected';
}


if (strstr($req_url, "/event-list.php") || strstr($req_url, "/event-add.php") ||
    strstr($req_url, "/event-edit.php") || strstr($req_url, "/notice-list.php") ||
    strstr($req_url, "/notice-add.php") || strstr($req_url, "/notice-edit.php") ||
    strstr($req_url, "/newsletter-list.php") ||
    strstr($req_url, "/newsletter-add.php") ||
    strstr($req_url, "/newsletter-edit.php") ||
    strstr($req_url, "/notification-list.php") ||
    strstr($req_url, "/notification-add.php") ||
    strstr($req_url, "/holiday-list.php")
) {
    $notice_class = 'selected';
}

if (strstr($req_url, "/appointment-list.php") ||
    strstr($req_url, "/gallery-list.php") ||
    strstr($req_url, "/gallery-add.php") ||
    strstr($req_url, "/principle-desk.php") ||
    strstr($req_url, "/cms.php") ||
    strstr($req_url, "/cms-add.php") ||
    strstr($req_url, "/cms-edit.php") ||
    strstr($req_url, "/poll-list.php")
) {
    $other_class = 'selected';
}


if (strstr($req_url, "/student-list.php") ||
    strstr($req_url, "/student-add.php") ||
    strstr($req_url, "/student-edit.php")
) {
    $class_c = 'selected';
}


if (strstr($req_url, "/emp-list.php") || strstr($req_url, "/emp-add.php")
    || strstr($req_url, "/emp-edit.php")
) {
    $emp_class = 'selected';
}
$role_id = $_SESSION['role_id'];
//$einfo = $emp_obj->getEmployee($_SESSION['emp_id']);
//$rinfo = $role_obj->getRole($_SESSION['role_id']);
//$pinfo = $parent_obj->getParent($_SESSION['parent_id']);

if (strstr($req_url, "/leave-application-list.php") ||
    strstr($req_url, "/leave-add.php") ||
    strstr($req_url, "/leave-edit.php") ||
    strstr($req_url, "/parent-list.php") ||
    strstr($req_url, "/parent-add.php") ||
    strstr($req_url, "/parent-edit.php") ||
    strstr($req_url, "/document-list.php") ||
    strstr($req_url, "/document-add.php") ||
    strstr($req_url, "/document-edit.php")
) {
    $leave_class = 'selected';
}

if (strstr($req_url, "/attandance-list.php") ||
    strstr($req_url, "/attandance-upload.php")
) {
    $attandance_class = 'selected';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>|| Greyslate - <?php echo $_SESSION["institute_name"]; ?> ||</title>
    <link type="text/css" rel="stylesheet" href="<?php echo CSS_URL; ?>loadingwindow.css"/>
    <!-- fancybox -->
    <script type="text/javascript">
        var base_url = "<?php echo SITE_URL; ?>";
    </script>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo THEME_URL; ?>global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo THEME_URL; ?>global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo THEME_URL; ?>global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo THEME_URL; ?>global/plugins/uniform/css/uniform.default.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo THEME_URL; ?>global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="<?php echo THEME_URL; ?>global/css/components.css" id="style_components" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo THEME_URL; ?>global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo THEME_URL; ?>admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
    <link id="style_color" href="<?php echo THEME_URL; ?>admin/layout/css/themes/darkblue.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo THEME_URL; ?>admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo THEME_URL; ?>css/style.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
    <link href="<?php echo LIB_URL; ?>ckeditor/sample.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo PLUGIN_URL; ?>bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo THEME_URL; ?>global/plugins/bootstrap-select/bootstrap-select.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>global/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo THEME_URL; ?>global/plugins/jquery-multi-select/css/multi-select.css"/>

    <link rel="stylesheet" type="text/css"
          href="<?php echo THEME_URL; ?>global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>


</head>

<body class="page-header-fixed page-quick-sidebar-over-content page-full-width">
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="<?php echo SITE_URL; ?>" style="font-size:30px; text-decoration: none;">
                <span style="color:#E4E6E8;">GREY</span> <span style="color:#E43F41;">SLATE</span>
            </a>
        </div>

        <!-- END LOGO -->
        <!-- BEGIN HORIZANTAL MENU -->
        <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
        <!-- DOC: This is desktop version of the horizontal menu. The mobile version is defined(duplicated) sidebar menu below. So the horizontal menu has 2 seperate versions -->
        <div class="hor-menu hidden-sm hidden-xs">
            <ul class="nav navbar-nav">
                <!--<li class="classic-menu-dropdown <?php /*if (!empty($dashboard_class)) { */?> active <?php /*} */?>">
                    <a href="<?php /*echo SITE_URL; */?>">
                        Dashboard <span class="<?php /*echo $dashboard_class; */?>">
                                </span>
                    </a>
                </li>-->
                <?php if ($role_id == 1) { ?>
                    <li class="classic-menu-dropdown <?php if (!empty($master_class)) { ?> active <?php } ?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Master Entries <i class="fa fa-angle-down"></i>
                            <span class="<?php echo $master_class; ?>">
                                </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="course-list.php">Manage Courses</a></li>
                            <li><a href="grade-list.php">Manage Grade</a></li>
                            <li><a href="section-list.php">Manage Section</a></li>
                        </ul>
                    </li>

                    <li class="classic-menu-dropdown <?php if (!empty($class_c)) { ?> active <?php } ?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Classroom <i class="fa fa-angle-down"></i>
                            <span class="<?php echo $class_c; ?>">
                                </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="class-list.php">Manage Classes</a></li>
                            <li><a href="student-list.php">Manage Students</a></li>
                            <li><a href="announcement-list.php">Manage Announcement</a></li>
                            <li><a href="timetable-list.php">Manage Timetable</a></li>
                            <li><a href="syllabus-list.php">Manage Syllabus</a></li>
                            <li><a href="examtimetable-list.php">Manage Exam Timetable</a></li>
                        </ul>
                    </li>


                    <li class="classic-menu-dropdown <?php if (!empty($notice_class)) { ?> active <?php } ?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Noticeboard <i class="fa fa-angle-down"></i>
                            <span class="<?php echo $notice_class; ?>">
                                </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="event-list.php">Manage Events</a></li>
                            <li><a href="notice-list.php">Manage Notice</a></li>
                            <li><a href="newsletter-list.php">Manage Newsletter</a></li>
                            <li><a href="notification-list.php">Important Notification</a></li>
                            <li><a href="holiday-add.php">Upload Holiday List File</a></li>
                        </ul>
                    </li>

                    <li class="classic-menu-dropdown <?php if (!empty($other_class)) { ?> active <?php } ?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Others <i class="fa fa-angle-down"></i>
                            <span class="<?php echo $other_class; ?>">
                                </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="appointment-list.php">Manage Appointment</a></li>
                            <li><a href="gallery-list.php">Manage Gallery</a></li>
                            <li><a href="cms.php">Manage CMS</a></li>
                            <li><a href="principle-desk.php">Principle Desk</a></li>
                            <li><a href="poll-list.php">Poll</a></li>
                        </ul>
                    </li>
                    <li class="classic-menu-dropdown <?php if (!empty($emp_class)) { ?> active <?php } ?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Teachers <i class="fa fa-angle-down"></i> <span class="<?php echo $emp_class; ?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="emp-add.php">Create Teacher Account</a></li>
                            <li><a href="emp-list.php">List Of Teacher</a></li>
                            <!--<li><a href="leave-application-list.php">Leave Application</a></li>-->
                        </ul>
                    </li>
                    <li class="classic-menu-dropdown <?php if (!empty($parent_class)) { ?> active <?php } ?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Manage Parent <i class="fa fa-angle-down"></i>
                            <span class="<?php echo $parent_class; ?>">
                                </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="parent-list.php">Parents</a></li>
                            <li><a href="student-list.php">List of Student</a></li>
                            <li><a href="parent-add.php">Create New Parent Account</a></li>
                           <!-- <li><a href="student_leave-application-list.php">Leave Application</a></li>-->
                            <li><a href="document-list.php">Manage Document</a></li>
                        </ul>
                    </li>

                    <li class="classic-menu-dropdown <?php if (!empty($leave_class)) { ?> active <?php } ?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Manage Leave <i class="fa fa-angle-down"></i>
                            <span class="<?php echo $leave_class; ?>">
                                </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="leave-application-list.php">Leave</a></li>

                        </ul>
                    </li>

                    <li class="classic-menu-dropdown <?php if (!empty($emp_class)) { ?> active <?php } ?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Assignment/Work Schedule<i class="fa fa-angle-down"></i> <span class="<?php echo $emp_class; ?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="assignment-add.php">Upload New Assignment</a></li>
                            <li><a href="   ">Upload New Worksheet</a></li>
                            <li><a href="   ">Upload Completed Worksheet</a></li>
                            <li><a href="assignment-list.php">List of Assignment</a></li>
                        </ul>
                    </li>

                    <li class="classic-menu-dropdown <?php /*if (!empty($emp_class)) { */?>  <?php /*} */?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Results <i class="fa fa-angle-down"></i> <span class="<?php /*echo $emp_class; */?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="result-add.php">Upload Result</a></li>
                            <li><a href="result-list.php">View/Download Result</a></li>
                        </ul>
                    </li>

                    <li class="classic-menu-dropdown <?php if (!empty($attandance_class)) { ?> active <?php } ?>">
                        <a href="attandance-list.php">
                            Manage Attandance <span class="<?php echo $attandance_class; ?>">
                                </span>
                        </a>
                    </li>

                    <li class="classic-menu-dropdown <?php if (!empty($report_class)) { ?> active <?php } ?>">
                        <a href="report.php">
                            Reports <span class="<?php echo $report_class; ?>">
                                </span>
                        </a>
                    </li>



                <?php } else if ($role_id == 2) {
                    ?>

                    <li class="classic-menu-dropdown <?php if (!empty($emp_class)) { ?> active <?php } ?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Profile <i class="fa fa-angle-down"></i> <span class="<?php echo $emp_class; ?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="profile.php">Manage Profile</a></li>
                            <li><a href="   ">Change Password</a></li>
                        </ul>
                    </li>

                    <li class="classic-menu-dropdown <?php if (!empty($emp_class)) { ?> active <?php } ?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Student Details <i class="fa fa-angle-down"></i> <span class="<?php echo $emp_class; ?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="student-list.php">List of Student</a></li>
                            <li><a href="student-edit.php">Update Student</a></li>
                        </ul>
                    </li>

                    <li class="classic-menu-dropdown <?php if (!empty($emp_class)) { ?> active <?php } ?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Assignment/Work Schedule<i class="fa fa-angle-down"></i> <span class="<?php echo $emp_class; ?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="assignment-add.php">Upload New Assignment</a></li>
                            <li><a href="   ">Upload New Worksheet</a></li>
                            <li><a href="   ">Upload Completed Worksheet</a></li>
                            <li><a href="assignment-list.php">List of Assignment</a></li>
                        </ul>
                    </li>

                    <li class="classic-menu-dropdown <?php if (!empty($emp_class)) { ?> active <?php } ?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Attendance <i class="fa fa-angle-down"></i> <span class="<?php echo $emp_class; ?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="attandance-list.php">View/Download Attandence</a></li>
                        </ul>
                    </li>

                    <li class="classic-menu-dropdown <?php /*if (!empty($emp_class)) { */?>  <?php /*} */?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Parent Meassage<i class="fa fa-angle-down"></i> <span class="<?php /*echo $emp_class; */?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="   ">Generete Parent Notification</a></li>
                            <li><a href=    >List for Communication</a></li>
                        </ul>
                    </li>

                    <li class="classic-menu-dropdown <?php /*if (!empty($emp_class)) { */?>  <?php /*} */?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Results <i class="fa fa-angle-down"></i> <span class="<?php /*echo $emp_class; */?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="result-list.php">View/Download Result</a></li>
                        </ul>
                    </li>

                    <li class="classic-menu-dropdown <?php if (!empty($emp_class)) { ?> active <?php } ?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Leave Details<i class="fa fa-angle-down"></i> <span class="<?php echo $emp_class; ?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">

                            <li><a href="leave-application-list.php">List of Leaves</a></li>
                            <li><a href="student_leave-application-list.php">List of Students Leave</a></li>
                        </ul>
                    </li>

                    <li class="classic-menu-dropdown <?php if (!empty($emp_class)) { ?> active <?php } ?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Announcement <i class="fa fa-angle-down"></i> <span class="<?php echo $emp_class; ?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="notification-list.php">List of Notification</a></li>
                        </ul>
                    </li>

                    <li class="classic-menu-dropdown <?php /*if (!empty($emp_class)) { */?>  <?php /*} */?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Documents <i class="fa fa-angle-down"></i> <span class="<?php /*echo $emp_class; */?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="   ">Admission Form</a></li>
                            <li><a href=    >School Rules & Regulation</a></li>
                            <li><a href=    >LC Tranfer Form</a></li>
                            <li><a href=    >Student Undertaking Form</a></li>
                        </ul>
                    </li>

                    <li class="classic-menu-dropdown <?php /*if (!empty($emp_class)) { */?>  <?php /*} */?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Others <i class="fa fa-angle-down"></i> <span class="<?php /*echo $emp_class; */?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="holiday-list.php">Holiday Schedule</a></li>
                            <li><a href="   ">Events Schedule</a></li>
                        </ul>
                    </li>



                    <?php
                } else{ ?>

                    <li class="classic-menu-dropdown <?php if (!empty($emp_class)) { ?> active <?php } ?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Profile <i class="fa fa-angle-down"></i> <span class="<?php echo $emp_class; ?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="profile.php">Profile</a></li>
                            <li><a href="   ">Change Password</a></li>
                            <li><a href="   ">Contact</a></li>
                            <li><a href="parent-detail.php?id=<?php echo $_SESSION['emp_id']; ?>">Parent details</a></li>
                        </ul>
                    </li>
                    <li class="classic-menu-dropdown <?php if (!empty($emp_class)) { ?> active <?php } ?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Student Details <i class="fa fa-angle-down"></i> <span class="<?php echo $emp_class; ?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="parent_student-list.php">List of students</a></li>
                            <li><a href="student-edit.php">Update child details</a></li>
                        </ul>
                    </li>
                    <li class="classic-menu-dropdown <?php if (!empty($emp_class)) { ?> active <?php } ?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Assignment/Work Schedule <i class="fa fa-angle-down"></i> <span class="<?php echo $emp_class; ?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                           <!-- <li><a href="   ">Upload Worksheets</a></li>-->
                            <li><a href="assignment-list.php">List of Assignment</a></li>
                        </ul>
                    </li>
                    <li class="classic-menu-dropdown <?php if (!empty($emp_class)) { ?> active <?php } ?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Attendance  <i class="fa fa-angle-down"></i> <span class="<?php echo $emp_class; ?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="attandance-list.php">View/Download Attandence</a></li>
                        </ul>
                    </li>
                    <li class="classic-menu-dropdown <?php /*if (!empty($emp_class)) { */?>  <?php /*} */?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Results <i class="fa fa-angle-down"></i> <span class="<?php /*echo $emp_class; */?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="result-list.php">View/Download Result</a></li>
                        </ul>
                    </li>
                    <li class="classic-menu-dropdown <?php if (!empty($emp_class)) { ?> active <?php } ?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Leave Details <i class="fa fa-angle-down"></i> <span class="<?php echo $emp_class; ?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <!--<li><a href="leave-add.php">Generate Leave Request</a></li>-->
                            <li><a href="leave-application-list.php">List of Leaves</a></li>

                        </ul>
                    </li>
                    <li class="classic-menu-dropdown <?php if (!empty($emp_class)) { ?> active <?php } ?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Announcement <i class="fa fa-angle-down"></i> <span class="<?php echo $emp_class; ?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="notification-list.php">List of Notification</a></li>
                        </ul>
                    </li>
                    <li class="classic-menu-dropdown <?php /*if (!empty($emp_class)) { */?>  <?php /*} */?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Documents <i class="fa fa-angle-down"></i> <span class="<?php /*echo $emp_class; */?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="   ">Admission Form</a></li>
                            <li><a href="   ">School Rules & Regulation</a></li>
                            <li><a href="   ">LC Tranfer Form</a></li>
                            <li><a href="   ">Student Undertaking Form</a></li>
                        </ul>
                    </li>
                    <li class="classic-menu-dropdown <?php /*if (!empty($emp_class)) { */?> <?php /*} */?>">
                        <a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown"
                           data-close-others="true">
                            Other <i class="fa fa-angle-down"></i> <span class="<?php /*echo $emp_class; */?>">
                                    </span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="holiday-list.php">Holiday Schedule</a></li>
                            <li><a href="event-list.php">Events Schedule</a></li>
                        </ul>
                    </li>

                <?php }
                ?>
            </ul>
        </div>
        <!-- END HORIZANTAL MENU -->
        <!-- BEGIN HEADER SEARCH BOX -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
           data-target=".navbar-collapse">
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                       data-close-others="true">
                        <img alt="" class="img-circle" src="<?php echo THEME_URL; ?>admin/layout/img/avatar.png"/>
                        <span class="username username-hide-on-mobile">
                                    <?php echo $_SESSION['login_name']; ?> </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="profile.php">
                                <i class="icon-user"></i> My Profile </a>
                        </li>
                        <li class="divider">
                        </li>

                        <li>
                            <a href="logout.php">
                                <i class="icon-key"></i> Log Out
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
