<?php
include("header.php");
if(!isset($_GET['id'])){
    header("location: document-list.php");
    exit(0);
}

$id = $_GET['id'];
$val = $doc_obj->getDocument($id);

?>
<!-- include js and css -->
<script src="<?php echo JS_URL; ?>iphone-style-checkboxes.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo CSS_URL; ?>radiobutton.css" type="text/css" media="screen" charset="utf-8" />
<link rel="stylesheet" href="<?php echo LIB_URL; ?>chosen/chosen.css" />
<script src="<?php echo LIB_URL; ?>chosen/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo JS_URL; ?>script/document.js" type="text/javascript" charset="utf-8"></script>

<div class="content">
  <div class="page-header full-content bg-blue">
        <div class="row">
            <div class="col-sm-6">
                <h1>School Management <small>Edit Document</small></h1>
            </div><!--.col-->
            <div class="col-sm-6">
                <ol class="breadcrumb">
                   <li><a href="#"><i class="ion-home"></i></a></li>
                    <li><a href="index.php">Dashboard</a></li>
                    <li><a href="document-list.php">View Document</a></li>
                    <li><a href="#" class="active">Edit Document</a></li>
                </ol>
            </div><!--.col-->
        </div><!--.row-->
    </div><!--.page-header-->
	 <div class="row">
     	<div class="col-md-12">
        	<div style="display:none" class="alert alert-danger-transparent">
            	<strong>Oops!</strong> Please correct following errors.
            </div>
			<div class="panel">
				<div class="panel-heading">
					<div class="panel-title">
						<h4>Edit Document</h4> <p style="float: right;"> (<span class="text-red">*</span>) required Fields</p>
					</div>
                </div><!--.panel-heading-->
				<div class="panel-body">
                	<form id="frmedit" action="" class="form-horizontal" method="post" enctype="multipart/form-data" >
                    	 <div class="form-content">
						 	<div class="form-group">
                                <label class="control-label col-md-3">Title<span class="text-red">*</span></label>
                                	<div class="col-md-5">
                                    	<div class="inputer">
                                        	<div class="input-wrapper">
                                            <input value="<?php echo $val->title; ?>" type="text" name="txttitle" id="txttitle" rows="3" cols="50" style="resize: none;" class="form-control required" title="Please enter Title."> 
											</div>
										</div>
                                	</div>
                            	</div>
								<div class="form-group">
                                	<label class="control-label col-md-3">Upload File<span class="text-red">*</span></label>
                                		<div class="col-md-5">
                                    		<div class="inputer">
                                        		<div class="input-wrapper">
                                           			  <input value = "<?php echo $val->file_name; ?> " type="file" name="file_name" id="file_name" rows="3" cols="50" style="resize: none;" class="form-control required" title="Please select file to upload.">
												</div>
											</div>
                                		</div>
                            	</div> 
                                <div class="form-group">
                                    <label class="control-label col-md-3">Uploaded By<span class="text-red">*</span></label>
                                        <div class="col-md-5">
                                            <div class="inputer">
                                                <div class="input-wrapper">
                                                      <input value="<?php echo $val->uploaded_by; ?>" type="text" name="uploaded_by" id="txtuploadedby" rows="3" cols="50" style="resize: none;" class="form-control required" title="Please enter id.">
                                                </div>
                                            </div>
                                        </div>
                                </div>   
                                <div class="form-group">
                                	<div class="formRow on_off_activestatus">             <?php
                                                $c_checked = '';
                                                if($val->status==1){
                                                    $c_checked = 'checked="checked"';
                                                }
                                            ?>                                     
                                            <label class="control-label col-md-3">Status</label>
                                            <input name="status" value="1" type="checkbox" id="status" checked=""   />
                                    </div>
                                </div>       
                           		 <div class="form-buttons">
                            		<div class="row">
                                		<div class="col-md-offset-3 col-md-9">
                                    		<input type="hidden" name="action" value="edit">
                                            <input type="hidden" name="id" value="<?php echo $_REQUEST['id']; ?>" />
                                    		<button type="submit" class="btn btn-blue">Submit</button>
                                    		<button onclick="location.href='document-list.php'" type="button" class="btn btn-flat btn-default">Cancel</button>
                            			</div>
                        			</div>
                    			</div>
                    		 </div>
						</form>
				</div>
    		</div>
		</div
><?php
include("sidebar.php");
include("footer.php");
?>