<?php

    include("includes/config-frontend.php");
    $objPHPExcel = new PHPExcel();
    $sql = "Select * From ".STUDENT_MASTER." ";
    $db->query($sql);

    $rows = $db->fetch_object();

    $i = 0;
    $main_arr = array();
    if(count($rows) > 0){
        foreach($rows as $val){
		 $main_arr[$i]["roll_no"]      = $val->roll_no;
            $main_arr[$i]["name"]     = $val->fname." " .$val->lname;
			$main_arr[$i]["profile_pic"]      = $val->profile_pic;
			$main_arr[$i]["p_email"]      = $val->p_email; 
			$main_arr[$i]["s_email"]      = $val->s_email; 
			$main_arr[$i]["address"]      = $val->address; 
			$main_arr[$i]["p_contact"]      = $val->p_contact; 
			$main_arr[$i]["s_contact"]      = $val->s_contact; 
            $main_arr[$i]["gender"]      = $val->gender;
            $main_arr[$i]["dob"]    = $val->dob;
            $main_arr[$i]["created_on"] = $val->created_on;
           $main_arr[$i]["updated_on"]      = $val->updated_on; 
            $i++;
        }
    }


    $rowCount = 1;
	 $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "Roll number");
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Name");
	 $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Profile pic");
	 $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, "Parent email");
	 $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, "Studentemail");
	 $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "Address");
	 $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, "Parent contact");
	 $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, "Student contact");
	     $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, "Gender");
    $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, "DOB");
    $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, "Created On");
   $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, "Updated On");


    $rowCount = 2;
    for($i=0;$i<count($main_arr);$i++){
 $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $main_arr[$i]['roll_no']);	
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $main_arr[$i]['name']);
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $main_arr[$i]['profile_pic']);
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $main_arr[$i]['p_email']);
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $main_arr[$i]['s_email']);
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $main_arr[$i]['address']);
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $main_arr[$i]['p_contact']);
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $main_arr[$i]['s_contact']);
       $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $main_arr[$i]['gender']);
        $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $main_arr[$i]['dob']);

        $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $main_arr[$i]['created_on']);
    $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $main_arr[$i]['updated_on']);
        $rowCount++;
        //pr($objPHPExcel); 
    }

    header('Content-Type: application/vnd.openxmlformats-   officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.date("d-m-Y").'-student.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

?>