<?php
include("header.php");

$cobj = new Result();


if($_SESSION['role_id'] == 1 ) {
    $where = " ";
    $data = $cobj->fetch_all($where);
}
elseif($_SESSION['role_id'] == 2 ) {
    $where = " ";
    $data = $cobj->fetch_all($where);
}
else{
    $where = "And SM.p_email = '".$_SESSION['email']."'";
    $data = $cobj->fetch_all($where);
}

$grade_list = FillCombo($grade_obj->fetch_grade_list(), "id", "standard_name", "");
$course_list = FillCombo($course_obj->fetch_course_list(), "id", "course_name", "");
$section_list = FillCombo($section_obj->fetch_section_list(), "id", "section_name", "");

?>
<div class="page-container">
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box red ">
                    <div class="portlet-title">
                        <div class="caption">
                            Search
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmSearch" id="frmSearch">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Title</label>
                                    <div class="col-md-9">
                                        <input type="text" id="tbxTitle" name="tbxTitle" placeholder="Name" class="form-control required"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Student Name</label>
                                    <div class="col-md-9">
                                        <input type="text" id="student_name" name="student_name" placeholder="Name" class="form-control "/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Student Roll No</label>
                                    <div class="col-md-9">
                                        <input type="text" id="student_rollno" name="student_rollno" placeholder="Name" class="form-control "/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Grade/Standard </label>
                                    <div class="col-md-9">
                                        <select name="grade_id" id="grade_id" class="form-control ">
                                            <?php echo $grade_list; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Course Name </label>
                                    <div class="col-md-9">
                                        <select name="course_id" id="course_id" class="form-control ">
                                            <?php echo $course_list; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Section Name </label>
                                    <div class="col-md-9">
                                        <select name="section_id" id="section_id" class="form-control ">
                                            <?php echo $section_list; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green" name="btnSearch" id="btnSearch" type="submit" onclick="return Search_Data();"><i class="fa fa-check"></i> Search</button>
                                        <button type="button" type="reset" id="btnReset" class="btn default">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>       
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Manage Result <small>view Result</small>
                    </h3>
                    <div class="row">
                        <form id="frmtable" name="frm" method="post"  enctype="multipart/form-data" >
                            <div class="col-md-12">
                                <div class="right" style="float:right;">
                                    <?php if($_SESSION['role_id'] == 2 ) {  ?><a href="result-add.php" class="btn btn-primary">
                                        Add New <i class="fa fa-plus"></i>
                                    </a><?php } ?>
                                   <a href="result-export.php" class="btn red">Export Data</a>
                                </div>
                                <br><br><br>

                                <!-- BEGIN BORDERED TABLE PORTLET-->
                                <div class="portlet box red">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cogs"></i>Manage Result
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div id="page_listing">
                                            <table class="table table-bordered table-striped table-condensed flip-content" >
                                                <thead>
                                                    <tr>
                                                        <th class="chb_col" width="1%" >
                                                            <input type="checkbox" name="checkall" onclick="javascript:checkAlluncheckAll(frm);">
                                                        </th>
                                                        <th width="9%">
                                                            Student Id
                                                        </th>
                                                        <th width="15%" >
                                                            Student Name
                                                        </th>
                                                        <th width="15%">
                                                            Exam Title
                                                        </th>
                                                        <th width="15%">
                                                            Course Name
                                                        </th>

                                                       <!-- <th>
                                                            Date
                                                        </th>-->

                                                        <th width="11%">
                                                            Standard
                                                        </th>
                                                        <th>
                                                            Total Marks
                                                        </th>
                                                        <th>
                                                            Passing Marks
                                                        </th>
                                                        <th>
                                                            Marks Obtained
                                                        </th>



                                                        <th width="10%" style="text-align: center;">
                                                            Action
                                                        </th>
                                                    </tr>
                                                </thead>

                                                <tbody id="content ui-sortable">

                                                    <?php
                                                    if (count($data) == 0) {
                                                        ?>
                                                        <tr><td colspan="8" align="center">No Result found</td></tr>
                                                        <?php
                                                    } else {
                                                        foreach ($data as $val) {
                                                            ?>
                                                            <tr>
                                                                <td class="chb_col">
                                                                    <input type="checkbox" name="id[]" value="<?php echo $val->id; ?>">
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->roll_no; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->fname." ".$val->lname; ?>
                                                                </td>

                                                                <td>
                                                                    <?php echo $val->exam_title; ?>
                                                                </td>

                                                                <td>
                                                                    <?php echo $val->course_name; ?>
                                                                </td>
                                                                <!--<td>
                                                                    <?php /*echo $val->exam_date; */?>
                                                                </td>-->

                                                                <td>
                                                                    <?php echo $val->standard_name."-".$val->section_name; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->total_marks; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->passing_marks; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->obt_marks; ?>
                                                                </td>
                                                                <td class="content_actions" style="text-align: center;">
                                                                    <a href="javascript:;" onclick="javascript:RemoveRecord('<?php echo $val->id; ?>')" title='Delete'>
                                                                        <img src="<?php echo IMAGE_URL; ?>ico/trashcan_gray.png" alt="" /></a> <a href="result-edit.php?id=<?php echo $val->id; ?>" class="sepV_a" title="Edit">
                                                                        <img src="<?php echo IMAGE_URL; ?>ico/pencil_gray.png" alt="" />
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                            <?php include("pagination.php"); ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- END BORDERED TABLE PORTLET-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>

<?php
include("footer.php");
?>
<script src="<?php echo JS_URL; ?>script/result.js" type="text/javascript" charset="utf-8"></script>