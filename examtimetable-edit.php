<?php
include("header.php");

if (!isset($_GET['id'])) {
    header("location: examtimetable-list.php");
    exit(0);
}

$id = $_GET['id'];
$val = $ett_obj->getExamTimeTable($id);

$grade_list = FillCombo($grade_obj->fetch_grade_list(), "id", "standard_name", $val->grade_id);
$course_list = FillCombo($course_obj->fetch_course_list(), "id", "course_name", $val->course_id);

?>

<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Edit Exam Timetable <small> edit exam timetable title & description</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo SITE_URL; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="examtimetable-list.php">Exam Timetable</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Edit Exam Timetable</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="portlet box red ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Edit Exam Timetable
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmadd" id="frmadd">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                You have some form errors. Please check below.
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Exam Title <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input value="<?php echo $val->exam_title; ?>" type="text" id="tbxtitle" name="tbxtitle" placeholder="Exam Timetable Title" class="form-control required"/>
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Grade/Standard <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <select name="grade_id" id="grade_id" class="form-control required">
                                                        <?php echo $grade_list; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Medium <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <select class="form-control required" name="medium" id="medium">
                                                    <option value="">----SELECT----</option>
                                                        <option value="English" <?php if($val->medium == 'English'){ ?> selected="selected" <?php } ?> >English</option>
                                                        <option value="Gujarati" <?php if($val->medium == 'Gujarati'){ ?> selected="selected" <?php } ?>>Gujarati</option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3">Start Date <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input id="start_date" value="<?php echo $val->start_date;?>" name="start_date" readonly="" class="form-control required date-picker" placeholder="YYYY-MM-DD">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">End Date <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input id="end_date" name="end_date" value="<?php echo $val->end_date;?>" readonly="" class="form-control required date-picker" placeholder="YYYY-MM-DD">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Upload File for Import</label>
                                                <div class="col-md-9">
                                                    <input type="file" name="file_name_import" id="file_name_import" class="form-control large" >
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3"></label>
                                                <div class="col-md-9">
                                                    <?php
                                                    if (file_exists(SAMPLE_EXAM_PATH . 'sample-examtimetable.xls')) {
                                                        ?>
                                                        <a href="<?php echo SAMPLE_EXAM_URL . 'sample-examtimetable.xls'; ?>" target="_blank">Download Sample File</a>
                                                        <?php
                                                    }
                                                    ?>    
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Upload File <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input type="file" name="file_name" id="file_name" class="form-control large required" placeholder="only xlsx format allowed" >
                                                </div>
                                            </div>


                                            
                                             <div class="form-group">
                                                <label class="control-label col-md-3">Status <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <select name="status" id="status" class="form-control required">
                                                        <option value="1" <?php if($val->status == 1){ ?> selected <?php } ?>>Active</option>
                                                        <option value="0" <?php if($val->status == 0){ ?> selected <?php } ?>>Inactive</option>
                                                    </select>
                                                </div>
                                            </div>


                                       
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input type="hidden" name="action" value="edit">
                                                    <input type="hidden" name="id" value="<?php echo $val->id; ?>">
                                                   
                                                    <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
                                                    <button type="button" onclick="location.href = 'examtimetable-list.php'" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                      
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>
<?php
include("footer.php");
?>
<link rel="stylesheet" href="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.css">
<script src="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.js"></script>
<script src="<?php echo JS_URL; ?>script/examtimetable.js" type="text/javascript" charset="utf-8"></script>
