<?php

    include("includes/config-frontend.php");
    $objPHPExcel = new PHPExcel();
    $sql = "Select * From ".EVENT_MASTER." order by id desc";
    $db->query($sql);

    $rows = $db->fetch_object();

    $i = 0;
    $main_arr = array();
    if(count($rows) > 0){
        foreach($rows as $val){
            $main_arr[$i]["event"]     = $val->event_title;
            $main_arr[$i]["desc"]       = $val->event_desc;
            $main_arr[$i]["date"]     = $val->event_date;
            $main_arr[$i]["time"]     = $val->event_time;
            $main_arr[$i]["location"]     = $val->event_location;
            $main_arr[$i]["created_on"] = $val->created_on;
            $main_arr[$i]["updated_on"] = $val->updated_on;
            $i++;
        }
    }


    $rowCount = 1;
    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "Event Title");
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Event Description");
    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Event Date");

    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, "Event Time");

    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, "Event Location");
    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "Created On");
    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, "Updated On");


    $rowCount = 2;
    for($i=0;$i<count($main_arr);$i++){
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $main_arr[$i]['event']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $main_arr[$i]['desc']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $main_arr[$i]['date']);

        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $main_arr[$i]['time']);

        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $main_arr[$i]['location']);
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $main_arr[$i]['created_on']);
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $main_arr[$i]['updated_on']);

        $rowCount++;
        //pr($objPHPExcel); 
    }

    header('Content-Type: application/vnd.openxmlformats-   officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.date("d-m-Y").'-event.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

?>