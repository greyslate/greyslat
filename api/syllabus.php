<?php
session_start();

$school_id = $_REQUEST['school_id'];
$course_id = $_REQUEST['course_id'];
$grade_id  = $_REQUEST['grade_id'];


if($school_id != "" && $course_id != "" && $grade_id != ""){
	$_SESSION['db_name'] = $school_id;

	include_once("../includes/config-frontend.php");

	$res_arr = array();

	$sql = "Select * from syllabus_master where course_id = '".$course_id."' And grade_id = '".$grade_id."' order by id desc";
	$db->query($sql);
	$result = $db->fetch_object();

	$i = 0;
	if(count($result) > 0){
		foreach($result as $val){

			$url = '';
			if(file_exists(SYLLABUS_PATH.$val->file_name)){
				$url = SYLLABUS_URL.$val->file_name;
			}

			$res_arr[$i]["title"] 		= $val->title;
			$res_arr[$i]["description"] = $val->description;
			$res_arr[$i]["file_path"] 	= $url;

			$i++;
		}
		$response_array["status"] = 1;
		$response_array["response"]	= $res_arr;	
	}
	else {
		$response_array["status"] = 1;
		$response_array["response"]	= "";		
	}
}
else {
	$response_array["status"] = 0;
	$response_array["response"]	= "Invalid Parameter";	
}

echo json_encode($response_array);
?>