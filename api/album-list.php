<?php
session_start();
$school_id 	= isset($_REQUEST['school_id']) ? $_REQUEST['school_id'] : "";


if($school_id != ""){
	$_SESSION['db_name'] = $school_id;

	include_once("../includes/config-frontend.php");

    $sql = "Select * from gallery_master where status = 1 order by id desc";
    $db->query($sql);

    $rows = $db->fetch_object();

    $res_arr = array();
    $i = 0;
    if(count($rows) > 0){
        foreach($rows as $val){
            $res_arr[$i]["album_id"]         =  $val->id;
            $res_arr[$i]["album_title"]  =  $val->gallery_title;
            $res_arr[$i]["school_path"]  =  "";
            
            
            $sql = "Select * from gallery_detail where gallery_id = '".$val->id."' order by id desc";
            $db->query($sql);
            $pdetail = $db->fetch_object();
            $o_arr = array();
            if(count($pdetail) > 0){
                $t = 0;
                $is_cover_pic_url = '';
                foreach($pdetail as $sval){
                    $o_arr[$t]["image_id"]          = $sval->id;
                    $o_arr[$t]["image_caption"]     = $val->gallery_title;
                    $o_arr[$t]["folder_id"]         = $sval->gallery_id;
                    $o_arr[$t]["school_"]           = "Test";
                    $o_arr[$t]["image_url"]         = GALLERY_URL.$sval->image_name;
                    $o_arr[$t]["time"]              = $sval->uploaded_on;

                    if($sval->is_cover_pic == 1){
                        $is_cover_pic_url = GALLERY_URL.$sval->image_name;
                    }
                    
                    $t++;
                }
            }
            
            $res_arr[$i]["album_thumnail"]  =   $is_cover_pic_url;
            $res_arr[$i]["image_list"]  =  $o_arr;
           
            $i++;
        }
        $response_array["status"] = 1;
        $response_array["response"] = $res_arr;
    }
    else {
        $response_array["status"] = 1;
        $response_array["response"] = "";
    }
}
else {
	$response_array["status"] = 0;
    $response_array["response"] = "Invalid Parameter";
}


echo json_encode($response_array);
?>