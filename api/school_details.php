<?php
session_start();
include("../includes/config-api.php");
global $db;
$school_id = isset($_REQUEST['school_id']) ? $_REQUEST['school_id'] : "";
$_SESSION['db_name'] = $school_id;

$sql = "Select * from school_master 
            where status = 1 And database_name = '" . $school_id . "'";

$db->query($sql);
$record = $db->fetch_object(MYSQL_FETCH_SINGLE);


$response_array = array();

if(count($record) > 0){
    $school_arr = array();

    $countryinfo = $country_obj->getCountry($record->country);
    $country_name = @$countryinfo->country_name;

    $stateinfo = $state_obj->getState($record->state);
    $state_name = @$stateinfo->state_name;

    $cityinfo = $city_obj->getCity($record->city);
    $city_name = @$cityinfo->city_name;


    $school_arr["institute_name"]       = $record->institute_name;
    $school_arr["institute_address"]    = $record->address;
    $school_arr["institute_city"]       = $city_name;
    $school_arr["institute_state"]      = $state_name;
    $school_arr["institute_country"]    = $country_name;
    $school_arr["institute_postalcode"] = $record->postalcode;
    $school_arr["institute_phone"]      = $record->phone_no;
    $school_arr["institute_logo"]       = ($record->institute_logo!="") ? UPLOAD_URLS.$record->school_path.$record->institute_logo : "";
    $school_arr["institute_email"]      = $record->institute_email;
    $school_arr["institute_type"]       = $record->institute_type;
    $school_arr["institute_database"]   = $record->database_name;


    include_once("../includes/config-frontend.php");

    $sql = "Select * from course_master where status = 1 order by course_name";
    $db->query($sql);
    $crecord = $db->fetch_object();

    $carr = array();
    if(count($crecord) > 0){
        $t = 0;
        foreach($crecord as $cval){
            $carr[$t]["course_id"]   = $cval->id;
            $carr[$t]["course_name"] = $cval->course_name;
        }
    }

    $school_arr["course"]   = $carr;


    $sql = "Select * from standard_master where status = 1 order by standard_name";
    $db->query($sql);
    $record = $db->fetch_object();

    $main_arr = array();
    if (count($record) > 0) {
        $i = 0;
        foreach ($record as $val) {

            $main_arr[$i]["grade"] = $val->standard_name;
            $main_arr[$i]["grade_id"] = $val->id;

            $sql = "Select * from section_master where status = 1 order by section_name ";
            $db->query($sql);
            $records = $db->fetch_object();

            if (count($records) > 0) {
                $j = 0;
                $sub_arr = array();
                foreach ($records as $value) {
                    $sub_arr[$j]["section"] = $value->section_name;
                    $sub_arr[$j]["section_id"] = $value->id;
                    $j++;
                }
            }

            $main_arr[$i]["sections"] = $sub_arr;

            $i++;
        }

    }
    $school_arr["other"] = $main_arr;
    $response_array["status"] = 1;
    $response_array["response"] = $school_arr;

}
else {
    $response_array["status"] = 0;
    $response_array["response"] = "Invalid School ID";
}
echo json_encode($response_array);
?>