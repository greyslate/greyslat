<?php
session_start();
$school_id 	= isset($_REQUEST['school_id']) ? $_REQUEST['school_id'] : "";


if($school_id != ""){
	$_SESSION['db_name'] = $school_id;

	include_once("../includes/config-frontend.php");

    $sql = "Select * from event_master where event_date >= '".date("Y-m-d")."' And status = 1 order by id desc";
    $db->query($sql);

    $rows = $db->fetch_object();

    $res_arr = array();
    $i = 0;
    if(count($rows) > 0){
        foreach($rows as $val){
            $res_arr[$i]["event_title"]        =  $val->event_title;
            $res_arr[$i]["event_desc"]         =  $val->event_desc;
            $res_arr[$i]["event_date"]         =  $val->event_date;
            $res_arr[$i]["event_time"]         =  $val->event_time;
            $res_arr[$i]["event_location"]     =  $val->event_location;
            $i++;
        }
        $response_array["status"] = 1;
        $response_array["response"] = $res_arr;
    }
    else {
        $response_array["status"] = 1;
        $response_array["response"] = "";
    }
}
else {
	$response_array["status"] = 0;
    $response_array["response"] = "Invalid Parameter";
}

echo json_encode($response_array);
?>