<?php
session_start();
include("../includes/config-api.php");
global $db;
$school_id = isset($_REQUEST['school_id']) ? $_REQUEST['school_id'] : "";
$_SESSION['db_name'] = $school_id;

if ($school_id) {
    $sql = "Select count(*) As cnt From emp_master Where email = '" . $_REQUEST['txtemail'] . "'";
    $db->query($sql);
    $crow = $db->fetch_object(MYSQL_FETCH_SINGLE);
    if ($crow->cnt > 0) {
        $response_array["status"] = 0;
        $response_array["response"] = "Email is already exits!";
    } else {
        $NewImageName = '';
        if (isset($_FILES['profile_pic']['name']) && $_FILES['profile_pic']['name'] != "") {
            //==== Image Uploading
            $RandomNum = time();
            $ImageName = str_replace(' ', '-', strtolower($_FILES['profile_pic']['name']));
            $ImageType = $_FILES['profile_pic']['type']; //"image/png", image/jpeg etc.
            $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
            $ImageExt = str_replace('.', '', $ImageExt);
            $ImageName = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
            $NewImageName = $ImageName . '-' . $RandomNum . '.' . $ImageExt;
            move_uploaded_file($_FILES["profile_pic"]["tmp_name"], EMP_PATH . $NewImageName);
        }

        if ($_REQUEST['txtemail']) {
            $arr = array(
                "school_name" => $_REQUEST['txtschoolname'],
                "email" => $_REQUEST['txtemail'],
                "password" => $_REQUEST['txtpassword'],
                "phone" => $_REQUEST['txtphone'],
                "fname" => $_REQUEST['txtfname'],
                "lname" => $_REQUEST['txtlname'],
                "profile_pic" => $NewImageName,
                "role_id" => 2,
                "created_on" => date("Y-m-d H:i:s"),
                "status" => 1,
            );
            $result = InsertData($arr, 'emp_master');
            $response_array["status"] = 1;
            $response_array["response"] = "Success";
        } else {
            $response_array["status"] = 0;
            $response_array["response"] = "Invalid Data.";
        }
    }
} else {
    $response_array["status"] = 0;
    $response_array["response"] = "Invalid School ID";
}
echo json_encode($response_array);
?>