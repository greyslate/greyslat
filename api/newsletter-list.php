<?php
session_start();

$school_id 	= isset($_REQUEST['school_id']) ? $_REQUEST['school_id'] : "";

if($school_id != ""){
	$_SESSION['db_name'] = $school_id;

	include_once("../includes/config-frontend.php");

    $sql = "Select * from newsletter_master where status = '1' order by id desc ";
    $db->query($sql);
    $row = $db->fetch_object();

    $res = array();
    if(count($row) >= 0){
        
        $i = 0;
        foreach($row as $rval){
            $res[$i]["newsletter_id"]   = $rval->id;
            $res[$i]["title"]           = $rval->title;
            $res[$i]["description"]     = $rval->description;
            $res[$i]["created_on"]      = $rval->created_on;
            $i++;
        }        
        $response_array["status"] = 1;
        $response_array["response"] = $res;
    }
    else {
        $response_array["status"]   = 1;
        $response_array["response"] = "";   
    }
    
}
else {
	$response_array["status"] = 0;
    $response_array["response"] = "Invalid Parameter";
}

echo json_encode($response_array);
?>