<?php
session_start();
$school_id 	= isset($_REQUEST['school_id']) ? $_REQUEST['school_id'] : "";


if($school_id != ""){
	$_SESSION['db_name'] = $school_id;

	include_once("../includes/config-frontend.php");

    $sql = "Select * from notification_master where status = 1 order by id desc";
    $db->query($sql);

    $rows = $db->fetch_object();

    $res_arr = array();
    $i = 0;
    if(count($rows) > 0){
        foreach($rows as $val){
            $res_arr[$i]["grade_id"]        =  $val->grade_id;
            $res_arr[$i]["section_id"]      = $val->section_id;
            $res_arr[$i]["notification_text"]   =  $val->notification_text;
            $i++;
        }
        $response_array["status"] = 1;
        $response_array["response"] = $res_arr;
    }
    else {
        $response_array["status"] = 1;
        $response_array["response"] = "";
    }
}
else {
	$response_array["status"] = 0;
    $response_array["response"] = "Invalid Parameter";
}
echo json_encode($response_array);
?>