<?php
session_start();

$school_id 	= isset($_REQUEST['school_id']) ? $_REQUEST['school_id'] : "";

$stud_id    = isset($_REQUEST['student_id']) ? $_REQUEST['student_id'] : "";
$title      = isset($_REQUEST['title']) ? $_REQUEST['title'] : "";
$desc       = isset($_REQUEST['reason']) ?  $_REQUEST['reason'] : "";
$leave_date = isset($_REQUEST['leave_date']) ?  $_REQUEST['leave_date'] : "";


if($school_id != "" && $leave_date!= "" && $stud_id!= "" && $title!= "" && $desc!= ""){
	$_SESSION['db_name'] = $school_id;

    $ids = explode("_",$school_id);
    $id = $ids[1];

	include_once("../includes/config-frontend.php");

    $sql = "Select count(*) As cnt from leave_application where student_id = '".$stud_id."' And leave_date = '".$leave_date."' ";
    $db->query($sql);
    $row = $db->fetch_object(MYSQL_FETCH_SINGLE);

    if($row->cnt <= 0){
        $arr = array(
                    "school_id"     => $id,
                    "student_id"    => $stud_id,
                    "title"         => $title,
                    "description"   => $desc,
                    "leave_date"    => $leave_date,
                    "posted_on"     => date("Y-m-d H:i:s"),
                    "status"        => 0,
                );

        $result = InsertData($arr,"leave_application");

        
        $response_array["status"] = 1;
        $response_array["response"] = "Success";
    }
    else {
        $response_array["status"]   = 1;
        $response_array["response"] = "You have already applied leave for this ".$leave_date;   
    }
}
else {
	$response_array["status"] = 0;
    $response_array["response"] = "Invalid Parameter";
}

echo json_encode($response_array);
?>