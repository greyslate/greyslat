<?php
session_start();
$school_id 	= isset($_REQUEST['school_id']) ? $_REQUEST['school_id'] : "";
$course_id 	= isset($_REQUEST['course_id']) ? $_REQUEST['course_id'] : "";
$grade_id 	= isset($_REQUEST['standard_id']) ? $_REQUEST['standard_id'] : "";
$section_id = isset($_REQUEST['section_id']) ? $_REQUEST['section_id'] : "";
$dob 		= isset($_REQUEST['dob']) ? $_REQUEST['dob'] : "";


if($school_id != "" && $course_id!= "" && $grade_id != "" && $section_id!= "" && $dob!=""){
	$_SESSION['db_name'] = $school_id;

	include_once("../includes/config-frontend.php");

	$sql = "Select * from student_master 
	            where status = 1 And course_id = '".$course_id."' And grade_id = '".$grade_id."' And section_id = '".$section_id."' And dob = '".$dob."'";

	$db->query($sql);
	$record = $db->fetch_object(MYSQL_FETCH_SINGLE);

	$response_array = array();

	if(count($record) > 0){
		$main_arr = array();

		if($record->profile_pic != ""){
			$profile_pic = STUDENT_URL.$record->profile_pic;
		}
		else {
			$profile_pic = "";
		}

        $main_arr["student_id"]     = $record->id;
		$main_arr["first_name"] 	= $record->fname;
		$main_arr["last_name"] 		= $record->lname;
		$main_arr["profile_pic"] 	= $profile_pic;
		$main_arr["email"] 			= $record->p_email;
		$main_arr["location"] 		= $record->address;
		$main_arr["phone"] 			= $record->p_contact;
		$main_arr["dob"] 			= $record->dob;
		$main_arr["gender"] 		= $record->gender;

		$response_array["status"] = 1;
	    $response_array["response"] = $main_arr;
	}
	else {
	    $response_array["status"] = 1;
	    $response_array["response"] = "";
	}

}
else {
	$response_array["status"] = 0;
    $response_array["response"] = "Invalid Parameter";
}

echo json_encode($response_array);
?>