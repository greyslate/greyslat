<?php
session_start();
$school_id 	= isset($_REQUEST['school_id']) ? $_REQUEST['school_id'] : "";
$student_id = isset($_REQUEST['student_id']) ? $_REQUEST['student_id'] : "";



if($school_id != "" && $student_id!= "" ){
	$_SESSION['db_name'] = $school_id;

	include_once("../includes/config-frontend.php");

	if (isset($_FILES['profile_pic']['name']) && $_FILES['profile_pic']['name'] != "") {

            //==== Image Uploading
            $RandomNum = time();
            $ImageName = str_replace(' ', '-', strtolower($_FILES['profile_pic']['name']));
            $ImageType = $_FILES['profile_pic']['type']; //"image/png", image/jpeg etc.
            $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
            $ImageExt = str_replace('.', '', $ImageExt);
            $ImageName = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
            $NewImageName = $school_id . '-' . $student_id . '.' . $ImageExt;
            move_uploaded_file($_FILES["profile_pic"]["tmp_name"], STUDENT_PATH . $NewImageName);
            
            $response_array["status"] = 1;
    		$response_array["response"] = STUDENT_URL.$NewImageName;

            $arr = array(
                            "profile_pic"   => $NewImageName,
                        );

            $where_arr = array("id" => $student_id);
            $data = UpdateData($arr,"student_master",$where_arr);
        }


}
else {
	$response_array["status"] = 0;
    $response_array["response"] = "Invalid Parameter";
}

echo json_encode($response_array);
?>