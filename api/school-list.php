<?php
    include("../includes/config.php");

    $school_name = isset($_REQUEST['school_name']) ? $_REQUEST['school_name'] : "";

    $where = '';
    if($school_name != ""){
    	$where = " And institute_name like '%".$school_name."%'";
    }

    $sql = "Select * from school_master as sm
    			inner join city_master as cm on cm.id = sm.city
	    			where sm.status = '1' ".$where." order by sm.institute_name";
    $db->query($sql);
    $record = $db->fetch_object();
    
    $response_array = array();

    if(count($record) > 0){
    	$school_arr = array();
    	$i = 0;
    	foreach($record as $val){
    		$school_arr[$i]["school_name"] 		= $val->institute_name;
    		$school_arr[$i]["school_database"]  = $val->database_name;
    		$school_arr[$i]["school_location"]  = $val->city_name;
    		$i++;
    	}
    	$response_array["status"] = 1;
    	$response_array["response"]	= $school_arr;
    }
    else {
    	$response_array["status"] = 1;
	    $response_array["response"] = "";
    }
    
    echo json_encode($response_array);
?>