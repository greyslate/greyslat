<?php
session_start();

$school_id 	= isset($_REQUEST['school_id']) ? $_REQUEST['school_id'] : "";
$stud_id    = isset($_REQUEST['student_id']) ? $_REQUEST['student_id'] : "";


if($school_id != "" && $stud_id!= ""){
	$_SESSION['db_name'] = $school_id;

	include_once("../includes/config-frontend.php");

    $sql = "Select * from appointment_master where student_id = '".$stud_id."' order by id desc ";
    $db->query($sql);
    $row = $db->fetch_object();

    $res = array();
    if(count($row) >= 0){
        
        $i = 0;
        foreach($row as $rval){
            $res[$i]["app_id"] = $rval->id;
            $res[$i]["app_title"] = $rval->title;
            $res[$i]["app_desc"] = $rval->description;
            $res[$i]["app_date"] = $rval->app_date;
            $res[$i]["app_time"] = $rval->app_time;
            $res[$i]["status"] = ($rval->status == 1) ? "Approved" : "Pending";
            $res[$i]["posted_on"] = $rval->posted_on;
            $i++;
        }
        
        $response_array["status"] = 1;
        $response_array["response"] = $res;
    }
    else {
        $response_array["status"]   = 1;
        $response_array["response"] = "";   
    }
    
}
else {
	$response_array["status"] = 0;
    $response_array["response"] = "Invalid Parameter";
}

echo json_encode($response_array);
?>