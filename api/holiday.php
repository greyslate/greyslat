<?php
session_start();

$school_id 	= isset($_REQUEST['school_id']) ? $_REQUEST['school_id'] : "";
//$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : "" ;

if($school_id != "") {
    $code = explode("GS-", $school_id);
    $_SESSION['db_name'] = "greyslat" . "_" . $code[1];
    global $db;

    include_once("../includes/config-frontend.php");
    //include_once("../includes/config-api1.php");

    //$_SESSION['db_name'] = $school_id;

    //include_once("../includes/config-frontend.php");

    $sql = "select * from holiday_master  ";
    $db->query($sql);

    $rows = $db->fetch_object();

    $res_arr = array();
    $i = 0;
    if(count($rows) > 0){
        foreach($rows as $val){
            $res_arr[$i]["id"]        =  $val->id;
            $res_arr[$i]["year"]        =  $val->year;
            $res_arr[$i]["school_id"]  =  $val->school_id;
            $res_arr[$i]["file_path"]     =  $val->file_path;
            $res_arr[$i]["file_name"]     =  $val->file_name;

            $res_arr[$i]["updated_on"]     =  $val->updated_on;

            $i++;
        }
        $response_array["status"] = 1;
        $response_array["response"] = $res_arr;
    }
    else {
        $response_array["status"] = 0;
        $response_array["message"] = "No data found";
    }
}
else {
    $response_array["status"] = 0;
    $response_array["message"] = "Invalid School ID";
}

echo json_encode($response_array);
?>