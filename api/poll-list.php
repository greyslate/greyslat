<?php
session_start();
$school_id 	= isset($_REQUEST['school_id']) ? $_REQUEST['school_id'] : "";


if($school_id != ""){
	$_SESSION['db_name'] = $school_id;

	include_once("../includes/config-frontend.php");

    $sql = "Select * from poll_master where status = 1 order by id desc";
    $db->query($sql);

    $rows = $db->fetch_object();

    $res_arr = array();
    $i = 0;
    if(count($rows) > 0){
        foreach($rows as $val){
            $res_arr[$i]["poll_id"]         =  $val->id;
            $res_arr[$i]["question_title"]  =  $val->question_title;

            $sql = "Select * from poll_detail where poll_id = '".$val->id."' order by id desc";
            $db->query($sql);
            $pdetail = $db->fetch_object();
            $o_arr = array();
            if(count($pdetail) > 0){
                $t = 0;
                foreach($pdetail as $sval){
                    $o_arr[$t]["option_id"]     = $sval->id;
                    $o_arr[$t]["option_title"]  = $sval->option_title;
                    $t++;
                }
            }

            $res_arr[$i]["poll_option"]  =  $o_arr;
            $i++;
        }
        $response_array["status"] = 1;
        $response_array["response"] = $res_arr;
    }
    else {
        $response_array["status"] = 1;
        $response_array["response"] = "";
    }
}
else {
	$response_array["status"] = 0;
    $response_array["response"] = "Invalid Parameter";
}

echo json_encode($response_array);
?>