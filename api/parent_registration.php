<?php
session_start();
include("../includes/config-api.php");
global $db;
$school_id = isset($_REQUEST['school_id']) ? $_REQUEST['school_id'] : "";
$_SESSION['db_name'] = $school_id;

if ($school_id) {
    $sql = "Select count(*) As cnt From parent_master Where email = '" . $_REQUEST['txtemail'] . "'";
    $db->query($sql);
    $crow = $db->fetch_object(MYSQL_FETCH_SINGLE);
    if ($crow->cnt > 0) {
        $response_array["status"] = 0;
        $response_array["response"] = "Email is already exits!";
    } else {
        if ($_REQUEST['txtemail']) {
            $arr = array(
                "fname" => $_REQUEST['txtfname'],
                "lname" => $_REQUEST['txtlname'],
                "email" => $_REQUEST['txtemail'],
                "password" => $_REQUEST['txtpassword'],
                "Mobile_No" => $_REQUEST['txtphone'],
                "created_on" => date("Y-m-d H:i:s"),
                "status" => isset($_REQUEST['status']) ? 1 : 0,
            );
            $result = InsertData($arr, "parent_master");
            $response_array["status"] = 1;
            $response_array["response"] = "Success";
        } else {
            $response_array["status"] = 0;
            $response_array["response"] = "Invalid Data.";
        }
    }
} else {
    $response_array["status"] = 0;
    $response_array["response"] = "Invalid School ID";
}
echo json_encode($response_array);
?>