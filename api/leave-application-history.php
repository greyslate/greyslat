<?php
session_start();

$school_id 	= isset($_REQUEST['school_id']) ? $_REQUEST['school_id'] : "";
$stud_id    = isset($_REQUEST['student_id']) ? $_REQUEST['student_id'] : "";


if($school_id != "" && $stud_id!= ""){
	$_SESSION['db_name'] = $school_id;

	include_once("../includes/config-frontend.php");

    $sql = "Select * from leave_application where student_id = '".$stud_id."' order by id desc ";
    $db->query($sql);
    $row = $db->fetch_object();

    $res = array();
    if(count($row) >= 0){
        
        $i = 0;
        foreach($row as $rval){

            if($rval->status == 1){
                $status = "Approved";
            }
            else if($rval->status == 2){
                $status = "Rejected";
            }
            else {
                $status = "Pending";
            }

            $res[$i]["app_id"]      = $rval->id;
            $res[$i]["title"]       = $rval->title;
            $res[$i]["reason"]      = $rval->description;
            $res[$i]["leave_date"]  = $rval->leave_date;
            $res[$i]["status"]      = $status;
            $res[$i]["posted_on"]   = $rval->posted_on;
            $i++;
        }
        
        $response_array["status"] = 1;
        $response_array["response"] = $res;
    }
    else {
        $response_array["status"]   = 1;
        $response_array["response"] = "";   
    }
    
}
else {
	$response_array["status"] = 0;
    $response_array["response"] = "Invalid Parameter";
}

echo json_encode($response_array);
?>