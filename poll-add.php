<?php
include("header.php");
?>
<script>
$(document).ready(function(){
    var str = '';
    var counter = 3;
   $("#addmore").on("click",function(){
       
       str = '<div id="dtag'+counter+'" class="formRow"><label for="title">Option - '+counter+'</label><span class="red">*</span><br /><input type="text" name="option[]" id="txtanswer'+counter+'" rows="3" cols="50" style="resize: none;" class="input-text required large" title="Please enter Option'+counter+'."></div>';
       $("#option_list").append(str);
       
       if(counter >=3){
           $("#removemore").show();
       }
       counter++;
       $("#counter").val(counter);
       
   });
   
   $("#removemore").on("click",function(){
       
       counter--;
       $("#dtag"+counter).remove();
       $("#counter").val(counter);
       if(counter <= 3){
           $("#removemore").hide();
       }
   });
   
});
</script>

<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Add Poll <small> add Poll information</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo SITE_URL; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="poll-list.php">Poll</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Add Poll</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Add Poll
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmadd" id="frmadd" enctype="multipart/form-data">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                You have some form errors. Please check below.
                                            </div>
                                          <div class="form-group">
                                            
                                            <label class="control-label col-md-3">Question Title <span class="required" aria-required="true">* </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtpollname" name="txtpollname" placeholder="Question Title" class="form-control required"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Option-1 <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtanswer1" name="option[]" placeholder="Optional" class="form-control required"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Option-2<span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtanswer2" name="option[]" placeholder="Optional" class="form-control required"/>
                                               </div>
                                           </div>
                                              <div class="form-group">
                                              <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="counter" id="counter" value="2" >
                                                <input type="button" id="addmore" value="Add More" class="button small radius">
                                                <input style="display:none;" type="button" id="removemore" value="Remove" class="button small radius">
                                               </div>
                                           </div>
                                          </div>
                                          
                                          </div>
                                          <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input type="hidden" name="action" value="add">
                                                    <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
                                                    <button type="button" onclick="location.href = 'poll-list.php'" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                     </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>

<?php
include("footer.php");
?>
<script src="<?php echo JS_URL; ?>script/poll.js" type="text/javascript" charset="utf-8"></script>
