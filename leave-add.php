<?php

include("header.php");

$cobj = new LeaveApplication();
$where = " And school_id = '" . $_SESSION['institute_id'] . "' ";
$data = $cobj->fetch_all($where);

$ltypeObj = new LeaveType();
$types = $ltypeObj->fetch_all();

$sobj = new Student();
$where1 = "And p_email = '".$_SESSION['email']."'";
$data = $sobj->fetch_student_list($where1);
//$student_list = FillCombo($student_obj->fetch_all(),"id", "fname", "");
?>
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Add Leave <small> add Leave title, date & description</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo SITE_URL; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="leave-application-list.php">Leave Application</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Add Leave</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="portlet box red ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Add Leave
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmadd" id="frmadd">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                You have some form errors. Please check below.
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Title <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtleavetitle" name="txtleavetitle" placeholder="Leave Title" class="form-control required"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Leave Type <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <select name="type_id" id="type_id" class="form-control required">
                                                        <option value="">--- SELECT TYPE ---</option>
                                                        <?php foreach($types as $type){ ?>
                                                            <option value="<?php echo $type->type_id ?>"><?php echo $type->type_title?></option>

                                                            <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <?php if($_SESSION['role_id'] != 2 ){ ?>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Student <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <select name="student_id" id="student_id" class="form-control required">
                                                        <option value="">--- SELECT STUDENT ---</option>
                                                        <?php foreach($data as $student){ ?>
                                                            <option value="<?php echo $student->id ?>"><?php echo $student->fname."(Roll No: ".$student->roll_no.")" ?></option>

                                                            <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Reason for leave <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <textarea id="txtleavedesc" name="txtleavedesc" class="form-control required" placeholder="Description"></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Leave Date <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input id="txtleavedateTo" name="txtleavedateTo" readonly="" class="form-control required date-picker" placeholder="YYYY-MM-DD">

                                                    <input id="txtleavedateFrom" name="txtleavedateFrom" readonly="" class="form-control required date-picker" placeholder="YYYY-MM-DD">

                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input type="hidden" name="action" value="add">
                                                    <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
                                                    <button type="button" onclick="location.href = 'leave-application-list.php'" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>

<?php
include("footer.php");
?>
<script src="<?php echo JS_URL; ?>script/leave-application.js" type="text/javascript" charset="utf-8"></script>
