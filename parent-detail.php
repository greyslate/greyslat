<?php
include("header.php");

session_start();

$emp_id= $_SESSION['emp_id'];
$val = $parent_obj->getParent($emp_id);



//$data = $cobj->fetch_all($where);
?>

<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        View Parent Detail <small> View Parent Detail</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">

                            <li>
                                <a href="#">View Parent Details</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE HEADER-->
                            <h3 class="page-title">
                                Manage Parent <small>view parent details</small>
                            </h3>
                            <div class="row">
                                <div class="portlet box red">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cogs"></i>Manage Parent
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-bordered table-hover">
                                            <tbody>
                                            <tr>
                                                <td align="left" width="15%">
                                                    ID
                                                </td>
                                                <td align="left">
                                                    <?php echo $val->id; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="15%">
                                                    Profile Pic
                                                </td>
                                                <td align="left">
                                                    <?php
                                                    if(!empty($val->profile_pic)){
                                                        ?>
                                                        <img src="<?php echo PARENT_URL.$val->profile_pic; ?>" width="100" />
                                                        <?php
                                                    }
                                                    else{
                                                        echo "Picture Not Avaialble";
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="15%">
                                                    Student Name
                                                </td>
                                                <td align="left">
                                                    <?php echo $val->fname . " " . $val->lname; ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="left" width="15%">
                                                    Email
                                                </td>
                                                <td align="left">
                                                    <?php echo $val->email; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="15%">
                                                    Address
                                                </td>
                                                <td align="left">
                                                    <?php echo $val->address; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="15%">
                                                    City
                                                </td>
                                                <td align="left">
                                                    <?php echo $val->city; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="15%">
                                                    State
                                                </td>
                                                <td align="left">
                                                    <?php echo $val->state; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="15%">
                                                    Contact
                                                </td>
                                                <td align="left">
                                                    <?php echo $val->Mobile_No; ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="left" width="15%">
                                                    Created On
                                                </td>
                                                <td align="left">
                                                    <?php echo $val->created_on; ?>
                                                </td>
                                            </tr>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END BORDERED TABLE PORTLET-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>

    </div>

    <?php
    include("footer.php");
    ?>