<?php
include("header.php");

//if (!isset($_GET['id'])) {
  //  header("location: index.php");
    //exit(0);
//}

$id = $_GET['id'];
$val = $school_obj->getSchool($id);

//$country_id = $val->country;
//$state_id = $val->state;
//$city_id = $val->city;
$type_list = fillArrayCombo($config["Institute_Type"], $val->institute_type);
?>

<!--<script>
    $(document).ready(function () {
        $("#frmedit").validate({
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            invalidHandler: function (event, validator) { //display error alert on form submit              
                $('.alert-danger-transparent').show();
                $('html, body').animate({
                    scrollTop: $("body").offset().top
                }, 2000);

            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('error'); // set error class to the control group
            },
            success: function (label) {
                label
                        .closest('.form-group').removeClass('error'); // set success class to the control group
            },
            submitHandler: function (form) {

                $("#loader").show();
                var formData = new FormData(form);

                $.ajax({
                    url: base_url + 'ajaxfiles/profile-edit.php',
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $("#loader").hide();
                        if (data.trim() == 'Yes') {
                            toastr.success("Record has been updated successfully", "Success!");
                            $("html, body").animate({scrollTop: 0}, 500);
                            setTimeout(function () {
                                window.location.href = "school-list.php"; //will redirect to your blog page (an ex: blog.html)
                            }, 1000);
                        }
                        else {
                            toastr.error(data, "Oops!");
                            return false;
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        $("#loader").hide();
                        toastr.error("Something went wrong.Please contact to Developer.", "Oops!");
                    }
                });

            }
        });
    });

</script>-->
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Edit School profile <small>edit school profile</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo SITE_URL; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            
                            <li>
                                <a href="#">Edit School Profile</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box red ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Edit School Information
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form name="frmedit" id="frmedit"  method="post" class="form-horizontal form-bordered form-label-stripped" enctype="multipart/form-data">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                You have some form errors. Please check below.
                                            </div>

                                            <div class="form-group">
                                            <label class="control-label col-md-3">Institute Code<span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input value="<?php  echo "GS-" . $_SESSION['institute_id'];?>" type="text" name="txtcode" id="txtcode" class="form-control required"  readonly="" placeholder="School Name">
                                                </div>
                                        </div>
                                        


                                        <div class="form-group">
                                            <label class="control-label col-md-3">School Name <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input value="<?php  echo $_SESSION['institute_name'];?>" type="text" name="txtinstitutename" id="txtinstitutename" class="form-control required" placeholder="School Name">
                                                </div>
                                        </div>
                                       
                                            
                                        <div class="form-group">
                                            <label class="control-label col-md-3">School Type <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <select name="school_type" id="school_type" class="selecter required">
                                                    <?php
                                                    echo $type_list;
                                                        ?>
                                                    </select>
                                                </div>
                                         </div>

                                        <div class="form-group">
                                           <label class="control-label col-md-3">Address <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <textarea name="txtaddress" id="txtaddress" class="form-control required" placeholder="Address"><?php echo $_SESSION['institute_address'];
                                                    ?></textarea>
                                                </div>
                                        </div>

                                    <!--    <div class="form-group">
                                            <label class="control-label col-md-3">City <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" name="city" id="city" class="form-control required" placeholder="City" value="<?php echo $val->city; ?>">
                                                </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-3">State <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" name="state" id="state" class="form-control required" placeholder="State" value="<?php echo $val->state; ?>">
                                                </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Country<span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" name="country" id="country" class="form-control required" placeholder="Country" value="<?php echo $val->country; ?>">
                                                </div>
                                        </div>-->
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Postal code<span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" name="txtpostalcode" id="txtpostalcode" class="form-control required" placeholder="Postal Code" value="<?php  echo $_SESSION['institute_postalcode'];?>">
                                                </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Phone No<span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" name="txtphone" id="txtphone" class="form-control required" placeholder="Phone No" value="<?php  echo $_SESSION['institute_phone'];?>">
                                                </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-3"> Email <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input type="email" name="txtemail" id="txtemail" class="form-control required" placeholder="Email" value="<?php  echo $_SESSION['institute_email'];?>">
                                                </div>
                                        </div>
                                        <input type="hidden" name="status" id="status" value="1" >    
                            <!--                            <div class="form-group">
                                                            <label class="control-label col-md-3">Status</label>
                                                            <div class="col-md-5">
                                                                <div class="radioer">
                                                                    <input type="radio" name="status" id="status" value="1" checked="checked">
                                                                    <label for="radioColor3">Active</label>
                                                                </div>
                                                                <div class="radioer">
                                                                    <input type="radio" name="status" id="status" value="0">
                                                                    <label for="radioColor4">De-active</label>
                                                                </div>
                                                            </div>
                                                        </div>-->
                                </div>
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <input type="hidden" name="action" value="edit">
                                            <input type="hidden" name="id" value="<?php echo $_SESSION['id']; ?>">
                                            <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
                                            <button type="button" 
                                            onclick="location.href = 'index.php'" class="btn default">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>


<?php
include("footer.php");

?>
<script src="<?php echo JS_URL; ?>script/profile.js" type="text/javascript" charset="utf-8"></script>