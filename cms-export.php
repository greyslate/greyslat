<?php

    include("includes/config-frontend.php");
    $objPHPExcel = new PHPExcel();
   
    $sql = "Select * From ".CMS_MASTER." order by id desc";


    $db->query($sql);

    $rows = $db->fetch_object();

    $i = 0;
    $main_arr = array();
    if(count($rows) > 0){
        foreach($rows as $val){


            $main_arr[$i]["name"]     = $val->name;
            $main_arr[$i]["alias"]      = $val->alias;
            $main_arr[$i]["content"]    = $val->content;
            
            $i++;
        }
    }


    $rowCount = 1;
    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "Name");
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Alias");
    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Content");



    $rowCount = 2;
    for($i=0;$i<count($main_arr);$i++){
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $main_arr[$i]['name']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $main_arr[$i]['alias']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $main_arr[$i]['content']);


        $rowCount++;
        //pr($objPHPExcel); 
    }

    header('Content-Type: application/vnd.openxmlformats-   officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.date("d-m-Y").'-cms.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

?>