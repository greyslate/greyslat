<?php

    include("includes/config-frontend.php");
    $objPHPExcel = new PHPExcel();
    $sql = "Select *,TT.id As id From ".RESULT_MASTER." As TT
                    Inner join ".COURSE_MASTER." AS CM on CM.id = TT.course_id   
                    Inner join ".GRADE_MASTER." AS GM on GM.id = TT.standard_id
                    Inner join ".STUDENT_MASTER." AS SM on SM.id = TT.student_id
                    Inner join ".EXAM_TIMETABLE." AS ET on ET.id = TT.exam_id
                    Inner join ".SECTION_MASTER." AS SE on SE.id = TT.section_id
                    Where 1=1 ".$where." Order By TT.id desc ";


    $db->query($sql);

    $rows = $db->fetch_object();

    $i = 0;
    $main_arr = array();
    if(count($rows) > 0){
        foreach($rows as $val){

           
            $main_arr[$i]["id"]     = $val->id;
            $main_arr[$i]["school_id"]      = $val->school_id;
            $main_arr[$i]["student_id"]    = $val->roll_no;
            $main_arr[$i]["student_name"]     = $val->fname . " " .$val->lname;
            $main_arr[$i]["standard_id"]    = $val->standard_name;
            $main_arr[$i]["section_id"]    = $val->section_name;
            $main_arr[$i]["exam_id"]    = $val->exam_title;
            $main_arr[$i]["course_id"]    = $val->course_name;

            $main_arr[$i]["obt_marks"]    = $val->obt_marks;
            $main_arr[$i]["total_marks"]    = $val->total_marks;
            $main_arr[$i]["passing_marks"]    = $val->passing_marks;
          
            
            $i++;
        }
    }


    $rowCount = 1;
    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "Id");
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "School Id");
    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Student Roll No");
    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, "Student Name");
    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, "Standard");
    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "Section");
    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, "Exam");
    $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, "Course");

    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, "Obtain Marks");
    $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, "Total Marks");
    $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, "Passing Marks");

    


    $rowCount = 2;
    for($i=0;$i<count($main_arr);$i++){
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $main_arr[$i]['id']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $main_arr[$i]['school_id']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $main_arr[$i]['student_id']);
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $main_arr[$i]['student_name']);
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $main_arr[$i]['standard_id']);
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $main_arr[$i]['section_id']);
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $main_arr[$i]['exam_id']);
        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $main_arr[$i]['course_id']);
        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $main_arr[$i]['obt_marks']);
        $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $main_arr[$i]['total_marks']);
        $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $main_arr[$i]['passing_marks']);

        

        $rowCount++;
        //pr($objPHPExcel); 
    }

    header('Content-Type: application/vnd.openxmlformats-   officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.date("d-m-Y").'-result.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

?>