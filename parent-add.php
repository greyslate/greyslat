<?php
include("header.php");

$school_list = FillCombo($school_obj->fetch_school_list(), "id", "institute_name", "");
?>
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Manage Parent <small>Parent basic & personal information</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo SITE_URL; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="parent-list.php">Parent</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Add Parent</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="portlet box red ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Add Parent
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmadd" id="frmadd">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                You have some form errors. Please check below.
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">First Name <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtfname" name="txtfname" placeholder="First Name" class="form-control required"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Last Name <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtlname" name="txtlname" placeholder="Last Name" class="form-control required"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Email <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="email" id="txtemail" name="txtemail" placeholder="Email" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Password <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="password" id="txtpassword" name="txtpassword" placeholder="Password" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Re-type Password <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="password" id="txtcpassword" name="txtcpassword" placeholder="Re-type Password" class="form-control required"/>
                                                </div>
                                            </div>

                                              <div class="form-group">
                                                <label class="control-label col-md-3">Mobile No <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtphone" name="txtphone" placeholder="Mobile No" class="form-control"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Address <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                  <textarea name="txtaddress" id="txtaddress" class="form-control required" placeholder="Address"></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">City <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="city" name="city" placeholder="city" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">State<span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="state" name="state" placeholder="state" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Profile Pic<span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="file" id="profile_pic" name="profile_pic" class="form-control required" placeholder="Upload only png jpeg file">
                                                </div>
                                            </div>

                                            <div class="form-actions">
                                                <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input type="hidden" name="action" value="add">
                                                    <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
                                                    <button type="button" onclick="location.href='parent-list.php'" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>
<?php
include("footer.php");
?>

<script src="<?php echo JS_URL; ?>script/parent1.js" type="text/javascript" charset="utf-8"></script>
