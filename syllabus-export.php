<?php

    include("includes/config-frontend.php");
    $objPHPExcel = new PHPExcel();
  // $sql = "Select * From ".SYLLABUS_MASTER." order by id desc";

$sql = "Select * From ".SYLLABUS_MASTER." As sm
                  INNER JOIN  " . COURSE_MASTER . " As CM on CM.id = sm.course_id 
                  INNER JOIN  " . GRADE_MASTER . " As GM on GM.id = sm.grade_id
                  order by sm.id desc";

    $db->query($sql);

    $rows = $db->fetch_object();

    $i = 0;
    $main_arr = array();
    if(count($rows) > 0){
        foreach($rows as $val){
            $main_arr[$i]["course"]     = $val->course_name;
            $main_arr[$i]["grade"]      = $val->standard_name;
            $main_arr[$i]["title"]      = $val->title;
            $main_arr[$i]["desc"]       = $val->description;
            $main_arr[$i]["created_on"] = $val->created_on;
            $i++;
        }
    }


    $rowCount = 1;
    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "Course Name");
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Grade");
   

    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Title");

    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, "Description");
   
    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, "Created On");


    $rowCount = 2;
    for($i=0;$i<count($main_arr);$i++){
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $main_arr[$i]['course']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $main_arr[$i]['grade']);
       

        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $main_arr[$i]['title']);

        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $main_arr[$i]['desc']);
       
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $main_arr[$i]['created_on']);

        $rowCount++;
        //pr($objPHPExcel); 
    }

    header('Content-Type: application/vnd.openxmlformats-   officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.date("d-m-Y").'-syllabus.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

?>