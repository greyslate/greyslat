<?php
include("includes/config.php");

if (isset($_SESSION['institute_id']) && $_SESSION['institute_id'] != "") {
    header("location: " . SITE_URL);
    exit(0);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>Greyslate - Admin Panel</title>
        <link rel="stylesheet" href="<?php echo CSS_URL; ?>loadingwindow.css" />
        <script type="text/javascript">
            var base_url = "<?php echo SITE_URL; ?>";
        </script>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="<?php echo THEME_URL; ?>global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo THEME_URL; ?>global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo THEME_URL; ?>global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo THEME_URL; ?>global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?php echo THEME_URL; ?>admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?php echo THEME_URL; ?>global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="<?php echo THEME_URL; ?>global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo THEME_URL; ?>admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo THEME_URL; ?>admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?php echo THEME_URL; ?>admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>

    </head>
    <body class="login" id="bodyid">
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="menu-toggler sidebar-toggler">
        </div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGO -->
        <div class="logo">
            <img width="120" src="<?php echo IMAGE_URL; ?>logo.png" />
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" method="post" id="frmlogin" name="frmlogin">
                <h3 class="form-title">Sign In</h3>
                <div id="failmsg" class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span>Invalid User Name/Password</span>
                </div>
               
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Code</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Code" id="txtcode" name="txtcode"/>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" id="txtlogin" name="txtlogin"/>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" id="txtpassword" name="txtpassword"/>
                </div>
                <div class="form-actions">
                    <button type="submit" name="Login" class="btn btn-success uppercase">Login</button>
                    <button type="button" onclick="location.href='sign_up.php'" class="btn btn-success uppercase"> Sign-Up</button>
                    <a href="forget_password.php" id="forget-password" class="forget-password">Forgot Password?</a>
                </div>
                <div class="create-account">
                    <p>
                        <a href="javascript:;" id="register-btn" class="uppercase">2016 © Greyslate.</a>
                    </p>
                </div>
            </form>
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
<!--            <form class="forget-form" action="index.html" method="post">
                <h3>Forget Password ?</h3>
                <p>
                    Enter your e-mail address below to reset your password.
                </p>
                <div class="form-group">
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" id="tbxEmailAddress" name="tbxEmailAddress"/>
                </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn btn-default">Back</button>
                    <button type="submit" onclick="javascript:forgotpassword();" class="btn btn-success uppercase pull-right">Submit</button>
                </div>
            </form>-->
            <!-- END FORGOT PASSWORD FORM -->
        </div>

        <!-- END LOGIN -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="<?php echo THEME_URL; ?>global/plugins/respond.min.js"></script>
        <script src="<?php echo THEME_URL; ?>global/plugins/excanvas.min.js"></script> 
        <![endif]-->
        <script src="<?php echo THEME_URL; ?>global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo THEME_URL; ?>global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="<?php echo THEME_URL; ?>global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo THEME_URL; ?>global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo THEME_URL; ?>global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="<?php echo THEME_URL; ?>global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo THEME_URL; ?>global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo THEME_URL; ?>global/scripts/metronic.js" type="text/javascript"></script>
        <script src="<?php echo THEME_URL; ?>admin/layout/scripts/layout.js" type="text/javascript"></script>
        <script src="<?php echo THEME_URL; ?>admin/layout/scripts/demo.js" type="text/javascript"></script>
        <script src="<?php echo THEME_URL; ?>admin/pages/scripts/login.js" type="text/javascript"></script>
        <script src="<?php echo JS_URL; ?>jquery-base64-master/jquery.base64.js" type="text/javascript"></script>
        <script src="<?php echo JS_URL; ?>jquery-base64-master/jquery.base64.min.js" type="text/javascript"></script>
        <script src="<?php echo JS_URL; ?>loading.js" type="text/javascript"></script>
        <script src="<?php echo JS_URL; ?>script/login.js" type="text/javascript"></script>                    

        <!-- END PAGE LEVEL SCRIPTS -->
        <script>
//                        jQuery(document).ready(function () {
//                            Metronic.init(); // init metronic core components
//                            Layout.init(); // init current layout
//                            Login.init();
//                            Demo.init();
//
//
//                        });

                        $(document).ready(function () {
                            $("#frmlogin").on('submit', (function (e) {
                                e.preventDefault();

                                $.ajax({
                                    url: '<?php echo SITE_URL; ?>ajaxfiles/ajaxlogin.php',
                                    type: "POST",
                                    data: new FormData(this),
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    success: function (data) {
                                        if (data != 1) {

                                            $("#failmsg").show()
                                            var arr = data.split("#");
                                            $("#failmsg").html(arr[1]);
                                            $("#failmsg").fadeIn(3000, function () {
                                                $("#failmsg").fadeOut(5000);
                                            });
                                            //$("#bodyid").hideLoading();
                                        }
                                        else {
                                            //$("#bodyid").hideLoading();
                                            window.location = base_url;
                                        }
                                    },
                                    error: function () {
                                    }
                                });

                            }));
                        })
        </script>
        <!-- END JAVASCRIPTS -->
    </body>

</html>

