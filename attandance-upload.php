<?php
include("header.php");
//$val = $assign_obj->getAssignment(1);
////$val = $holiday_obj->getHoliday(1);
//$grade_list = FillCombo($grade_obj->fetch_grade_list(), "id", "standard_name", "");
//$course_list = FillCombo($course_obj->fetch_course_list(), "id", "course_name", "");
//$section_list = FillCombo($section_obj->fetch_section_list(), "id", "section_name", "");
//$emp_list = FillCombo($emp_obj->fetch_Employee_list(),"id", "fname", "");
?>
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Attandance List <small> upload Attandance list file</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo SITE_URL; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="attandance-list.php">Attandance List</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="portlet box red ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Upload Attandance List
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmedit" id="frmedit">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                You have some form errors. Please check below.
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Roll No <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtroll" name="txtroll" placeholder="Roll No " class="form-control required" onblur="getname(this->value)" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Student Name<span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtfname" name="txtfname" placeholder="Student Name " class="form-control required" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Date <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input id="txtdate" name="txtdate" readonly="" class="form-control required date-picker" placeholder="YYYY-MM-DD">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Present <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <select class="form-control required" name="present" id="present">
                                                        <option value="">----Select Attandance----</option>
                                                        <option value="Present">Present</option>
                                                        <option value="Half Day">Half Day</option>
                                                        <option value="Absent">Absent</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <input type="hidden" name="action" value="add">
                                                        <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>
<?php
/*include("footer.php");
*/?><!--
<link rel="stylesheet" href="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.css">
<script src="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.js"></script>
<script src="<?php /*echo JS_URL; */?>script/attandance.js" type="text/javascript" charset="utf-8"></script>-->
<?php
include("footer.php");
?>
<script src="<?php echo JS_URL; ?>script/attandance.js" type="text/javascript" charset="utf-8"></script>
<script>
    function getname()
    {
        var student_roll_no=$("#txtroll").val();
        $.ajax({
            type:"post",
            dataType:"json",
            data:"student_roll_no="+student_roll_no,
            url:"get_attandance.php",
            success:function(json)
            {
                $("#txtroll").val(json);
                $("#txtfname").val(json);

            }
        });
    }