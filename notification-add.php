<?php
include("header.php");

$grade_list  = FillCombo($grade_obj->fetch_grade_list(), "id", "standard_name", "");
$section_list = FillCombo($section_obj->fetch_section_list(), "id", "section_name", "");
$student_list = FillCombo($student_obj->fetch_student_list(), "id", "fname","");
?>
?>

<!-- include js and css -->
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Add Notification
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo SITE_URL; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="notification-list.php">Notification</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Add Notification</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="portlet box red ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Add Notification
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmadd" id="frmadd">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                You have some form errors. Please check below.
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Grade/Standard <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <select name="grade_id" id="grade_id" class="form-control required" title="Please select Grade/Standard">
                                                    <?php
                                                        echo $grade_list;
                                                    ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Section <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <select name="section_id" id="section_id" class="form-control required" placeholder="Please select Section">
                                                    <?php
                                                        echo $section_list;
                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Student Name <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <select name="student_id" id="student_id" class="form-control required" placeholder="Please select Section">
                                                    <?php
                                                        echo $student_list;
                                                    ?>
                                                    </select>
                                                   
                                                </div>
                                            </div>
                                            

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Notification Message<span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <textarea id="txtmsg" name="txtmsg" class="form-control required" placeholder="Notification Message"></textarea>
                                                </div>
                                        </div>
                                   </div>
                                  <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input type="hidden" name="action" value="add">
                                                    <button type="submit" class="btn green"><i class="fa fa-check"></i>Send Notification</button>
                                                    <button type="button" onclick="location.href = 'notification-list.php'" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>

<?php
include("footer.php");
?>
<script src="<?php echo JS_URL; ?>script/send_notification.js" type="text/javascript" charset="utf-8"></script>