<?php
include("header.php");
if (!isset($_GET['id'])) {
    header("location: timetable-list.php");
    exit(0);
}
$id = $_GET['id'];
$val = $timetable_obj->getTimeTable($id);

$grade_list = FillCombo($grade_obj->fetch_grade_list(), "id", "standard_name", $val->grade_id);
$course_list = FillCombo($course_obj->fetch_course_list(), "id", "course_name", $val->course_id);
$section_list = FillCombo($section_obj->fetch_section_list(), "id", "section_name", $val->section_id);
$emp_list = FillCombo($emp_obj->fetch_Employee_list(), "id", "fname", "");
?>
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Edit Timetable <small> update timetable information</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo SITE_URL; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="timetable-list.php">Timetable</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Edit Timetable</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="portlet box red ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Edit Timetable
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmadd" id="frmadd" enctype="multipart/form-data" >
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                You have some form errors. Please check below.
                                            </div>
                                            

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Grade/Standard <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <select class="form-control required" name="grade_id" id="course_id">
                                                        <?php echo $grade_list; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Section <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <select class="form-control required" name="section_id" id="section_id">
                                                        <?php echo $section_list; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Medium <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <select class="form-control required" name="medium" id="medium">
                                                        <option value="English" <?php if($val->medium == 'English'){ ?> selected="selected" <?php } ?> >English</option>
                                                        <option value="Gujarati" <?php if($val->medium == 'Gujarati'){ ?> selected="selected" <?php } ?>>Gujarati</option>
                                                    </select>
                                                </div>
                                            </div>

                                             

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Upload File for Import</label>
                                                <div class="col-md-9">
                                                    <input type="file" name="file_name_import" id="file_name_import" class="form-control large" >
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3"></label>
                                                <div class="col-md-9">
                                                    <?php
                                                    if (file_exists(SAMPLE_TIMETABLE_PATH . 'sample-timetable.csv')) {
                                                        ?>
                                                        <a href="<?php echo SAMPLE_TIMETABLE_URL . 'sample-timetable.csv'; ?>" target="_blank">Download Sample File</a>
                                                        <?php
                                                    }
                                                    ?>    
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Upload File <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input type="file" name="file_name" id="file_name" class="form-control large required" placeholder="only xlsx format allowed" >
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3"></label>
                                                <div class="col-md-9">
                                                    <a target="_blank" href="<?php echo TIMETABLE_URL . $val->file_name; ?>">View File</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input type="hidden" name="action" value="edit">
                                                    <input type="hidden" name="oimg" value="<?php echo $val->file_name; ?>">
                                                    <input type="hidden" name="id" value="<?php echo $val->id; ?>"> 
                                                    <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
                                                    <button type="button" onclick="location.href = 'timetable-list.php'" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>

<?php
include("footer.php");
?>
<link rel="stylesheet" href="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.css">
<script src="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.js"></script>
<script src="<?php echo JS_URL; ?>script/timetable.js" type="text/javascript" charset="utf-8"></script>
