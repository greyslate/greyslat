<?php
include("header.php");

$cobj = new LeaveApplication();
$emp_id = $_SESSION['emp_id'];
if($_SESSION['role_id'] != 1 ) {
    $where = "And teacher_id = '" . $_SESSION['emp_id'] . "' OR parent_id = '" . $_SESSION['emp_id'] . "'  ";
//$where = "And parent_id = '".$_SESSION['emp_id']."'";
}
$data = $cobj->fetch_all($where);

?>

<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
             <div class="row">
                <div class="col-md-12">
                    <div class="portlet box red ">
                        <div class="portlet-title">
                            <div class="caption">
                                Search
                            </div>
                        </div>
                        <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                            <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmSearch" id="frmSearch">

                                    <div class="form-body">
                                   <div class="form-group">
                                        <label class="control-label col-md-3">Title</label>
                                        <div class="col-md-9">
                                            <input type="text" id="tbxTitle" name="tbxTitle" placeholder="Name" class="form-control "/>
                                        </div>
                                    </div>
                                        <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Date </label>
                                            <div class="col-md-9">
                                                <input id="start_date" name="start_date" readonly="" class="form-control date-picker" placeholder="YYYY-MM-DD">
                                                <input id="end_date" name="end_date" readonly="" class="form-control date-picker" placeholder="YYYY-MM-DD">
                                            </div>
                                        </div>
                                        </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Status</label>
                                        <div class="col-md-9">
                                            <select id="drpsearch_status" class="form-control " name="drpsearch_status" >
                                                <option value="">---Select---</option>
                                                <option value="1">Approved</option>
                                                <option value="0">Rejected</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn green" name="btnSearch" id="btnSearch" type="submit" onclick="return Search_Data();"><i class="fa fa-check"></i> Search</button>
                                            <button type="button" type="reset" id="btnReset" class="btn default">Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>       
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Manage Leave <small>view Leave</small>
                    </h3>
                    <div class="row">
                        <form id="frmtable" name="frm" method="post"  enctype="multipart/form-data" >
                            <div class="col-md-12">
                                <div class="right" style="float:right;">
                                    <a href="leave-add.php" class="btn btn-primary">
                                        Add New <i class="fa fa-plus"></i>
                                    </a>
                                    <a href="leave-export.php" class="btn red">Export Data</a>
                                </div>
                                <br><br><br>

                                <!-- BEGIN BORDERED TABLE PORTLET-->
                                <div class="portlet box red">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cogs"></i>Manage Leave
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div id="page_listing">
                                            <table class="table table-bordered table-striped table-condensed flip-content" >
                                                <thead>
                                                    <tr>
                                                        <th class="chb_col" width="1%" >
                                                            <input type="checkbox" name="checkall" onclick="javascript:checkAlluncheckAll(frm);">
                                                        </th>
                                                        <th width="20%">
                                                            Title
                                                        </th>
                                                         <th width="20%">
                                                            Reason For Leave
                                                        </th>
                                                        <th>
                                                            Leave Date
                                                        </th>
                                                        <?php if($_SESSION['role_id'] == 1 ){ ?>
                                                            <th>
                                                                Role
                                                            </th>
                                                        <?php } ?>
                                                        <th>
                                                            Posted On
                                                        </th>
                                                        <th>
                                                            Updated On
                                                        </th>

                                                        <th class="center" style="text-align: center;">
                                                            Status
                                                        </th>
                                                        <th width="10%" style="text-align: center;">
                                                            Action
                                                        </th>
                                                    </tr>
                                                </thead>

                                                <tbody id="content ui-sortable">

                                                    <?php
                                                    if (count($data) == 0) {
                                                        ?>
                                                        <tr><td colspan="8" align="center">No Result found</td></tr>
                                                        <?php
                                                    } else {
                                                        foreach ($data as $val) {
                                                            ?>
                                                            <tr>
                                                                <td class="chb_col">
                                                                    <input type="checkbox" name="id[]" value="<?php echo $val->id; ?>">
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->title; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->description; ?>
                                                                </td>
                                                                <td>
                                                                   <?php echo $val->leave_date_to . "  To  " . $val->leave_date_from; ?>
                                                                </td>
                                                                <?php if($_SESSION['role_id'] == 1 ){ ?>
                                                                    <td>
                                                                        <?php /*if ($cobj["student_id"] == 10 ){
                                                                            echo "Teacher";
                                                                        } */
                                                                        if ($val->student_id == NULL && $val->parent_id == NULL) {
                                                                            echo "Teacher";
                                                                        }else

                                                                {
                                                                    echo "Parent";
                                                                }?>
                                                                    </td>
                                                                <?php } ?>
                                                                <td>
                                                                    <?php echo $val->posted_on; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->updated_on; ?>
                                                                </td>

                                                                <?php if($_SESSION['role_id'] == 1 ) { ?>
                                                                <td id="td_status_<?php echo $val->id; ?>" style="text-align: center;">


                                                                    <a href="javascript:;" onclick="ChangeStatus('<?php echo $val->status; ?>', '<?php echo $val->id; ?>')"

                                                                       title='<?php echo ($val->status == 1 ? "Approved" : "Pending"); ?>'>

                                                                        <img  src='<?php echo IMAGE_URL; ?><?php
                                                                        echo ($val->status == 1 ? "ico-1.gif" : "ico-0.gif");
                                                                        ?>

                                                                              ' alt='<?php echo ($val->status == 1 ? "Approved" : "Pending"); ?>'

                                                                              title='<?php echo ($val->status == 1 ? "Approved" : "Pending"); ?>' />

                                                                    </a>

                                                                </td>
                                                                <?php } ?>
                                                                <?php if($_SESSION['role_id'] != 1 ) { ?>
                                                                    <td id="td_status_<?php echo $val->id; ?>" style="text-align: center;">


                                                                       <!-- <a href="javascript:;" onclick="ChangeStatus('<?php /*echo $val->status; */?>', '<?php /*echo $val->id; */?>')"-->

                                                                           <!--title='<?php /*echo ($val->status == 1 ? "" : ""); */?>'>-->

                                                                            <img  src='<?php echo IMAGE_URL; ?><?php
                                                                            echo ($val->status == 1 ? "ico-1.gif" : "ico-0.gif");
                                                                            ?>

                                                                              ' alt='<?php echo ($val->status == 1 ? "Approved" : "Pending"); ?>'

                                                                                  title='<?php echo ($val->status == 1 ? "Approved" : "Pending"); ?>' />

                                                                        <!--</a>-->

                                                                    </td>
                                                                <?php } ?>
                                                                <td class="content_actions" style="text-align: center;">
                                                                    <a href="javascript:;" onclick="javascript:RemoveRecord('<?php echo $val->id; ?>')" title='Delete'>
                                                                        <img src="<?php echo IMAGE_URL; ?>ico/trashcan_gray.png" alt="" /></a> <a href="leave-edit.php?id=<?php echo $val->id; ?>" class="sepV_a" title="Edit">
                                                                        <img src="<?php echo IMAGE_URL; ?>ico/pencil_gray.png" alt="" />
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <?php

                                                        }
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                            <?php

                                            include("pagination.php"); ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- END BORDERED TABLE PORTLET-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>

<?php
include("footer.php");
?>
<script src="<?php echo JS_URL; ?>script/leave-application.js" type="text/javascript" charset="utf-8"></script>
