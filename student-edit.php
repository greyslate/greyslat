<?php
include("header.php");

if (!isset($_GET['id'])) {
    header("location: student-list.php");
    exit(0);
}

$id = $_GET['id'];
$val = $student_obj->getStudent($id);


$grade_list = FillCombo($grade_obj->fetch_grade_list(), "id", "standard_name", $val->grade_id);
$course_list = FillCombo($course_obj->fetch_course_list(), "id", "course_name", $val->course_id);
$section_list = FillCombo($section_obj->fetch_section_list(), "id", "section_name", $val->section_id);
$parent_list = FillCombo($parent_obj->fetch_parent_list(), "id", "fname", $val->parent_id);
?>
<!-- include js and css -->
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Edit Student <small> edit student basic & personal information</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo SITE_URL; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="student-list.php">Student</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Edit Student</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Edit Student
                                    </div>
                                    <div class="tools">
                                        (*) required fields.
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="horizontal-form" method="post" name="frmadd" id="frmadd" enctype="multipart/form-data">
                                        <div class="form-body">
                                            <h3 class="form-section">Person Info</h3>
                                            <div class="row">

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Roll No <span class="required" aria-required="true">* </span></label>
                                                        <input value="<?php echo $val->roll_no;?>" type="text" name="txtrollno" id="txtrollno" class="form-control" placeholder="Roll No">
                                                        
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">First Name <span class="required" aria-required="true">* </span></label>
                                                        <input value="<?php echo $val->fname; ?>" type="text" name="txtfname" id="txtfname" class="form-control" placeholder="First Name">

                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            Middle Name
                                                            <span class="required" aria-required="true">* </span>
                                                        </label>
                                                        <input value="<?php echo $val->mname; ?>" type="text" id="txtmname" name="txtmname" class="form-control" placeholder="Middle Name">

                                                    </div>
                                                </div>
                                                <!--/span-->
                                                </div>

                                                <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Last Name <span class="required" aria-required="true">* </span></label>
                                                        <input value="<?php echo $val->lname; ?>" type="text" id="txtlname" name="txtlname" class="form-control" placeholder="Last Name">
                                                    </div>
                                                </div>
                                                
                                                   <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Parent Name <span class="required" aria-required="true">* </span></label>
                                                        <select class="form-control required" name="parent_id" id="parent_id">
                                                            <?php echo $parent_list; ?>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Email <span class="required" aria-required="true">* </span></label>
                                                        <input value="<?php echo $val->p_email; ?>" type="email" id="txtpemail" name="txtpemail" class="form-control required" placeholder="Email">
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Phone <span class="required" aria-required="true">* </span></label>
                                                        <input value="<?php echo $val->p_contact; ?>" type="text" id="txtphone1" name="txtphone1" class="form-control required" placeholder="Phone No">
                                                    </div>
                                                </div>

                                          
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Gender</label>
                                                        <select class="form-control" name="gender" id="gender">
                                                            <option <?php if($val->gender == "Male") { echo "selected"; } ?> value="Male">Male</option>
                                                            <option <?php if($val->gender == "Female") { echo "selected"; } ?> value="Female">Female</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Date of Birth <span class="required" aria-required="true">* </span></label>
                                                        <input type="text" value="<?php echo $val->dob; ?>" id="txtdob" name="txtdob" readonly="" class="form-control date-picker required" placeholder="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                </div>

                                                <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Profile Pic</label>
                                                        <input type="file" id="file_name" name="file_name" class="form-control" placeholder="Upload Pic">
                                                        <?php
                                                        if($val->profile_pic != ""){
                                                            ?>
                                                        <br>
                                                        <img src="<?php echo STUDENT_URL.$val->profile_pic; ?>" width="100" />
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            
                                            <h3 class="form-section">Other Information</h3>
                                            <div class="row">
                                                <div class="col-md-12 ">
                                                    <div class="form-group">
                                                        <label class="control-label">Address <span class="required" aria-required="true">*</span></label>
                                                        <input value="<?php echo $val->address; ?>" type="text" class="form-control required" name="txtaddress" id="txtaddress">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Course <span class="required" aria-required="true">*</span></label>
                                                        <select class="form-control required" name="course_id" id="course_id">
                                                            <?php echo $course_list; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Grade <span class="required" aria-required="true">*</span></label>
                                                        <select class="form-control required" name="grade_id" id="grade_id">
                                                            <?php echo $grade_list; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Section <span class="required" aria-required="true">*</span></label>
                                                        <select class="form-control required" name="section_id" id="section_id">
                                                            <?php echo $section_list; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="form-actions right">
                                            <input type="hidden" name="action" value="edit">
                                            <input type="hidden" name="id" id="id" value="<?php echo $_REQUEST['id']; ?>">
                                            <input type="hidden" name="oimg" id="oimg" value="<?php echo $val->profile_pic; ?>" />
                                            <button type="button" onclick="location.href = 'student-list.php'" class="btn default">Cancel</button>
                                            <button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
                                        </div>

                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>

<?php
include("footer.php");
?>
<script src="<?php echo JS_URL; ?>script/student.js" type="text/javascript" charset="utf-8"></script>
