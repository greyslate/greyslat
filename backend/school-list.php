<?php
include("header.php");

$cobj = new School();
$data = $cobj->fetch_all();
?>
<style>
    .image 
    {
        height:480px;
        width:640px;
        overflow: hidden;
    }
    .image img
    {
        width:640px; 
        height:480px;;
        margin:auto;
        display:block;
    }
</style>
<!-- include js and css -->
<script src="<?php echo JS_URL; ?>script/school.js" type="text/javascript" charset="utf-8"></script>
<div class="content">

    <div class="page-header full-content bg-blue">
        <div class="row">
            <div class="col-sm-6">
                <h1>School Management <small>view school</small></h1>
            </div><!--.col-->
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="#"><i class="ion-home"></i></a></li>
                    <li><a href="index.php">Dashboard</a></li>
                    <li><a href="#" class="active">View School</a></li>
                </ol>
            </div><!--.col-->
        </div><!--.row-->
    </div><!--.page-header-->
    
    <div class="row">
        <div class="col-md-12">
            <a href="school-add.php" title="Add School" class="btn btn-floating btn-red btn-ripple"><i class="ion-android-add"></i></a>
        </div>
    </div>
    <br>
    <div class="row">
        
        <?php
        if (empty($data)) {
            ?>
            <div class="col-md-12">
                No School Found
            </div>
            <?php
        } else {
            foreach ($data as $val) {
                ?>
                <div class="col-md-4">
                    <div class="card card-meal card-meal-red card-square card-shadow">
                        <div class="card-heading">
                            <h3 class="card-title" style="font-size:25px;">
                                <?php echo $val->institute_name; ?>
                                
                                <span style="float:right; font-size: 18px;">GS-<?php echo $val->id; ?></span>
                            </h3>
                            <div class="card-subhead">
                                
                                <?php
                                $date=date_create($val->reg_date);
                                $reg_date = date_format($date,"d F, Y");
                                ?>
                                Created On <a href="#"><?php echo $reg_date; ?></a>
                            </div><!--.card-subhead-->
                        </div><!--.card-heading-->
                        <div class="card-body">
                            <div class="image">
                                <?php
                                $image_path = UPLOAD_PATH . $val->school_path . $val->institute_logo;
                                $image_url = UPLOAD_URL . $val->school_path . $val->institute_logo;
                                if (file_exists($image_path) && !empty($val->institute_logo)) {
                                    ?>
                                    <img src="<?php echo $image_url; ?>" />
                                    <?php
                                } else {
                                    ?>
                                    <img width="640" src="<?php echo IMAGE_URL; ?>school_image.png" />
                                    <?php
                                }
                                ?>
                            </div><!--.image-->
                            <ul class="users">
                                <li>
                                    <?php echo $val->school_description; ?>
                                </li>
                            </ul>
                        </div><!--.card-body-->
                        <div class="card-footer" style="font-size: 20px;">
                            <ul>
                                <li>
                                    <a style="color:white" href="school-edit.php?id=<?php echo $val->id; ?>" class="btn btn-floating btn-red btn-ripple"><i class="ion-android-create"></i></a>
                                    
                                </li>
                                
                            </ul>
                        </div><!--.card-footer-->
                    </div><!--.card-->
                </div>


                <?php
            }
        }
        ?>
    </div><!--.row-->

    <div class="footer-links margin-top-40">
        <div class="row no-gutters">
            <div class="col-xs-6 bg-indigo">
                <a href="index.php">
                    <span class="state">Dashboard</span>
                    <span>Homepage</span>
                    <span class="icon"><i class="ion-android-arrow-back"></i></span>
                </a>
            </div><!--.col-->
            <div class="col-xs-6 bg-blue">
                <a href="school-add.php">
                    <span class="state">School</span>
                    <span>Add School</span>
                    <span class="icon"><i class="ion-android-arrow-forward"></i></span>
                </a>
            </div><!--.col-->
        </div><!--.row-->
    </div><!--.footer-links-->

</div><!--.content-->

<?php
include("sidebar.php");
include("footer.php");
?>   

