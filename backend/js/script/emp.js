// JavaScript Document
$(document).ready(function(e) {
    // All select box will convert chosen selectbox style
    $("selector").chosen();
    

    // Search box toggle 
    $(".bAct_toggle").click(function() {
        $(this).parent().next().slideToggle();
        var htmlsrc = $(this).find("img");
        if (htmlsrc.hasClass('bAct_minus') == true){
            htmlsrc.removeClass('bAct_minus');
            htmlsrc.addClass('bAct_plus');
        }
        else{
            htmlsrc.removeClass('bAct_plus');
            htmlsrc.addClass('bAct_minus');
        }
    });

    $("#btnSearch").click(function() {
        setClass();
        $("#tbxTitle").val('');
        $(".filtercourse .active").removeClass("active");
    });
    
    $("#btnReset").click(function(){
        //$("#bodyid").showLoading();
        setClass();
        $.ajax({
            url: base_url+'ajaxfiles/emp-list.php',
            type: 'POST',
            data: '',
            success: function(data){	
               // $("#bodyid").hideLoading();
                $('#page_listing').html(data);
            }
        });
    });
    
    
    
    //$('.on_off_activestatus :checkbox').iphoneStyle({ 
//        checkedLabel: 'Yes', 
//        uncheckedLabel: 'No',
//        onChange: function(elem, value) {
//            if(value==false){
//                $("#status").val("0");  
//            }
//            else{
//                $("#status").val("1");  	
//            }
//        }
//    });
    
    $("#frmadd").on('submit',(function(e){
        e.preventDefault();

       // $("#bodyid").showLoading();

        var error = false;
        var errortext=''
                
        
        var $inputs = $('#frmadd :input');
        $inputs.each(function(){
            if($(this).hasClass('required')){
                if($(this).val()=='' && $(this).attr('name') != "content"){//alert(here1);
                    error = true;
                    errortext += '<li>- '+$(this).attr('title')+'</li>';
                    $(this).closest('.desc_div').show();
                }
            }
        });
        
       // if($("#txtemail").val() != ""){
         //   if(verifyMail1($("#txtemail").val()) == false){
          //      error = true;
           ///     errortext += '<li>- Please Enter valid Email.</li>';
            //}
       // }
        
        if($("#txtpassword").val()!='' && $("#txtcpassword").val()!=''){
            if($("#txtpassword").val()!=$("#txtcpassword").val()){
                error = true;
                errortext += '<li>- Password and Re-type Password are not same.</li>';
            }
        }
        
        
			
        if(error==true){//alert(here3);
            $("#error_div").html('<ul><li>Please correct the following errors</li>'+errortext+'</ul>'); 
            $("#error_div").show();									
            //$("#error_div").attr("tabindex",-1).focus();
            $("html, body").animate({ scrollTop: 0 }, 500);
            //$("#bodyid").hideLoading();
            return false;	
        }
        else {
           
            $.ajax({
                        url: base_url+'ajaxfiles/emp-addedit.php',
                        type: "POST",
                        data:  new FormData(this),
                        contentType: false,
                        cache: false,
                        processData:false,
                        success: function(data){
                          //  $("#bodyid").hideLoading();
                            if(data.trim() == 'Yes'){
                                $("#divMsg").html("Record has been added successfully."); 
                                $("#divMsg").show();
                                $("html, body").animate({ scrollTop: 0 }, 500);
                                setTimeout(function () {
                                    window.location.href = "emp-list.php"; //will redirect to your blog page (an ex: blog.html)
                                 }, 1000);
                            }
                            else {
                                $("#error_div").html(data); 
                                $("#error_div").show();
                                $("html, body").animate({ scrollTop: 0 }, 500);
                                return false;
                            }
                        },
                        error: function(){} 	        
                });
            }
    }));
    
    
    $("#frmedit").on('submit',(function(e){
        e.preventDefault();

       // $("#bodyid").showLoading();

        var error = false;
        var errortext=''
        var $inputs = $('#frmedit :input');
        $inputs.each(function(){
            if($(this).hasClass('required')){
                if($(this).val()=='' && $(this).attr('name') != "content"){//alert(here1);
                    error = true;
                    errortext += '<li>- '+$(this).attr('title')+'</li>';
                    $(this).closest('.desc_div').show();
                }
            }
        });
        
        if($("#txtemail").val() != ""){
            if(verifyMail1($("#txtemail").val()) == false){
                error = true;
                errortext += '<li>- Please Enter valid Email.</li>';
            }
        }
			
        if(error==true){//alert(here3);
            $("#error_div").html('<ul><li>Please correct the following errors</li>'+errortext+'</ul>'); 
            $("#error_div").show();									
            //$("#error_div").attr("tabindex",-1).focus();
            $("html, body").animate({ scrollTop: 0 }, 500);
           // $("#bodyid").hideLoading();
            return false;	
        }
        else {
           
            $.ajax({
                        url: base_url+'ajaxfiles/emp-addedit.php',
                        type: "POST",
                        data:  new FormData(this),
                        contentType: false,
                        cache: false,
                        processData:false,
                        success: function(data){
                            //$("#bodyid").hideLoading();
                            if(data.trim() == 'Yes'){
                                $("#divMsg").html("Record has been updated successfully."); 
                                $("#divMsg").show();
                                $("html, body").animate({ scrollTop: 0 }, 500);
                                setTimeout(function () {
                                    window.location.href = "emp-list.php"; //will redirect to your blog page (an ex: blog.html)
                                 }, 1000);
                            }
                            else {
                                $("#error_div").html(data); 
                                $("#error_div").show();
                                $("html, body").animate({ scrollTop: 0 }, 500);
                                return false;
                            }
                        },
                        error: function(){} 	        
            });
        }
    }));
        
    
});

function ChangeStatus(val,id){
    //$("#bodyid").showLoading();
    
    var q_mark = $("#q_mark").val();
    
    $.ajax({
        url: base_url+'ajaxfiles/emp-list.php',
        type: 'POST',
        data: {action:"changestatus", id : id , status : val, q_mark : q_mark } ,
        success: function(data){
           // $("#bodyid").hideLoading();
            $('#page_listing').html(data);
            $("#divMsg").show();
            $("html, body").animate({ scrollTop: 0 }, 500);
            $("#divMsg").html("Record has been updated successfully.");
        }
    });
}

function RemoveRecord(id){

    var res_cnf=confirm("Are you sure you want to delete this record ?");
    
    var q_mark = $("#q_mark").val();

    if(res_cnf == true){
      //  $("#bodyid").showLoading();
        
        $.ajax({
            url: base_url+'ajaxfiles/emp-list.php',
            type: 'POST',
            data: {action:"delete", id : id , q_mark : q_mark } ,
            success: function(data){
               // $("#bodyid").hideLoading();
                $('#page_listing').html(data);
                $("#divMsg").show();
                $("html, body").animate({ scrollTop: 0 }, 500);
                $("#divMsg").html("Record has been deleted successfully.");
                
            }
        });	
    }
}


function custAction(action,actiongobutton){		

    $("#checkgobutton").val("Go");
    if(action == "Action"){
        alert("Please select action");
        return false;
    }

    var param = $("#frmtable").serialize();		

    if ($("#frmtable input:checkbox:checked").length == 0){
        alert("Please select atleast one record");
        return false;
    }

    var dis_msg_txt;

    if(action == "Delete"){   
        dis_msg_txt = "deleted";
        var res_cnf=confirm("Are you sure you want to Delete this record ?");
    }

    if(action == "Active"){   
        dis_msg_txt = "activated";
        var res_cnf=confirm("Are you sure you want Activate this record ?");
    }

    if(action == "Inactive"){   
        dis_msg_txt = "inactivated";
        var res_cnf=confirm("Are you sure you want Deactivate this record ?");
    }
    
//    if($("#tbxTitle").val() != ""){}
//        param += "&tbxTitle="+$("#tbxTitle").val()+"&task=search";

    if(res_cnf == true){
       // $("#bodyid").showLoading();
        $.ajax({
                url: base_url+'ajaxfiles/emp-list.php',
                type: 'POST',
                data: param,
                success: function(data){	
                   // $("#bodyid").hideLoading();
                    $('#page_listing').html(data);
                    
                    if(dis_msg_txt == "deleted"){
                        $("#divMsg").show();
                        $("html, body").animate({ scrollTop: 0 }, 500);
                        $("#divMsg").html("Record has been deleted successfully");
                    }
                    else if(dis_msg_txt == "inactivated" || dis_msg_txt == "activated"){
                        $("#divMsg").show();
                        $("html, body").animate({ scrollTop: 0 }, 500);
                        $("#divMsg").html("Record has been updated successfully");
                    }
                }
        });
    }
}

function Search_Data(){
   // $("#bodyid").showLoading();
    var form_data = $("#frmtable , #frmSearch").serialize();
    form_data = form_data + "&task=search";
    $.ajax({
            url: base_url+'ajaxfiles/emp-list.php',
            type: 'POST',
            data: form_data,//{task:task, search_term:search_term , search_status:search_status,pageval:pageval,limit_val:limit_val} ,
            success: function(data){
                $("#page_listing").html(data);
                $("#checkgobutton").val("");
                $("#drpAction").val('').trigger("liszt:updated");
               // $("#bodyid").hideLoading();
            }
    });	
    return false;
}

function Pagination(pageno){
   // $("#bodyid").showLoading();

    var form_data = $("#frmtable , #frmSearch").serialize();
    form_data = form_data + "&task=search";
    form_data = form_data + "&start="+pageno;

    $.ajax({
        url: base_url+'ajaxfiles/emp-list.php',
        type: 'POST',
        data: form_data,
        success: function(data){
            $("#page_listing").html(data);
           // $("#bodyid").hideLoading();
        }
    });
    return false;
}
    
function setPage(page){
        
    //$("#bodyid").showLoading();

    var form_data = $("#frmtable , #frmSearch").serialize();
    form_data = form_data + "&task=search";
    form_data = form_data + "&pagesize="+page;


    $.ajax({
        url: base_url+'ajaxfiles/emp-list.php',
        type: 'POST',
        data: form_data,
        success: function(data){
            $("#page_listing").html(data);
           // $("#bodyid").hideLoading();
        }
    });
    return false;
}


function setEmp(val){
   // $("#bodyid").showLoading();

    $("#q_mark").val(val);

    var form_data = $("#frmtable , #frmSearch").serialize();
    form_data = form_data + "&task=char";

    $.ajax({
        url: base_url+'ajaxfiles/emp-list.php',
        type: 'POST',
        data: form_data,
        success: function(data){
            $("#page_listing").html(data);
           // $("#bodyid").hideLoading();
        }
    });
    return false;
}


function resetEmp(val){
   /// $("#bodyid").showLoading();
    $("#q_mark").val("");
    $.ajax({
        url: base_url+'ajaxfiles/emp-list.php',
        type: 'POST',
        data: '',
        success: function(data){
            $("#page_listing").html(data);
           // $("#bodyid").hideLoading();
        }
    });
    return false;
}

function setClass(){
    $(".filtercourse .active").removeClass("active");
    $(".anc").addClass("active");
}