// JavaScript Document

$(document).ready(function(e) {

    // All select box will convert chosen selectbox style

    $("select").chosen();

    // Search box toggle 
    $(".bAct_toggle").click(function() {
        $(this).parent().next().slideToggle();
        var htmlsrc = $(this).find("img");

        if (htmlsrc.hasClass('bAct_minus') == true){
            htmlsrc.removeClass('bAct_minus');
            htmlsrc.addClass('bAct_plus');
        }
        else{
            htmlsrc.removeClass('bAct_plus');
            htmlsrc.addClass('bAct_minus');
        }
    });

    $("#btnReset").click(function() {
        $("#drpsearch_status").val('').trigger("liszt:updated");
        $("#tbxTitle").val('');
    });
    
    $("#btnReset").click(function(){
        $("#bodyid").showLoading();
        $.ajax({
            url: base_url+'ajaxfiles/cms.php',
            type: 'POST',
            data: '',
            success: function(data){	
                $("#bodyid").hideLoading();
                $('#cms_page_listing').html(data);
            }
        });
    });
    
    $('.on_off_activestatus :checkbox').iphoneStyle({ 
        checkedLabel: 'Yes', 
        uncheckedLabel: 'No',
        onChange: function(elem, value) {
            if(value==false){
                $("#status").val("0");  
            }
            else{
                $("#status").val("1");  	
            }
        }
    });
        
    $("#btnAdd").click(function(){
        $("#bodyid").showLoading();
        var error = false;
        var errortext=''
        var $inputs = $('#frmmodifycms :input');
        $inputs.each(function(){
            if($(this).hasClass('required')){
                if($(this).val()=='' && $(this).attr('name') != "content1"){//alert(here1);
                    error = true;
                    errortext += '<li>- '+$(this).attr('title')+'</li>';
                    $(this).closest('.desc_div').show();
                }
            }
        });
			
        if(CKEDITOR.instances.content1.getData() == ""){//alert(here2);
            error = true;
            errortext += '<li>- '+'Please Enter cms page content. '+'</li>';
        }
			
        if(error==true){//alert(here3);
            $("#error_div").html('<ul><li>Please correct the following errors</li>'+errortext+'</ul>'); 
            $("#error_div").show();									
            //$("#error_div").attr("tabindex",-1).focus();
            $("html, body").animate({ scrollTop: 0 }, 500);
            $("#bodyid").hideLoading();
            return false;	
        }
        else{
            
            $("#content").val(CKEDITOR.instances.content1.getData());
            var param = $("#frmmodifycms").find('input[name!=content1], textarea, select').serialize();

            $.ajax({
                    url: base_url+'ajaxfiles/cms_addedit.php',
                    type: 'POST',
                    data: param,
                    success: function(data){
                        $("#bodyid").hideLoading();
                        if(data.trim() == 'Yes'){
                            $("#divMsg").html("Record has been updated successfully."); 
                            $("#divMsg").show();
                            $("html, body").animate({ scrollTop: 0 }, 500);
                            setTimeout(function () {
                                window.location.href = "cms.php"; //will redirect to your blog page (an ex: blog.html)
                             }, 1000);
                            //window.location.href='cms.php';
                        }
                        else {
                            $("#error_div").html('<ul><li>Please correct the following errors</li><li>- '+data+'</li></ul>'); 
                            $("#error_div").show();
                            $("html, body").animate({ scrollTop: 0 }, 500);
                            $("#bodyid").hideLoading();
                        }
                    }
            });
         }
    });   
    
    
    $("#btnEdit").click(function(){
        $("#bodyid").showLoading();
        var error = false;
        var errortext=''
        var $inputs = $('#frmmodifycms :input');
        $inputs.each(function(){
            if($(this).hasClass('required')){
                if($(this).val()=='' && $(this).attr('name') != "content1"){//alert(here1);
                    error = true;
                    errortext += '<li>- '+$(this).attr('title')+'</li>';
                    $(this).closest('.desc_div').show();
                }
            }
        });
			
        if(CKEDITOR.instances.content1.getData() == ""){//alert(here2);
            error = true;
            errortext += '<li>- '+'Please Enter cms page content. '+'</li>';
        }
			
        if(error==true){//alert(here3);
            $("#error_div").html('<ul><li>Please correct the following errors</li>'+errortext+'</ul>'); 
            $("#error_div").show();									
            //$("#error_div").attr("tabindex",-1).focus();
            $("html, body").animate({ scrollTop: 0 }, 500);
            $("#bodyid").hideLoading();
            return false;	
        }
        else{
            
            $("#content").val(CKEDITOR.instances.content1.getData());
            var param = $("#frmmodifycms").find('input[name!=content1], textarea, select').serialize();

            $.ajax({
                    url: base_url+'ajaxfiles/cms_addedit.php',
                    type: 'POST',
                    data: param,
                    success: function(data){
                        $("#bodyid").hideLoading();
                        if(data.trim() == 'Yes'){
                            $("#divMsg").html("Record has been updated successfully."); 
                            $("#divMsg").show();
                            $("html, body").animate({ scrollTop: 0 }, 500);
                            setTimeout(function () {
                                window.location.href = "cms.php"; //will redirect to your blog page (an ex: blog.html)
                             }, 1000);
                            //window.location.href='cms.php';
                        }
                        else {
                            $("#error_div").html('<ul><li>Please correct the following errors</li><li>- '+data+'</li></ul>'); 
                            $("#error_div").show();
                            $("html, body").animate({ scrollTop: 0 }, 500);
                            $("#bodyid").hideLoading();
                        }
                    }
            });
         }
    });  
    
});

function RemoveRecord(id){

    var res_cnf=confirm("Are you sure you want to delete this CMS page?");

    if(res_cnf == true){
        $("#bodyid").showLoading();
        $.ajax({
            url: base_url+'ajaxfiles/cms.php',
            type: 'POST',
            data: {action:"delete", id : id } ,
            success: function(data){
                $("#bodyid").hideLoading();
                $('#cms_page_listing').html(data);
                $("#divMsg").show();
                $("html, body").animate({ scrollTop: 0 }, 500);
                $("#divMsg").html("Record has been deleted successfully.");
                
            }
        });	
    }
}


function custAction(action,actiongobutton){		

    $("#checkgobutton").val("Go");
    if(action == "Action"){
        alert("Please select action");
        return false;
    }

    var param = $("#cmstable").serialize();		

    if ($("#cmstable input:checkbox:checked").length == 0){
        alert("Please select atlist one cms page");
        return false;
    }

    var dis_msg_txt;

    if(action == "Delete"){   
        dis_msg_txt = "deleted";
        var res_cnf=confirm("Are you sure you want to Delete this CMS?");
    }

    if(action == "Active"){   
        dis_msg_txt = "activated";
        var res_cnf=confirm("Are you sure you want Activate this CMS?");
    }

    if(action == "Inactive"){   
        dis_msg_txt = "inactivated";
        var res_cnf=confirm("Are you sure you want Deactivate this CMS?");
    }

    if(res_cnf == true){
        $("#bodyid").showLoading();
        $.ajax({
                url: base_url+'ajaxfiles/cms.php',
                type: 'POST',
                data: param,
                success: function(data){	
                    $("#bodyid").hideLoading();
                    $('#cms_page_listing').html(data);
                    
                    if(dis_msg_txt == "deleted"){
                        $("#divMsg").show();
                        $("html, body").animate({ scrollTop: 0 }, 500);
                        $("#divMsg").html("Record has been deleted successfully");
                    }
                    else if(dis_msg_txt == "inactivated" || dis_msg_txt == "activated"){
                        $("#divMsg").show();
                        $("html, body").animate({ scrollTop: 0 }, 500);
                        $("#divMsg").html("Record has been updated successfully");
                    }
                }
        });
    }
}

function Search_Data(){
    $("#bodyid").showLoading();
    var form_data = $("#cmstable , #frmSearch").serialize();
    form_data = form_data + "&task=search";
    $.ajax({
            url: base_url+'ajaxfiles/cms.php',
            type: 'POST',
            data: form_data,//{task:task, search_term:search_term , search_status:search_status,pageval:pageval,limit_val:limit_val} ,
            success: function(data){
                $("#cms_page_listing").html(data);
                $("#checkgobutton").val("");
                $("#drpAction").val('').trigger("liszt:updated");
                $("#bodyid").hideLoading();
            }
    });	
    return false;
}

function Pagination(pageno){
        $("#bodyid").showLoading();

        var form_data = $("#cmstable , #frmSearch").serialize();
        form_data = form_data + "&task=search";
        form_data = form_data + "&start="+pageno;

        $.ajax({
            url: base_url+'ajaxfiles/cms.php',
            type: 'POST',
            data: form_data,
            success: function(data){
                $("#cms_page_listing").html(data);
                $("#bodyid").hideLoading();
            }
        });
        return false;
}
    
    function setPage(page){
        
        $("#bodyid").showLoading();
        
        var form_data = $("#cmstable , #frmSearch").serialize();
        form_data = form_data + "&task=search";
        form_data = form_data + "&pagesize="+page;


        $.ajax({
            url: base_url+'ajaxfiles/cms.php',
            type: 'POST',
            data: form_data,
            success: function(data){
                $("#cms_page_listing").html(data);
                $("#bodyid").hideLoading();
            }
        });
        return false;
    }