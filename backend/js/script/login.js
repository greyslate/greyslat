/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function checkLogin() {
    //$("#bodyid").showLoading();
    var login_name = $("#txtlogin").val();
    var password = $("#txtpassword").val();
    var form_data = "login=" + login_name + "&password=" + password + "&action=login";
    $.ajax({
        url: base_url + 'ajaxfiles/ajaxlogin.php',
        type: 'POST',
        data: form_data,
        success: function(data) {
            if (data != 1) {
                $("#failmsg").show()
                var arr = data.split("#");
                $("#failmsg").html(arr[1]);
                $("#failmsg").fadeIn(3000, function() {
                    // $( "#failmsg" ).fadeOut( 5000 );
                });
                //$("#bodyid").hideLoading();
            }
            else {
                //$("#bodyid").hideLoading();
                window.location = base_url;
            }
        }
    });
}

function forgotpassword(){
        var error = false;

//        var tbxEmailAddress = $("#tbxEmailAddress").val();
//
//        if (tbxEmailAddress == '')
//            error = true;
//
//        var atpos = tbxEmailAddress.indexOf("@");
//        var dotpos = tbxEmailAddress.lastIndexOf(".");
//        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= tbxEmailAddress.length){
//
//            error = true;
//
//        }

//        if (error == true){
//            $("#error_msg").html("Please enter valid email address");
//            $("#error_msg").show();
//            return false;
//        }

        var param = $("#frmUserForgetPwd").serialize();

        $.ajax({
            url: base_url+'ajaxfiles/ajaxlogin.php',
            type: 'POST',
            data: param+"&type=forgotpassword",
            success: function(data){
                if (data != 'Yes'){
                    $("#failmsg").html("Invalid Username/Password");
                    $("#failmsg").show();
                }
                else{
                    $("#divMsg").html("Password has been sent to your email."); 
                    $("#divMsg").show();
                    $("html, body").animate({ scrollTop: 0 }, 500);
                    $( "#divMsg" ).fadeIn( 3000, function() {
                        $( "#divMsg" ).fadeOut( 5000 );
                    });
                }
            }
        });
}

$(document).ready(function()
{



    $(".sl_link").click(function(event) {

        $('#divMsg').hide();

        $("#error_msg").hide();

        $('.l_pane').slideToggle('normal').toggleClass('dn');

        $('.sl_link,.lb_ribbon').children('span').toggle();

        event.preventDefault();

    });



    // Forgot password

    $("#btnForgotPwd").click(function(event) {

        var error = false;

        var tbxEmailAddress = $("#tbxEmailAddress").val();

        if (tbxEmailAddress == '')
            error = true;

        var atpos = tbxEmailAddress.indexOf("@");
        var dotpos = tbxEmailAddress.lastIndexOf(".");
        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= tbxEmailAddress.length){

            error = true;

        }

        if (error == true){
            $("#error_msg").html("Please enter valid email address");
            $("#error_msg").show();
            return false;
        }

        var param = $("#frmUserForgetPwd").serialize();

        $.ajax({
            url: base_url+'ajaxfiles/ajaxlogin.php',
            type: 'POST',
            data: param+"&type=forgotpassword",
            success: function(data){

                $("#bodyid").hideLoading();
                if (data == ''){
                    $("#error_msg").html("Invalid Username/Password");
                    $("#error_msg").show();
                }
                else{
                    $("#divMsg").html("Password has been sent to your email."); 
                    $("#divMsg").show();
                    $("html, body").animate({ scrollTop: 0 }, 500);
                    $( "#divMsg" ).fadeIn( 3000, function() {
                        $( "#divMsg" ).fadeOut( 5000 );
                    });
                }
            }
        });
    });
});