<div class="myrs-pagination clearfix">
<table width="100%" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td>
                                
         <?php
		 	if (isset($_REQUEST['start'])){
				$_SESSION['start_record'] = $_REQUEST['start'];
			}
	
			if($cobj->total_record > $_SESSION['page_size']){
				
				
				$pagelink = generate_pagination1($cobj->total_record, $_SESSION['page_size'],$_SESSION['start_record'] , "frm");
				echo $pagelink;
			}
		?>           
                
        </td>       
            <td align="right" width="20%">             
                <div class="rangeTotal">
                    <span class="myrs-pagination-total">
                        <strong id="divTotRec"><?php echo $cobj->total_record; ?> records</strong>
                    </span>
                    <span class="myrs-pagination-maxvalue" style="display: inline;">
                        <a class="myrs-pagination-maxvalue-toggler" href="javascript:;" onclick="page_view_toggel();">
                            <strong id="StrRecPerPage">
                                <?php 
                                    echo $_SESSION['page_size']; 
                                ?>
                            </strong> 
                            Per Page
                        </a>
                        <input type="hidden" name="pageval" id="pageval" value="<?php  echo PAGE_LIMIT; ?>" />
                        <div class="myrs-pagination-maxvalue-list" style="display: none;margin-top:8px; ">
                            <ul>
                                <?php
                                    foreach($config["Page_Size"] as $val){
                                        ?>
                                            <li>
                                                <a href="javascript:void(0);" onclick="javascript:setPage('<?php echo $val; ?>');">
                                                    <strong><?php echo $val; ?></strong> Per Page
                                                </a>
                                            </li>
                                        <?php
                                    }
                                ?>
                            </ul>
                        </div>
                    </span>
                </div>
                <input type="hidden" name="page" id="page" value="0">      
            </td>
    </tr>
</tbody></table>
</div>

