<?php
    require_once("../../includes/config.php");
    
    global $db;
    
    
    $obj = new Admin();
    
    $info = $obj->getAdminDetail();
    
    $old_password = $_POST['txtoldpassword'];
    $new_password = $_POST['txtnewpassword'];
    
    
    if($old_password != $info->password){
        echo "Old Password is wrong";
    }
    else {
        $arr = array(
                        "password" => $new_password,
                );
        $where_arr = array("admin_id" => $_SESSION['admin_id']);
        $result = UpdateData($arr, ADMIN_MASTER, $where_arr);
        echo "Yes";
    }
    
    
?>