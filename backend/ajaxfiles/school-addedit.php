<?php

define("ADMIN", true);
include("../../includes/config.php");
global $dbs;

function create_db($cPanelUser, $cPanelPass, $dbName) {

    if ($_SERVER['SERVER_NAME'] == "localhost") {
        $dbs = new DB_MYSQL("localhost", MAIN_DB, $cPanelUser, $cPanelPass, false);
        $dbs->query("create database $dbName");
    } else {
        $buildRequest = "/frontend/x3/sql/addb.html?db=" . $dbName;

        $openSocket = fsockopen('localhost', 2082);
        if (!$openSocket) {
            return "Socket error";
            exit();
        }

        $authString = $cPanelUser . ":" . $cPanelPass;
        $authPass = base64_encode($authString);
        $buildHeaders = "GET " . $buildRequest . "\r\n";
        $buildHeaders .= "HTTP/1.0\r\n";
        $buildHeaders .= "Host:localhost\r\n";
        $buildHeaders .= "Authorization: Basic " . $authPass . "\r\n";
        $buildHeaders .= "\r\n";

        fputs($openSocket, $buildHeaders);
        while (!feof($openSocket)) {
            fgets($openSocket, 128);
        }
        fclose($openSocket);
    }




//echo "Created database $dbName";
}

if (isset($_POST['action']) && $_POST['action'] == "add") {

$emp_id = 0;

    $flag = false;
    //---------- Check Login Name is already exits or not
    $error_text = '<ul>';
//    $sql = "Select count(*) As cnt From " . SCHOOL_MASTER . " Where login_name = '" . $_REQUEST['txtlogin'] . "'";
//    $db->query($sql);
//    $srow = $db->fetch_object(MYSQL_FETCH_SINGLE);
//
//
//    if ($srow->cnt > 0) {
//        $flag = true;
//        $error_text .= '<li>- Login Name is already taken.</li>';
//    }
    //----------- Check Institute Name is already exits
    $sql = "Select count(*) As cnt From " . SCHOOL_MASTER . " Where institute_name = '" . $_REQUEST['txtinstitutename'] . "'";

    $db->query($sql);
    $crow = $db->fetch_object(MYSQL_FETCH_SINGLE);

    if ($crow->cnt > 0) {
        $flag = true;
        $error_text .= '<li>- Institute Name is already exits!</li>';
    }
    $error_text .= '</ul>';

    //----- Get Max Number For Database
    $msql = "Select max(id) As max From " . SCHOOL_MASTER;
    $db->query($msql);

    $mrow = $db->fetch_object(MYSQL_FETCH_SINGLE);
    $max_number = $mrow->max + 1;
    
    
    
    $db_name = "greyslat_".$max_number;



    if ($flag == false) {

        //---- Create A Folder


        if (!file_exists(UPLOAD_PATH . $db_name)) {
            mkdir(UPLOAD_PATH . $db_name, 0777);
        }

        if (!file_exists(UPLOAD_PATH . $db_name . "/school")) {
            mkdir(UPLOAD_PATH . $db_name . "/school", 0777);
        }

        if (!file_exists(UPLOAD_PATH . $db_name . "/student")) {
            mkdir(UPLOAD_PATH . $db_name . "/student", 0777);
        }

        if (!file_exists(UPLOAD_PATH . $db_name . "/holiday")) {
            mkdir(UPLOAD_PATH . $db_name . "/holiday", 0777);
        }

        if (!file_exists(UPLOAD_PATH . $db_name . "/syllabus")) {
            mkdir(UPLOAD_PATH . $db_name . "/syllabus", 0777);
        }

        if (!file_exists(UPLOAD_PATH . $db_name . "/timetable")) {
            mkdir(UPLOAD_PATH . $db_name . "/timetable", 0777);
        }

        if (!file_exists(UPLOAD_PATH . $db_name . "/notice")) {
            mkdir(UPLOAD_PATH . $db_name . "/notice", 0777);
        }

        if (!file_exists(UPLOAD_PATH . $db_name . "/event")) {
            mkdir(UPLOAD_PATH . $db_name . "/event", 0777);
        }

        if (!file_exists(UPLOAD_PATH . $db_name . "/principle_desk")) {
            mkdir(UPLOAD_PATH . $db_name . "/principle_desk", 0777);
        }

        if (!file_exists(UPLOAD_PATH . $db_name . "/gallery")) {
            mkdir(UPLOAD_PATH . $db_name . "/gallery", 0777);
        }

        if (!file_exists(UPLOAD_PATH . $db_name . "/employee")) {
            mkdir(UPLOAD_PATH . $db_name . "/employee", 0777);
        }

        if (!file_exists(UPLOAD_PATH . $db_name . "/exam_timetable")) {
            mkdir(UPLOAD_PATH . $db_name . "/exam_timetable", 0777);
        }

        $school_paths = $db_name . "/school/";

        $upload_logo_path = UPLOAD_PATH . $school_paths;

        $NewImageName = '';
        if (isset($_FILES['institute_logo']['name']) && $_FILES['institute_logo']['name'] != "") {

            //==== Image Uploading
            $RandomNum = time();
            $ImageName = str_replace(' ', '-', strtolower($_FILES['institute_logo']['name']));
            $ImageType = $_FILES['institute_logo']['type']; //"image/png", image/jpeg etc.
            $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
            $ImageExt = str_replace('.', '', $ImageExt);
            $ImageName = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
            $NewImageName = $ImageName . '-' . $RandomNum . '.' . $ImageExt;
            move_uploaded_file($_FILES["institute_logo"]["tmp_name"], $upload_logo_path . $NewImageName);

            $resizeObj = new ImageResize($upload_logo_path . $NewImageName);

            //---- Product Listing
            $resizeObj->resizeImage(640, 480, 'auto');
            $resizeObj->saveImage($upload_logo_path . "thumb_" . $NewImageName, 100);
        }

        $arr = array(
            "institute_name" => $_REQUEST['txtinstitutename'],
            "school_description" => $_REQUEST['txtdesc'],
            "address" => $_REQUEST['txtaddress'],
            "principal_name" => $_REQUEST['txtprincipalname'],
            "teach_medium" => implode("," , $_REQUEST['teach_medium']),
            "edu_board" =>implode("," , $_REQUEST['edu_board']),
            "school_standard" => $_REQUEST['school_standard'],
            "city" => $_REQUEST['city'],
            "state" => $_REQUEST['state'],
            "country" => $_REQUEST['country'],
            "postalcode" => $_REQUEST['txtpostalcode'],
            "phone_no" => $_REQUEST['txtphone'],
            "school_path" => $school_paths,
            "institute_logo" => $NewImageName,
            "institute_email" => $_REQUEST['txtemail'],
            "institute_type" => $_REQUEST['school_type'],
            "reg_date" => date("Y-m-d H:i:s"),
            "status" => isset($_REQUEST['status']) ? 1 : 0,
            "database_name" => $db_name,
        );

        $result = InsertData($arr, SCHOOL_MASTER);
        



        //------------ Dynamic Database


        $source_db = MAIN_DB;
        $target_db = $db_name;


        $host_name = 'localhost';
        $user_name = DB_USER;
        $password = DB_PASS;

        //echo $field_name = "Tables_in_".$source_db;die;
        $dbs = new DB_MYSQL($host_name, $source_db, $user_name, $password, false);

        $dbs->query("show tables");
        $result = $dbs->fetch_object();
        // Get names of all tables in source database

        $fields_name = "Tables_in_" . $source_db;
        foreach ($result as $row) {
            $name = $row->$fields_name;
            $query = "show create table $name";
            $dbs->query($query);
            $result = $dbs->fetch_object();
            $field_name = "Create Table";
            $tables[] = array('name' => $name, 'query' => $result[0]->$field_name);
        }

        create_db(ROOT_USER, ROOT_PASS, $target_db);

        //$dbs->query("create database $target_db");
        //$dbs->query("GRANT ALL PRIVILEGES ON $target_db.* TO '$user_name'@'%' identified by '$password'");
        $dbs = new DB_MYSQL($host_name, $target_db, ROOT_USER, ROOT_PASS, false);

        $total = count($tables);

        for ($i = 0; $i < $total; $i++) {
            $name = $tables[$i]['name'];
            $q = $tables[$i]['query'];

            $dbs->query($q);
            $dbs->query("insert into $name select * from $source_db.$name");
        }

        $db_arr = array(
            "fname" => $_REQUEST['txtfname'],
            "lname" => $_REQUEST['txtlname'],
            "email" => $_REQUEST['txtlogin'],
            "password" => $_REQUEST['txtpassword'],
            "phone" => $_REQUEST['txtphone'],
            "role_id" => 1,
            "status" => 1,
            "created_on" => date("Y-m-d H:i:s"),
        );


        InsertDataGlobal($db_arr, "emp_master");
        $emp_id = $dbs->sql_inserted_id();



        echo "Yes";
    } else {
        echo $error_text;
    }
}


if (isset($_POST['action']) && $_POST['action'] == "edit") {

    $id = $_REQUEST['id'];

    $sql = "Select count(*) As cnt From " . SCHOOL_MASTER . " Where id != '" . $_REQUEST['id'] . "' And institute_name = '" . $_REQUEST['txtinstitutename'] . "'";
    $db->query($sql);
    $crow = $db->fetch_object(MYSQL_FETCH_SINGLE);

    if ($crow->cnt == 0) {

        $NewImageName = '';
        if (isset($_FILES['institute_logo']['name']) && $_FILES['institute_logo']['name'] != "") {

            $school_info = $school_obj->getSchool($id);
            $school_paths = $school_info->school_path;

            $upload_logo_path = UPLOAD_PATH . $school_paths;

            if ($school_info->institute_logo != "") {
                @unlink($upload_logo_path . $school_info->institute_logo);
                @unlink($upload_logo_path . "thumb_" . $school_info->institute_logo);
            }

            //==== Image Uploading
            $RandomNum = time();
            $ImageName = str_replace(' ', '-', strtolower($_FILES['institute_logo']['name']));
            $ImageType = $_FILES['institute_logo']['type']; //"image/png", image/jpeg etc.
            $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
            $ImageExt = str_replace('.', '', $ImageExt);
            $ImageName = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
            $NewImageName = $ImageName . '-' . $RandomNum . '.' . $ImageExt;
            move_uploaded_file($_FILES["institute_logo"]["tmp_name"], $upload_logo_path . $NewImageName);

            $resizeObj = new ImageResize($upload_logo_path . $NewImageName);

            //---- Product Listing
            $resizeObj->resizeImage(640, 480, 'auto');
            $resizeObj->saveImage($upload_logo_path . "thumb_" . $NewImageName, 100);
        } else {
            $NewImageName = $_REQUEST['oimg'];
        }

        $arr = array(
            "institute_name" => $_REQUEST['txtinstitutename'],
            "school_description" => $_REQUEST['txtdesc'],
           "principal_name" => $_REQUEST['txtprincipalname'],
            "teach_medium" => implode("," , $_REQUEST['teach_medium']),
            "edu_board" =>implode("," , $_REQUEST['edu_board']),
            "school_standard" => $_REQUEST['school_standard'],
            "address" => $_REQUEST['txtaddress'],
            "city" => $_REQUEST['city'],
            "state" => $_REQUEST['state'],
            "country" => $_REQUEST['country'],
            "postalcode" => $_REQUEST['txtpostalcode'],
            "phone_no" => $_REQUEST['txtphone'],
            "institute_logo" => $NewImageName,
            "institute_email" => $_REQUEST['txtemail'],
            "institute_type" => $_REQUEST['school_type'],
            "status" => isset($_REQUEST['status']) ? 1 : 0,
        );

        $where_arr = array("id" => $id);

        $result = UpdateData($arr, SCHOOL_MASTER, $where_arr);
        echo "Yes";
    } else {
        echo "School Name is already exits...!";
    }
}
?>