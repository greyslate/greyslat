<?php
define("ADMIN", true);
include("../../includes/config.php");

$cobj = new School();
if (isset($_REQUEST['start'])) {
    $_SESSION['start_record'] = $_REQUEST['start'];
}


if ((isset($_REQUEST['action']) && $_REQUEST['action'] == "delete") && (isset($_REQUEST['id']) && $_REQUEST['id'] != "")) {
    $irow = $cobj->getSchool($_REQUEST['id']);
    $image_name = SCHOOL_PATH . $irow->institute_logo;
    $timage_name = SCHOOL_PATH ."thumb_". $irow->institute_logo;
    
    @unlink($image_name);
    @unlink($timage_name);
    $cobj->DeleteSchool($_REQUEST['id']);
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Delete") {
    if (isset($_POST['id'])) {
        foreach ($_POST['id'] as $val) {
            $irow = $cobj->getSchool($val);
            $image_name = SCHOOL_PATH . $irow->institute_logo;
            $timage_name = SCHOOL_PATH ."thumb_". $irow->institute_logo;

            @unlink($image_name);
            @unlink($timage_name);
            $cobj->DeleteSchool($val);
        }
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Active" && !isset($_POST['task'])) {

    foreach ($_POST['id'] as $val) {

        $arr = array(
            "status" => 1,
        );

        $where_arr = array("id" => $val);
        $result = UpdateData($arr, SCHOOL_MASTER, $where_arr);
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Inactive" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 0,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr, SCHOOL_MASTER, $where_arr);
    }
}

if (isset($_POST['action']) && $_POST['action'] == "changestatus") {
    $status = $_POST['status'];
    $id = $_POST['id'];

    if ($status == 1) {
        $st = 0;
    } else {
        $st = 1;
    }

    $arr = array(
        "status" => $st,
    );

    $where_arr = array("id" => $id);
    $result = UpdateData($arr, SCHOOL_MASTER, $where_arr);
}

$where = '';

if (isset($_POST['task']) && $_POST['task'] == "search") {
    if (isset($_POST['tbxTitle']) && $_POST['tbxTitle'] != "") {
        $where .= " And institute_name like '%" . $_POST['tbxTitle'] . "%'";
    }
    if (isset($_POST['drpsearch_status']) && $_POST['drpsearch_status'] != "") {
        $where .= " And status = '" . $_POST['drpsearch_status'] . "'";
    }
}

if (isset($_REQUEST['q_mark']) && $_REQUEST['q_mark'] != "") {
    $q = $_REQUEST['q_mark'];
    $where .= " And institute_name like '" . $q . "%'";
}

$data = $cobj->fetch_all($where);
?>

<table cellpadding="0" cellspacing="0" border="0" id="tblsortable" class="displaysort mobile_dt tablesorter" style="top: 0px;">
    <thead>
        <tr>
            <th class="chb_col" width="1" >
                <input type="checkbox" name="checkall" onclick="javascript:checkAlluncheckAll(frm);">
            </th>
            <th width="5%">Institute Logo</th>
            <th>
                Institute Code
            </th>
            <th width="20%">
                Institute Name
            </th>
            <th>
                Email
            </th>
            <th>
                Location
            </th>
            <th>
                Phone No
            </th>
            <th>
                Institute Type
            </th>

            <th class="center" width="5%" style="text-align: center;">
                Status
            </th>
            <th width="10%" style="text-align: center;">
                Action
            </th>
        </tr>
    </thead>

    <tbody id="content ui-sortable">

        <?php
        if (count($data) == 0) {
            ?>
            <tr><td colspan="8" align="center">No Result found</td></tr>
            <?php
        } else {
            foreach ($data as $val) {
                $country_row = $country_obj->getCountry($val->country);
                $country_name = $country_row->country_name;

                $state_row = $state_obj->getState($val->state);
                $state_name = $state_row->state_name;

                $city_row = $city_obj->getCity($val->city);
                $city_name = $city_row->city_name;
                ?>
                <tr>
                    <td class="chb_col">
                        <input type="checkbox" name="id[]" value="<?php echo $val->id; ?>">
                    </td>
                    <td>
                        <?php
                            if($val->institute_logo != "" && file_exists(UPLOAD_PATH.$val->school_path.$val->institute_logo)){
                                ?>
                                <img width="100" src="<?php echo UPLOAD_URL.$val->school_path.$val->institute_logo; ?>" />
                                <?php
                            }
                            else{
                                ?>
                                <img width="100" src="<?php echo IMAGE_URL; ?>noimage.gif" />
                                <?php    
                            }
                        ?>

                    </td>
                    <td>
                        KON-<?php echo $val->id; ?>
                    </td>
                    <td>
                        <?php echo $val->institute_name; ?>
                    </td>
                    <td>
                        <?php echo $val->institute_email; ?>
                    </td>
                    <td>
                        <?php echo $city_name; ?>,<br />
                        <?php echo $state_name . ", " . $country_name; ?>
                    </td>
                    <td>
                        <?php echo $val->phone_no; ?>
                    </td>
                    <td>
                        <?php echo $val->institute_type; ?>
                    </td>

                    <td id="td_status_<?php echo $val->id; ?>" style="text-align: center;">

                        <a href="javascript:;" onclick="ChangeStatus('<?php echo $val->status; ?>', '<?php echo $val->id; ?>')"

                           title='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>'>

                            <img  src='<?php echo IMAGE_URL; ?><?php
                echo ($val->status == 1 ? "ico-1.gif" : "ico-0.gif");
                ?>

                                  ' alt='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>'

                                  title='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>' />

                        </a>

                    </td>
                    <td class="content_actions" style="text-align: center;">
                        <a href="javascript:;" onclick="javascript:RemoveRecord('<?php echo $val->id; ?>')" title='Delete'>
                            <img src="<?php echo IMAGE_URL; ?>ico/trashcan_gray.png" alt="" /></a> <a href="school-edit.php?id=<?php echo $val->id; ?>" class="sepV_a" title="Edit">
                            <img src="<?php echo IMAGE_URL; ?>ico/pencil_gray.png" alt="" />
                        </a>
                    </td>
                </tr>
        <?php
    }
}
?>
    </tbody>
</table>

        <?php
        include("../pagination.php");
        ?>