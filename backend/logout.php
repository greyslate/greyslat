<?php
    session_start();
    
    if(!isset($_SESSION['admin_id'])){
        header("location: login.php");
        exit(0);
    }
    
    session_destroy();
    
    header("location: login.php");
    exit(0);
?>