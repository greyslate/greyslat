<?php
    define("ADMIN",true);
    include("../includes/config.php");
    
    
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Change Password</title>
    <link rel="stylesheet" href="<?php echo CSS_URL; ?>style.css" />
    <link rel="stylesheet" href="<?php echo CSS_URL; ?>foundation.css" />
    <script src="<?php echo JS_URL; ?>jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
        var base_url = "<?php echo SITE_URL; ?>backend/";
    </script>
    <script language="javascript" src="<?php echo JS_URL; ?>script/changepassword.js"></script>
</head>
<body style="background:#fff;">
    
    <form name="frmchangepassword" id="frmchangepassword" method="post" >
    
        <div id="divOrderView" style="padding: 10px;">
            <div>
                <h2>Change Password</h2>
            </div>
            <div class="alert-message success" id="divMsg" style="display: none;">Test</div>		
            <div style="width: 88%; display: none;" id="failmsg" class="notification n-error" >Test</div>
            <table cellpadding="0" cellspacing="0" border="0" width="70%" class="display mobile_dt2" style="top: 0px;">
                <thead>
                    <tr>
                        <th align="left" colspan="2">
                            Change Password
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="30%"><strong>Old Password</strong></td>
                        <td><input title="Please enter Old Password" type="password" name="txtoldpassword" id="txtoldpassword" class="input-text required large" /></td>
                    </tr>

                    <tr>
                        <td><strong>New Password</strong></td>
                        <td><input title="Please enter New Password" type="password" name="txtnewpassword" id="txtnewpassword" class="input-text required large" /></td>
                    </tr>

                    <tr>
                        <td><strong>Confirm New Password</strong></td>
                        <td><input title="Please enter Confirm Password" type="password" name="txtcnewpassword" id="txtcnewpassword" class="input-text required large" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input onclick="javascript:changePassword();" type="button" class="button small radius" name="Submit" value="Change Password" /></td>
                    </tr>
                </tbody>
            </table>

        </div>
        
    </form>
    
    
</body>
</html>
