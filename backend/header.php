<?php
define("ADMIN", TRUE);
include("../includes/config.php");

if (!isset($_SESSION['admin_id']) && $_SESSION['admin_id'] == "") {
    header("location: login.php");
    exit(0);
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Greyslate - Panel</title>

        <meta name="description" content="Pleasure is responsive, material admin dashboard panel">
        <meta name="author" content="Teamfox">

        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
        <meta name="apple-touch-fullscreen" content="yes">
        
        <script type="text/javascript">
            var base_url = "<?php echo SITE_URL; ?>backend/";
        </script>

        <!-- BEGIN CORE CSS -->
        <link rel="stylesheet" href="<?php echo CSS_URL; ?>admin1.css">
        <link rel="stylesheet" href="<?php echo CSS_URL; ?>elements.css">
        <!-- END CORE CSS -->

        <!-- BEGIN PLUGINS CSS -->
        <link rel="stylesheet" href="<?php echo PLUGIN_URL; ?>rickshaw/rickshaw.min.css">
        <link rel="stylesheet" href="<?php echo PLUGIN_URL; ?>bxslider/jquery.bxslider.css">

        <link rel="stylesheet" href="<?php echo CSS_URL; ?>plugins.css">
        <!-- END PLUGINS CSS -->

        <!-- BEGIN SHORTCUT AND TOUCH ICONS -->
        <link rel="shortcut icon" href="<?php echo IMAGE_URL; ?>icons/favicon.ico">
        <link rel="apple-touch-icon" href="<?php echo IMAGE_URL; ?>icons/apple-touch-icon.png">
        <!-- END SHORTCUT AND TOUCH ICONS -->

        <script src="<?php echo PLUGIN_URL; ?>modernizr/modernizr.min.js"></script>

        

        <!-- BEGIN GLOBAL AND THEME VENDORS -->
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script src="<?php echo JS_URL; ?>global-vendors.js"></script>
       
        <!-- END GLOBAL AND THEME VENDORS -->
        
        <!-- BEGIN PLUGINS AREA -->
        <script src="<?php echo PLUGIN_URL; ?>bxslider/jquery.bxslider.min.js"></script>
        <script src="<?php echo PLUGIN_URL; ?>audiojs/audiojs/audio.min.js"></script>
        <script src="<?php echo PLUGIN_URL; ?>d3/d3.min.js"></script>
        <script src="<?php echo PLUGIN_URL; ?>rickshaw/rickshaw.min.js"></script>
        <script src="<?php echo PLUGIN_URL; ?>jquery-knob/excanvas.js"></script>
        <script src="<?php echo PLUGIN_URL; ?>jquery-knob/dist/jquery.knob.min.js"></script>
        <script src="<?php echo PLUGIN_URL; ?>gauge/gauge.min.js"></script>
        <!-- END PLUGINS AREA -->

        <script src="<?php echo SCRIPT_URL; ?>sliders.js"></script>

        <script src="<?php echo JS_URL; ?>jquery.validate.js" type="text/javascript"></script>

        <script src="<?php echo PLUGIN_URL; ?>bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
        <script src="<?php echo JS_URL; ?>ui-toastr.min.js" type="text/javascript"></script>
        <script>
            toastr.options = {
                "closeButton": true,
                "preventDuplicates": true,
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "4000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut",
            }
        </script>

        <!-- PLEASURE -->
        <script src="<?php echo JS_URL; ?>pleasure.js"></script>
        <!-- ADMIN 1 -->
        <script src="<?php echo JS_URL; ?>layout.js"></script>


        <!-- BEGIN INITIALIZATION-->
        <script>
            $(document).ready(function () {
                ///Pleasure.init();
                Layout.init();

                //WidgetAudio.single();
                //ChartsKnob.init();


            });
        </script>
        <!-- END INITIALIZATION-->
    </head>
    <body>

        <div class="nav-bar-container">

            <!-- BEGIN ICONS -->
            <div class="nav-menu">
                <div class="hamburger">
                    <span class="patty"></span>
                    <span class="patty"></span>
                    <span class="patty"></span>
                    <span class="patty"></span>
                    <span class="patty"></span>
                    <span class="patty"></span>
                </div><!--.hamburger-->
            </div><!--.nav-menu-->

            <div class="nav-search">
                <span class="search"></span>
            </div><!--.nav-search-->

            <div class="nav-user">
                <div class="user">
                    <img src="<?php echo IMAGE_URL; ?>faces/tolga-ergin.jpg" alt="">
                    <span class="badge">3</span>
                </div><!--.user-->
                <div class="cross">
                    <span class="line"></span>
                    <span class="line"></span>
                </div><!--.cross-->
            </div><!--.nav-user-->
            <!-- END OF ICONS -->

            <div class="nav-bar-border"></div><!--.nav-bar-border-->

            <!-- BEGIN OVERLAY HELPERS -->
            <div class="overlay">
                <div class="starting-point">
                    <span></span>
                </div><!--.starting-point-->
                <div class="logo">GREY SLATE</div><!--.logo-->
            </div><!--.overlay-->

            <div class="overlay-secondary"></div><!--.overlay-secondary-->
            <!-- END OF OVERLAY HELPERS -->

        </div>
        
        <div id="loader" class="loading-bar determinate margin-top-10" style="display: none;"></div>