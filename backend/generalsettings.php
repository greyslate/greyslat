<?php

include("header.php");

$gobj = new Generalsettings();


$data= $gobj->Get_Settings_Data();


?>
    <script src="<?php echo JS_URL; ?>iphone-style-checkboxes.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="<?php echo CSS_URL; ?>radiobutton.css" type="text/css" media="screen" charset="utf-8" />
    <script src="<?php echo JS_URL; ?>script/generalsettings.js" type="text/javascript" charset="utf-8"></script>

    
    <div class="container" style="padding-bottom: 30px;height:386px;">
        <div class="row">
            <?php
                if(isset($_GET['m']) && $_GET['m'] != ""){
            ?>
            <div class="alert-message success" id="divMsg">
                <?php echo GENERAL_SETTINGS_MSG; ?>
            </div>
            <?php
                }
            ?>
            <div class="twelve columns">
                <div class="box_c">
                    <div class="box_c_heading cf">
                        <p>
                            Global Configuration
                        </p>
                    </div>
                    <form id="frmtestimonial" action="" method="post" target="ifrmForm"
                    class="nice custom" enctype="multipart/form-data" >
                    <div class="box_c_content" id="setting_main_div">
                        <div id="error_div" class="alert-message error" style="display: none;"></div>
                        <p class="" style="float: right; padding: 10px;">
                            (<span class="red">* </span>) Required Fields
                        </p>
                        <div class="row">
                            <div class="twelve columns">
                                <div class="box_c">
                                    <div class="box_c_headingInner cf">
                                       <div class="heading_title">
                                            Site Settings
                                            
                                        </div>
                                         <div class="open_close_image">
                                           <img src="<?php echo IMAGE_URL; ?>blank.gif" class="open_class" alt="Open" />
                                            
                                        </div>
                                    </div>
                                    <div class="box_c_content form_a desc_div">
                                        <div class="formRow">
                                            <label for="txtSiteUrl">
                                                Site URL</label><span class="red">*</span>
                                            <input name="siteurl" type="text" id="siteurl" maxlength="100" class="input-text large required validURL" value="<?php echo $data->siteurl;?>" title="Please Enter Site URL." />
                                        </div>
                                        <div class="formRow">
                                            <label for="txtInfoEmail">
                                                Site Email </label><span class="red">*</span>
                                            <input name="siteemail" type="text" id="siteemail" size="30" maxlength="250" class="input-text large required validEmail" value="<?php echo $data->siteemail;?>" title="Please Enter Site Email." />
                                        </div>
                                        <div class="formRow">
                                            <label for="txtCurrency">
                                                Currency</label><span></span>
                                            <input name="currency" type="text" id="currency" size="30" maxlength="20" class="input-text large" value="<?php echo $data->currency;?>" />
                                        </div>
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="twelve columns">
                                <div class="box_c">
                                    <div class="box_c_headingInner cf"> 
                                        <div class="heading_title">
                                             Tax Settings
                                        </div>
                                        <div class="open_close_image">
                                            <img src="<?php echo IMAGE_URL; ?>blank.gif" class="open_class" alt="Open" />
                                        </div>
                                        
                                    </div>
                                    <div class="box_c_content form_a desc_div">
                                        <div class="formRow">
                                            <label for="GST">
                                                GST (%)</label><span class="red">*</span>
                                            <input name="gst" type="text" id="gst" size="30" maxlength="250" class="input-text large required" value="<?php echo $data->gst;?>"   title="Please Enter GST (%)." />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="twelve columns">
                                <div class="box_c">
                                    <div class="box_c_headingInner cf">
                                    <div class="heading_title">
                                            Metadata Settings
                                            
                                        </div>
                                         <div class="open_close_image">
                                          <img src="<?php echo IMAGE_URL; ?>blank.gif" class="open_class" alt="Open" />
                                            
                                        </div>
                                       
                                    </div>
                                    <div class="box_c_content form_a desc_div">
                                        <div class="formRow">
                                            <label for="txtMetaKeywords">
                                                Meta Keywords</label><span class="red">*</span>
                                            <textarea name="meta_keywords" id="meta_keywords" rows="3" cols="50" style="resize: none;" class="large required" title="Please Enter Meta Keywords."><?php echo $data->meta_keywords;?></textarea>
                                        </div>
                                        <div class="formRow">
                                            <label for="txtMetaTitle">
                                                Meta Title</label><span class="red">*</span>
                                            <input name="meta_title" type="text" id="meta_title" size="30" maxlength="250" class="input-text large required" value="<?php echo $data->meta_title;?>"  title="Please Enter Meta Title." />
                                        </div>
                                        <div class="formRow">
                                            <label for="txtMetaDesc">
                                                Meta Description</label><span class="red">*</span>
                                            <textarea name="meta_description" id="meta_description" rows="3" cols="50" style="resize: none;" class="large required" title="Please Enter Meta Description."><?php echo $data->meta_description;?></textarea>
                                        </div>
                                        
                                        <div class="formRow">
                                            <label for="txtAnalyticCode">
                                                Analytic Code</label><span></span>
                                            <textarea name="analytic_code" id="analytic_code" rows="3" cols="50" style="resize: none;" class="large"><?php echo $data->analytic_code;?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="twelve columns">
                                <div class="box_c">
                                    <div class="box_c_headingInner cf">
                                    <div class="heading_title">
                                            Social Media Link Settings
                                            
                                        </div>
                                         <div class="open_close_image">
                                          <img src="<?php echo IMAGE_URL; ?>blank.gif" class="open_class" alt="Open" />
                                            
                                        </div>
                                        
                                        
                                    </div>
                                    <div class="box_c_content form_a desc_div">
                                        <div id="Div2" style="margin: 0 auto" class="chart_flw" title="Bar chart">
                                            <div class="formRow">
                                                <label for="txtFacebook">
                                                    Facebook Link</label><span></span>
                                                <input name="facebook_link" type="text" id="facebook_link" size="30" maxlength="250" class="input-text large" value="<?php echo $data->facebook_link;?>" />
                                            </div>
                                            <div class="formRow">
                                                <label for="txtTwitter">
                                                    Twitter Link</label><span></span>
                                                <input name="twitter_link" type="text" id="twitter_link" size="30" maxlength="250" class="input-text large" value="<?php echo $data->twitter_link;?>" />
                                            </div>
                                            <div class="formRow">
                                                <label for="txtLinkedin">
                                                    Google Link</label><span></span>
                                                <input name="linkedin_link" type="text" id="linkedin_link" size="30" maxlength="250" class="input-text large" value="<?php echo $data->linkedin_link;?>" />
                                            </div>
                                            <div class="formRow">
                                                <label for="txtinstagram">
                                                    Instagram Link</label><span></span>
                                                <input name="instagram_link" type="text" id="instagram_link" size="30" maxlength="250" class="input-text large" value="<?php echo $data->instagram_link;?>" />
                                            </div>
                                            <div class="formRow">
                                                <label for="txtinstagram">
                                                    Pinterest Link</label><span></span>
                                                <input name="pinterest_link" type="text" id="pinterest_link" size="30" maxlength="250" class="input-text large" value="<?php echo $data->pinterest_link;?>" />
                                            </div>
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="twelve columns">
                                <div class="box_c">
                                    <div class="box_c_headingInner cf">
                                       <div class="heading_title">
                                            General Settings
                                            
                                        </div>
                                         <div class="open_close_image">
                                          <img src="<?php echo IMAGE_URL; ?>blank.gif" class="open_class" alt="Open" />
                                            
                                        </div>
                                        
                                    </div>
                                    <div class="box_c_content form_a desc_div">
                                        <div class="formRow">
                                            <label for="txtFooterText">
                                                Signature</label><span></span>
                                             <textarea name="footer_copyrights_text" id="footer_copyrights_text" rows="3" cols="50" style="resize: none;" class="large"><?php echo stripslashes($data->footer_copyrights_text);?></textarea>
                                        </div>
                                        <div class="formRow">
                                            <label for="txtFooterText">
                                                Bank Transfer Text</label><span></span>
                                             <textarea name="bank_transfer" id="bank_transfer" rows="3" cols="50" style="resize: none;" class="large"><?php echo stripslashes($data->bank_transfer);?></textarea>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="row">
                            <div class="twelve columns">
                                <div class="box_c">
                                    <div class="box_c_headingInner cf"> <div class="heading_title">
                                             Payment Settings
                                            
                                        </div>
                                         <div class="open_close_image">
                                          <img src="<?php echo IMAGE_URL; ?>blank.gif" class="open_class" alt="Open" />
                                            
                                        </div>
                                        
                                    </div>
                                    <div class="box_c_content form_a desc_div">
                                    <div class="formRow on_off_paypal">
                                        <h1 style="font-size:2.6rem;">Paypal Payment Gateway</h1>
                                            <label for="paypalstatus"><?php // echo $data->issmtp;?>
                                               Do you want to enable Paypal payment gateway?
                                               <input type="hidden" name="paypal_status" id="paypal_status" value="<?php if($data->paypal_status) echo "1"; else echo "0";?>" /></label>
                                            <input name="paypal_status1" value="" type="checkbox" id="paypal_status1" <?php if(isset($data->paypal_status) && trim($data->paypal_status)=="1") echo 'checked="checked"'; ?>  />
                                            
                                        </div>
                                        <div id="paypal_content_id">
                                        <div class="formRow">
                                            <label for="radioGatewayServer">
                                                Gateway Server</label><span class="red">*</span> <br/>
                                            <input type="radio" value="Sandbox" <?php if($data->paypal_gateway_server == "Sandbox") echo 'checked="checked"';  ?> name="paypal_gateway_server"/> Sandbox
                                            <input type="radio" value="Live" <?php if($data->paypal_gateway_server == "Live") echo 'checked="checked"';  ?> name="paypal_gateway_server"/> Live
<!--                                            <input name="smtpemail" type="text" id="smtpemail" size="30" maxlength="250" class="input-text large required" value="<?php echo $data->smtpemail;?>"   title="Please Enter Email Address." />-->
                                        </div>
                                        <div class="formRow">
                                            <label for="txtEmailaddress">
                                               Paypal Email Address</label><span class="red">*</span>
                                            <input name="paypal_email_address" type="text" id="paypal_email_address" size="30" maxlength="250" class="input-text large required" value="<?php echo $data->paypal_email_address ;?>"   title="Please Enter Email Address." />
                                        </div>
                                        <div class="formRow">
                                            <label for="txtPassword">
                                                Paypal URL</label><span class="red">*</span>
                                            <input name="paypal_url" type="text" id="paypal_url" size="30" maxlength="1000" class="input-text large required" value="<?php echo $data->paypal_url;?>"   title="Please Enter Paypal URL." />
                                        </div>
                                            <br/>
                                        </div>
                                         
                                         
                                    </div>
                                </div>
                            </div>
                        </div>
   <!--                        6-6-2013 k end -->
                        
                          <div class="formRow1">                        
                        <input name="btnSave" type="button" id="btnSave" class="button small radius" value="Save Information" />
                         
                    </div>
                    </div>
                 
                    <div style="clear: both;">
                    </div>
                    </form>
                </div>
            </div>
      
        </div>
    </div>
<?php
	include("footer.php");
?>   
