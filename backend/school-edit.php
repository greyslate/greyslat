<?php
include("header.php");

if (!isset($_GET['id'])) {
    header("location: school-list.php");
    exit(0);
}

$id = $_GET['id'];
$val = $school_obj->getSchool($id);

$country_id = $val->country;
$state_id = $val->state;
$city_id = $val->city;


//$country_list = FillCombo($country_obj->fetch_country_list(), "id", "country_name", $val->country);
//$state_list = FillCombo($state_obj->fetch_state_list($country_id), "id", "state_name", $state_id);
//$city_list = FillCombo($city_obj->fetch_city_list($state_id), "id", "city_name", $city_id);

$type_list = fillArrayCombo($config["Institute_Type"], $val->institute_type);
?>

<script>
    $(document).ready(function () {
        $("#frmschool").validate({
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            invalidHandler: function (event, validator) { //display error alert on form submit              
                $('.alert-danger-transparent').show();
                $('html, body').animate({
                    scrollTop: $("body").offset().top
                }, 2000);

            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('error'); // set error class to the control group
            },
            success: function (label) {
                label
                        .closest('.form-group').removeClass('error'); // set success class to the control group
            },
            submitHandler: function (form) {

                $("#loader").show();
                var formData = new FormData(form);

                $.ajax({
                    url: base_url + 'ajaxfiles/school-addedit.php',
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $("#loader").hide();
                        if (data.trim() == 'Yes') {
                            toastr.success("Record has been updated successfully", "Success!");
                            $("html, body").animate({scrollTop: 0}, 500);
                            setTimeout(function () {
                                window.location.href = "school-list.php"; //will redirect to your blog page (an ex: blog.html)
                            }, 1000);
                        }
                        else {
                            toastr.error(data, "Oops!");
                            return false;
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        $("#loader").hide();
                        toastr.error("Something went wrong.Please contact to Developer.", "Oops!");
                    }
                });

            }
        });
    });

</script>
<div class="content">
    <div class="page-header full-content bg-blue">
        <div class="row">
            <div class="col-sm-6">
                <h1>School Management <small>Edit School</small></h1>
            </div><!--.col-->
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="#"><i class="ion-home"></i></a></li>
                    <li><a href="index.php">Dashboard</a></li>
                    <li><a href="school-list.php">View School</a></li>
                    <li><a href="#" class="active">Edit School</a></li>
                </ol>
            </div><!--.col-->
        </div><!--.row-->
    </div>
    <form name="frmschool" id="frmschool"  method="post" class="form-horizontal" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-12">
                <div style="display:none" class="alert alert-danger-transparent">
                    <strong>Oops!</strong> Please correct following errors.
                </div>
            </div><!--.col-md-12-->
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="panel-title"><h4>SCHOOL INFORMATION</h4></div>
                    </div><!--.panel-heading-->
                    <div class="panel-body">


                        <div class="form-content">
                            <div class="form-group">
                                <label class="control-label col-md-3">School Name <span class="text-red">*</span></label>
                                <div class="col-md-5">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input value="<?php echo $val->institute_name; ?>" type="text" name="txtinstitutename" id="txtinstitutename" class="form-control required" placeholder="School Name">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-3">School Description </label>
                                <div class="col-md-5">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <textarea name="txtdesc" id="txtdesc" class="form-control" placeholder="Description"><?php echo $val->school_description; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">School Type</label>
                                <div class="col-md-5">
                                    <select name="school_type" id="school_type" class="selecter required">
                                        <?php
                                        echo $type_list;
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Address <span class="text-red">*</span></label>
                                <div class="col-md-5">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <textarea name="txtaddress" id="txtaddress" class="form-control required" placeholder="Address"><?php echo $val->address; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Principal Name <span class="text-red">*</span></label>
                                <div class="col-md-5">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input value="<?php echo $val->principal_name;?>" type="text" name="txtprincipalname" id="txtprincipalname" class="form-control required" placeholder="Principal Name">
                                        </div>
                                    </div>
                                </div>
                            </div>

                             <div class="form-group">
                                <label class="control-label col-md-3">School Teaching Medium <span class="text-red">*</span></label>
                                <div class="col-md-5">
                                   <div class="input-wrapper">
                                            <input type="checkbox"  name="teach_medium[]" value="Hindi" />Hindi
                                            <input type="checkbox"  name="teach_medium[]" value="Gujarati" />Gujarati  
                                            <input type="checkbox"  name="teach_medium[]" value="English" />English
                                            <input type="checkbox"  name="teach_medium[]" value="Marathi" />Marathi
                                       
                                    </div>
                                </div>
                            </div>

                           <div class="form-group">
                                <label class="control-label col-md-3">Education Board <span class="text-red">*</span></label>
                                <div class="col-md-5">
                                   <div class="input-wrapper">
                                            <input type="checkbox"  name="edu_board[]" value="CBSE" />CBSE
                                            <input type="checkbox"  name="edu_board[]" value="ISCSE" />ISCSE  
                                            <input type="checkbox"  name="edu_board[]" value="GESB" />GSEB
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">School Class Standard</label>
                                <div class="col-md-5">
                                    <select name="school_standard" id="school_standard" class="form-control required">
                                       <option value="">Select your Class Standard</option>
                                       <option <?php if($val->school_standard == "Pre-School") { echo "selected"; } ?> value="Pre-School">Pre-School</option>
                                       <option <?php if($val->school_standard == "Standard-1") { echo "selected"; } ?> value="Standard-1">Standard-1</option>
                                       <option <?php if($val->school_standard == "Standard-2") { echo "selected"; } ?> value="Standard-2">Standard-2</option>
                                       <option <?php if($val->school_standard == "Standard-3") { echo "selected"; } ?> value="Standard-3">Standard-3</option>
                                       <option <?php if($val->school_standard == "Standard-4") { echo "selected"; } ?> value="Standard-4">Standard-4</option>
                                       <option <?php if($val->school_standard == "Standard-5") { echo "selected"; } ?> value="Standard-5">Standard-5</option>
                                       <option <?php if($val->school_standard == "Standard-6") { echo "selected"; } ?> value="Standard-6">Standard-6</option>
                                       <option <?php if($val->school_standard == "Standard-7") { echo "selected"; } ?> value="Standard-7">Standard-7</option>
                                       <option <?php if($val->school_standard == "Standard-8") { echo "selected"; } ?> value="Standard-8">Standard-8</option>
                                       <option <?php if($val->school_standard == "Standard-9") { echo "selected"; } ?> value="Standard-9">Standard-9</option>
                                       <option <?php if($val->school_standard == "Standard-10") { echo "selected"; } ?> value="Standard-10">Standard-10</option>
                                       <option <?php if($val->school_standard == "Standard-11") { echo "selected"; } ?> value="Standard-11">Standard-11</option>
                                       <option <?php if($val->school_standard == "Standard-12") { echo "selected"; } ?> value="Standard-12">Standard-12</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">City <span class="text-red">*</span></label>
                                <div class="col-md-5">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="city" id="city" class="form-control required" placeholder="City" value="<?php echo $val->city; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3">State <span class="text-red">*</span></label>
                                <div class="col-md-5">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="state" id="state" class="form-control required" placeholder="State" value="<?php echo $val->state; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Country <span class="text-red">*</span></label>
                                <div class="col-md-5">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="country" id="country" class="form-control required" placeholder="Country" value="<?php echo $val->country; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3">Postal Code <span class="text-red">*</span></label>
                                <div class="col-md-5">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="txtpostalcode" id="txtpostalcode" class="form-control required" placeholder="Postal Code" value="<?php echo $val->postalcode; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Phone No <span class="text-red">*</span></label>
                                <div class="col-md-5">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="txtphone" id="txtphone" class="form-control required" placeholder="Phone No" value="<?php echo $val->phone_no; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Email <span class="text-red">*</span></label>
                                <div class="col-md-5">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="email" name="txtemail" id="txtemail" class="form-control required" placeholder="Email" value="<?php echo $val->institute_email; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3">School Logo</label>
                                <div class="col-md-5">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="file" name="institute_logo" id="institute_logo" title="Please Select Institute Logo.">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php
                            if ($val->institute_logo != "") {
                                $image_url = UPLOAD_URL . $val->school_path . $val->institute_logo;
                                ?>
                                <div class="form-group">
                                    <label class="control-label col-md-3"></label>
                                    <div class="col-md-5">
                                        <div class="inputer">
                                            <div class="input-wrapper">

                                                <img width="100" src="<?php echo $image_url; ?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <input type="hidden" name="status" id="status" value="1" >    
                            <!--                            <div class="form-group">
                                                            <label class="control-label col-md-3">Status</label>
                                                            <div class="col-md-5">
                                                                <div class="radioer">
                                                                    <input type="radio" name="status" id="status" value="1" checked="checked">
                                                                    <label for="radioColor3">Active</label>
                                                                </div>
                                                                <div class="radioer">
                                                                    <input type="radio" name="status" id="status" value="0">
                                                                    <label for="radioColor4">De-active</label>
                                                                </div>
                                                            </div>
                                                        </div>-->

                         <div class="form-group">
                                <label class="control-label col-md-3">School Achievements <span class="text-red">*</span></label>
                                <div class="col-md-5">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <textarea name="txtachievement" id="txtachievement" class="form-control required" placeholder="School Achievements"><?php echo $val->school_achievement;?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                             <div class="form-group">
                                <label class="control-label col-md-3">Allow Notification to Teacher <span class="text-red">*</span></label>
                                <div class="col-md-5">
                                    <div class="inputer">
                                        <input type="radio" name="allow_teacher_notification" value="Yes">Yes
                                        <input type="radio" name="allow_teacher_notification" value="No">No
                                    </div>
                                </div>
                            </div>



                        </div>

                        <div class="form-buttons">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <input type="hidden" name="action" value="add">
                                    <button type="submit" class="btn btn-blue">Submit</button>
                                    <button onclick="location.href = 'school-list.php'" type="button" class="btn btn-flat btn-default">Cancel</button>
                                    <input type="hidden" name="action" value="edit">
                                    <input type="hidden" name="id" value="<?php echo $val->id; ?>">
                                    <input type="hidden" name="oimg" value="<?php echo $val->institute_logo; ?>">
                                </div>
                            </div>
                        </div>

                    </div><!--.panel-body-->
                </div><!--.panel-->

            </div><!--.col-md-12-->
        </div>
    </form>
</div>

<?php
include("footer.php");
?>