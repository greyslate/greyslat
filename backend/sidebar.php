<div class="layer-container">
    <!-- BEGIN MENU LAYER -->
    <div class="menu-layer">
        <ul>
            <li data-open-after="true">
                <a href="index.php">Dashboard</a>
            </li>
            <li>
                <a href="javascript:;">School</a>
                <ul class="child-menu">
                    <li><a href="school-add.php">Add School</a></li>
                    <li><a href="school-list.php">View School</a></li>
                </ul>
            </li>
            
        </ul>
    </div><!--.menu-layer-->
</div><!--.layer-container-->