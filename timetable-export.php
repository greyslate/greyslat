<?php

    include("includes/config-frontend.php");
    $objPHPExcel = new PHPExcel();
   


    $sql = "Select * From ".TIME_TABLE." AS tt
                INNER JOIN ".COURSE_MASTER." AS CM on CM.id = tt.course_id
                INNER JOIN ".STANDARD_MASTER." AS StM on StM.id = tt.grade_id
                INNER JOIN ".SECTION_MASTER." AS SM on SM.id = tt.section_id

                order by tt.id desc";


    $db->query($sql);

    $rows = $db->fetch_object();

    $i = 0;
    $main_arr = array();
    if(count($rows) > 0){
        foreach($rows as $val){

            

            $main_arr[$i]["course"]     = $val->course_name;
            $main_arr[$i]["class"]      = $val->standard_name. " " . $val->section_name;
            
            $main_arr[$i]["created_on"] = $val->created_on;
            $i++;
        }
    }


    $rowCount = 1;
    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "Course Name");
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Grade with Section");
    
    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Created On");


    $rowCount = 2;
    for($i=0;$i<count($main_arr);$i++){
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $main_arr[$i]['course']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $main_arr[$i]['class']);
       
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $main_arr[$i]['created_on']);

        $rowCount++;
        //pr($objPHPExcel); 
    }

    header('Content-Type: application/vnd.openxmlformats-   officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.date("d-m-Y").'-timetable.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

?>