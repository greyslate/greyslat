<?php
include("../includes/config-frontend.php");

if (isset($_POST['action']) && $_POST['action'] == "add") {
    
    $flag    = false; 
    
    $error_text = '';

    if ($flag == false) {

      $title       = $_REQUEST['txttitle'];
      $desc        = $_REQUEST['txtdesc'];
      $uploaded_by = $_REQUEST['txtuploadedby'];
      $NewImageName = '';
      if (isset($_FILES['file_name']['name']) && $_FILES['file_name']['name'] != "") 
        {

            //==== Image Uploading
            $RandomNum = time();
            $ImageName = str_replace(' ', '-', strtolower($_FILES['file_name']['name']));
            $ImageType = $_FILES['file_name']['type']; //"image/png", image/jpeg etc.
            $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
            $ImageExt = str_replace('.', '', $ImageExt);
            $ImageName = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
            $NewImageName = $ImageName . '-' . $RandomNum . '.' . $ImageExt;
            move_uploaded_file($_FILES["file_name"]["tmp_name"], DOCUMENT_PATH . 
              $NewImageName);
        }
        
        $arr = array(
                       //"school_id"     => $_SESSION['institute_id'],
                        "title"         => $title,
                        "file_path"     => $_SESSION["db_name"]."/document/",
                        "file_name"     => $NewImageName,
                        "created_on"    => date("Y-m-d H:i:s"),
                        "uploaded_by"   =>$uploaded_by,
                        "status"        =>isset($_REQUEST['status']) ? 1 : 0,
                    );

      $result = InsertData($arr, DOCUMENT_MASTER);
       echo "Yes";
        
    } else {
        echo $error_text;
    }
}

if (isset($_POST['action']) && $_POST['action'] == "edit") {

    $flag        = false;
    $id          = $_REQUEST['id'];
  
  
    $error_text = '';

    if ($flag == false) {
        $title       = $_REQUEST['txttitle'];
        $uploaded_by = $_REQUEST['txtuploadedby'];
       $NewImageName = '';
       if (isset($_FILES['file_name']['name']) && $_FILES['file_name']['name'] != "") {

           @unlink(DOCUMENT_PATH.$_REQUEST['oimg']);
           //==== Image Uploading
           $RandomNum = time();
           $ImageName = str_replace(' ', '-', strtolower($_FILES['file_name']['name']));
           $ImageType = $_FILES['file_name']['type']; //"image/png", image/jpeg etc.
           $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
           $ImageExt = str_replace('.', '', $ImageExt);
           $ImageName = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
           $NewImageName = $ImageName . '-' . $RandomNum . '.' . $ImageExt;
           move_uploaded_file($_FILES["file_name"]["tmp_name"], DOCUMENT_PATH . 
            $NewImageName);
       }
       else {
           $NewImageName = $_REQUEST['oimg'];
       }

       $arr = array(
                    "title"          => $title,
                    "file_name"      => $NewImageName,
                    "file_path"      => $_SESSION["db_name"]."/document/",
                    "uploaded_by"    => $uploaded_by,
                    "status"         => isset($_REQUEST['status']) ? 1 : 0,
                   );

       $where_arr = array("id" => $id);            
       $result = UpdateData($arr, DOCUMENT_MASTER,$where_arr);
      echo "Yes";
        
    } else {
        echo $error_text;
    }
    
}
?>