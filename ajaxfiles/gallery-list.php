<?php

include("../includes/config-frontend.php");

$cobj = new Gallery();
$cgobj = new GalleryDetail();

if (isset($_REQUEST['start'])){
    $_SESSION['start_record'] = $_REQUEST['start'];
}

if ((isset($_REQUEST['action']) && $_REQUEST['action'] == "delete") && (isset($_REQUEST['id']) && $_REQUEST['id'] != "")) {
    
    $wheres = " And gallery_id = '".$_REQUEST['id']."' ";
    $gdetail = $cobj->get_all_gallery($wheres);
    if(count($gdetail) > 0){
        foreach($gdetail as $gval){
            $ginfo = $cgobj->getGalleryDetail($gval->id);
            @unlink(GALLERY_PATH.$ginfo->image_name);
            $cgobj->DeleteGalleryDetail($gval->id);
        }
    }
    
    $cobj->DeleteGallery($_REQUEST['id']);
       
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Delete") {
    if (isset($_POST['id'])) {
        foreach ($_POST['id'] as $val) {
            $wheres = " And gallery_id = '".$val."' ";
            $gdetail = $cobj->get_all_gallery($wheres);
            if(count($gdetail) > 0){
                foreach($gdetail as $gval){
                    $ginfo = $cgobj->getGalleryDetail($gval->id);
                    @unlink(GALLERY_PATH.$ginfo->image_name);
                    $cgobj->DeleteGalleryDetail($gval->id);
                }
            }
            $cobj->DeleteGallery($val);
        }
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Active" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 1,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr, GALLERY_MASTER, $where_arr);
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Inactive" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 0,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr, GALLERY_MASTER, $where_arr);
    }
}

if (isset($_POST['action']) && $_POST['action'] == "changestatus") {
    $status = $_POST['status'];
    $id = $_POST['id'];

    if ($status == 1) {
        $st = 0;
    } else {
        $st = 1;
    }

    $arr = array(
        "status" => $st,
    );

    $where_arr = array("id" => $id);
    $result = UpdateData($arr, GALLERY_MASTER, $where_arr);
}

$where = "";

if (isset($_POST['task']) && $_POST['task'] == "search") {
    if (isset($_POST['tbxTitle']) && $_POST['tbxTitle'] != "") {
        $where .= " And gallery_title like '%" . $_POST['tbxTitle'] . "%'";
    }
    if (isset($_POST['drpsearch_status']) && $_POST['drpsearch_status'] != "") {
        $where .= " And status = '" . $_POST['drpsearch_status'] . "'";
    }
}


$data = $cobj->fetch_all($where);

//=== Get Gallery Count
$gsql = "Select gallery_id,count(*) As cnt From ".GALLERY_DETAIL." group by gallery_id ";
$q = $db->query($gsql);
$qrow = $db->fetch_object();

$gallery_arr = array();
if(count($qrow) > 0){
    foreach($qrow as $val){
        $gallery_arr[$val->gallery_id] = $val->cnt;
    }
}

//=== Get Main Picture
$gsql = "Select gallery_id,image_name From ".GALLERY_DETAIL." where is_cover_pic = '1' group by gallery_id ";
$q = $db->query($gsql);
$qrow = $db->fetch_object();

$gallery_main_pic = array();
if(count($qrow) > 0){
    foreach($qrow as $val){
        $gallery_main_pic[$val->gallery_id] = $val->image_name;
    }
}
?>

<table class="table table-bordered table-striped table-condensed flip-content" >
    <thead>
        <tr>
            <th class="chb_col" width="1">
                <input type="checkbox" name="checkall" onclick="javascript:checkAlluncheckAll(frm);">
            </th>
            <th width="20%">
               Cover Picture
            </th>
            <th width="20%">
                Gallery Title
            </th>
            <th width="20%">
                Gallery
            </th>
            <th>
                Created On
            </th>
            <th>
                Updated On
            </th>
            <th class="center" width="5%" style="text-align: center;">
                Status
            </th>
            <th width="10%" style="text-align: center;">
                Action
            </th>
        </tr>
    </thead>

    <tbody id="content ui-sortable">
    <?php 

        if(count($data) == 0){
        ?>
            <tr><td colspan="8" align="center">No Result found</td></tr>
        <?php
        }
        else{
            foreach($data as $val){ 
            ?>
                <tr>
                    <td class="chb_col">
                        <input type="checkbox" name="id[]" value="<?php echo $val->id; ?>">
                    </td>
                    <td>
                        <?php  

                            if(isset($gallery_main_pic[$val->id])){
                                ?>
                        <img width="100" src="<?php echo GALLERY_URL.$gallery_main_pic[$val->id]; ?>" />
                                <?php
                            }

                        ?>
                    </td>
                    <td>
                        <?php echo $val->gallery_title; ?>
                    </td>
                    <td>
                        <a href="gallery-detail.php?gid=<?php echo $val->id; ?>">Add Gallery</a>
                        (<?php echo isset($gallery_arr[$val->id]) ? $gallery_arr[$val->id] : 0; ?>)
                    </td>
                    <td>
                        <?php echo $val->created_on; ?>
                    </td>
                    <td>
                        <?php echo $val->updated_on; ?>
                    </td>

                    <td id="td_status_<?php echo $val->id; ?>" style="text-align: center;">

                        <a href="javascript:;" onclick="ChangeStatus('<?php echo $val->status; ?>','<?php echo $val->id; ?>')"

                            title='<?php echo ($val->status == 1 ? "Active"  : "Inactive"); ?>'>

                            <img  src='<?php echo IMAGE_URL; ?><?php

                            echo ($val->status == 1 ? "ico-1.gif"  : "ico-0.gif");

                            ?>

                            ' alt='<?php echo ($val->status == 1 ? "Active"  : "Inactive"); ?>'

                              title='<?php echo ($val->status == 1 ? "Active"  : "Inactive"); ?>' />

                        </a>

                    </td>
                    <td class="content_actions" style="text-align: center;">
                        <a href="javascript:;" onclick="javascript:RemoveRecord('<?php echo $val->id; ?>')" title='Delete'>
                            <img src="<?php echo IMAGE_URL; ?>ico/trashcan_gray.png" alt="" /></a> <a href="gallery-edit.php?id=<?php echo $val->id; ?>" class="sepV_a" title="Edit">
                            <img src="<?php echo IMAGE_URL; ?>ico/pencil_gray.png" alt="" />
                        </a>
                    </td>
                </tr>
             <?php 
            }
        }
    ?>
    </tbody>
</table>
    
<?php 
include("../pagination.php"); 
?>