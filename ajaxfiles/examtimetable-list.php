<?php
include("../includes/config-frontend.php");

$cobj = new ExamTimeTable();
if (isset($_REQUEST['start'])) {
    $_SESSION['start_record'] = $_REQUEST['start'];
}

if ((isset($_REQUEST['action']) && $_REQUEST['action'] == "delete") && (isset($_REQUEST['id']) && $_REQUEST['id'] != "")) {
    $cobj->DeleteExamTimeTable($_REQUEST['id']);
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Delete") {
    if (isset($_POST['id'])) {
        foreach ($_POST['id'] as $val) {
            $cobj->DeleteExamTimeTable($val);
        }
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Active" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 1,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr, EXAM_TIMETABLE, $where_arr);
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Inactive" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 0,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr, EXAM_TIMETABLE, $where_arr);
    }
}

if (isset($_POST['action']) && $_POST['action'] == "changestatus") {
    $status = $_POST['status'];
    $id = $_POST['id'];

    if ($status == 1) {
        $st = 0;
    } else {
        $st = 1;
    }

    $arr = array(
        "status" => $st,
    );

    $where_arr = array("id" => $id);
    $result = UpdateData($arr, EXAM_TIMETABLE, $where_arr);
}

$where = '';
if (isset($_POST['task']) && $_POST['task'] == "search") {
    if (isset($_POST['tbxTitle']) && $_POST['tbxTitle'] != "") {
        $where .= " And TT.exam_title like '%" . $_POST['tbxTitle'] . "%'";
    }

    if (isset($_POST['drpsearch_status']) && $_POST['drpsearch_status'] != "") {
        $where .= " And TT.status = '" . $_POST['drpsearch_status'] . "'";
    }
    if (isset($_POST['grade_id']) && $_POST['grade_id'] != "") {
        $where .= " And TT.grade_id = '" . $_POST['grade_id'] . "'";
    }
    if (isset($_POST['medium']) && $_POST['medium'] != "") {
        $where .= " And TT.medium = '" . $_POST['medium'] . "'";
    }
    if (isset($_POST['start_date']) && $_POST['start_date'] != "") {
        $where .= " And TT.start_date >= '" . $_POST['start_date'] . "'";
    }
    if (isset($_POST['end_date']) && $_POST['end_date'] != "") {
        $where .= " And TT.end_date <= '" . $_POST['end_date'] . "'";
    }
}

$data = $cobj->fetch_all($where);
?>
<table class="table table-bordered table-striped table-condensed flip-content" >
    <thead>
        <tr>
            <th class="chb_col" width="1%" >
                <input type="checkbox" name="checkall" onclick="javascript:checkAlluncheckAll(frm);">
            </th>
            <th width="20%">
                Exam Title
            </th>
            <th>
                Grade/Standard
            </th>

            <th>
                Medium
            </th>

            <th>
                Start Date
            </th>
            <th>
                End Date
            </th>
            <th>
                View File
            </th>
            <th class="center" width="5%" style="text-align: center;">
                Status
            </th>

            <th width="15%" style="text-align: center;">
                Action
            </th>
        </tr>
    </thead>

    <tbody id="content ui-sortable">

        <?php
        if (count($data) == 0) {
            ?>
            <tr><td colspan="8" align="center">No Result found</td></tr>
            <?php
        } else {
            foreach ($data as $val) {
                ?>
                <tr>
                    <td class="chb_col">
                        <input type="checkbox" name="id[]" value="<?php echo $val->id; ?>">
                    </td>
                    <td>
                        <?php echo $val->exam_title; ?>
                    </td>
                    <td>
                        <?php echo $val->standard_name; ?>
                    </td>

                    <td>
                        <?php echo $val->medium; ?>
                    </td>

                    <td>
                        <?php echo $val->start_date; ?>
                    </td>
                    <td>
                        <?php echo $val->end_date; ?>
                    </td>
                    <td style="text-align: center;">
                        <a href="<?php echo EXAM_URL . $val->file_name; ?>" >
                            View File
                        </a>
                    </td>
                    <td id="td_status_<?php echo $val->eid; ?>" style="text-align: center;">

                        <a href="javascript:;" onclick="ChangeStatus('<?php echo $val->status; ?>', '<?php echo $val->eid; ?>')"

                            title='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>'>

                            <img  src='<?php echo IMAGE_URL; ?><?php
                                echo ($val->status == 1 ? "ico-1.gif" : "ico-0.gif");
                                ?>

                                ' alt='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>'

                                title='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>' />

                        </a>

                    </td>
                    <td class="content_actions" style="text-align: center;">
                        <a href="javascript:;" onclick="javascript:RemoveRecord('<?php echo $val->eid; ?>')" title='Delete'>
                            <img src="<?php echo IMAGE_URL; ?>ico/trashcan_gray.png" alt="" /></a> <a href="examtimetable-edit.php?id=<?php echo $val->eid; ?>" class="sepV_a" title="Edit">
                            <img src="<?php echo IMAGE_URL; ?>ico/pencil_gray.png" alt="" />
                        </a>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>
<?php
include("../pagination.php");
?>