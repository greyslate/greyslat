<?php

include("../includes/config-frontend.php");

$cobj = new Appointment();
if (isset($_REQUEST['start'])){
    $_SESSION['start_record'] = $_REQUEST['start'];
}

if ((isset($_REQUEST['action']) && $_REQUEST['action'] == "delete") && (isset($_REQUEST['id']) && $_REQUEST['id'] != "")) {
    $cobj->DeleteAppointment($_REQUEST['id']);
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Delete") {
    if (isset($_POST['id'])) {
        foreach ($_POST['id'] as $val) {
            $cobj->DeleteAppointment($val);
        }
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Active" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 1,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr, APPOINTMENT_MASTER, $where_arr);
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Inactive" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 0,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr, APPOINTMENT_MASTER, $where_arr);
    }
}

if (isset($_POST['action']) && $_POST['action'] == "changestatus") {
    $status = $_POST['status'];
    $id = $_POST['id'];

    if ($status == 1) {
        $st = 0;
    } else {
        $st = 1;
    }

    $arr = array(
        "status" => $st,
    );

    $where_arr = array("id" => $id);
    $result = UpdateData($arr, APPOINTMENT_MASTER, $where_arr);
}

$where = "";

if (isset($_POST['task']) && $_POST['task'] == "search") {
    if (isset($_POST['tbxTitle']) && $_POST['tbxTitle'] != "") {
        $where .= " And title like '%" . $_POST['tbxTitle'] . "%'";
    }
    if (isset($_POST['drpsearch_status']) && $_POST['drpsearch_status'] != "") {
        $where .= " And status = '" . $_POST['drpsearch_status'] . "'";
    }
}


$data = $cobj->fetch_all($where);

?>

<table class="table table-bordered table-striped table-condensed flip-content" >
    <thead>
        <tr>
            <th class="chb_col" width="1" >
                <input type="checkbox" name="checkall" onclick="javascript:checkAlluncheckAll(frm);">
            </th>
            <th width="20%">
                Appointment Title
            </th>
            <th width="20%">
                Student Name
            </th>
            <th width="20%">
                Appointment Date
            </th>
            <th width="20%">
                Appointment Time
            </th>
            <th>
                Posted On
            </th>
           <th>
               Status
            </th >
            <th width="10%" style="text-align: center;">
                Action
            </th>
        </tr>
    </thead>

    <tbody id="content ui-sortable">

    <?php 

        if(count($data) == 0){
        ?>
            <tr><td colspan="8" align="center">No Result found</td></tr>
        <?php
        }
        else{
            foreach($data as $val){

                $sinfo = $student_obj->getStudent($val->student_id);
                $student_id = $sinfo->fname." ".$sinfo->lname;

            ?>
                <tr>
                    <td class="chb_col">
                        <input type="checkbox" name="id[]" value="<?php echo $val->id; ?>">
                    </td>
                    <td>
                        <?php echo $val->title; ?>
                    </td>
                    <td>
                        <?php echo $student_id; ?>
                    </td>

                    <td>
                        <?php echo $val->app_date; ?>
                    </td>
                    <td>
                        <?php echo $val->app_time; ?>
                    </td>
                    <td>
                        <?php echo $val->posted_on; ?>
                    </td>
                       <td id="td_status_<?php echo $val->id; ?>" style="text-align: center;">

                                                            <a href="javascript:;" onclick="ChangeStatus('<?php echo $val->status; ?>','<?php echo $val->id; ?>')"

                                                                title='<?php echo ($val->status == 1 ? "Approved"  : "Pending"); ?>'>

                                                                <img  src='<?php echo IMAGE_URL; ?><?php

                                                                echo ($val->status == 1 ? "ico-1.gif"  : "ico-0.gif");

                                                                ?>

                                                                ' alt='<?php echo ($val->status == 1 ? "Approved"  : "Pending"); ?>'

                                                                  title='<?php echo ($val->status == 1 ? "Approved"  : "Pending"); ?>' />

                                                            </a>

                                                        </td>
                    
                    <td class="content_actions" style="text-align: center;">
                        <a href="javascript:;" onclick="javascript:RemoveRecord('<?php echo $val->id; ?>')" title='Delete'>
                            <img src="<?php echo IMAGE_URL; ?>ico/trashcan_gray.png" alt="" /></a> 

                    </td>
                </tr>
             <?php 
            }
        }
    ?>
    </tbody>
</table>
    
<?php 
include("../pagination.php"); 
?>