<?php
include("../includes/config-frontend.php");

if (isset($_POST['action']) && $_POST['action'] == "add") {

    $flag = false;
    $error_text = '';

    if ($flag == false) {
        $class       = $_REQUEST['txtclass'];
        $course      = $_REQUEST['txtcourse'];
        $date        = $_REQUEST['txtdate'];
        $homework    = $_REQUEST['txthomework'];
        
        $arr = array(
                        "class_id"       => $class,
                        "course_id"      => $course,
                        "date"           => $date,
                        "homework"       => $homework,
                    );
        $result = InsertData($arr, HOMEWORK);
        } 
}

if (isset($_POST['action']) && $_POST['action'] == "edit") {

    $id          = $_REQUEST['id'];
    $class       = $_REQUEST['txtclass'];
    $course      = $_REQUEST['txtcourse'];
    $date        = $_REQUEST['txtdate'];
    $homework    = $_REQUEST['txthomework'];
  
    $arr = array(
                    "class_id"    => $class,
                    "course_id"   => $course,
                    "date"        => $date,
                    "homework"    => $homework,
                );
    $where_arr = array("id" => $id);   
    $result = UpdateData($arr, HOMEWORK, $where_arr);
}
?>