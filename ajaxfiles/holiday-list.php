<?php
include("../includes/config-frontend.php");

$cobj = new Holiday();
if (isset($_REQUEST['start'])) {
    $_SESSION['start_record'] = $_REQUEST['start'];
}

if ((isset($_REQUEST['action']) && $_REQUEST['action'] == "delete") && (isset($_REQUEST['id']) && $_REQUEST['id'] != "")) {
    $cobj->DeleteHoliday($_REQUEST['id']);
}

/*if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Delete") {
    if (isset($_POST['id'])) {
        foreach ($_POST['id'] as $val) {
            $cobj->DeleteHoliday($val);
        }
    }
}*/

/*if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Active" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 1,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr,HOLIDAY_MASTER, $where_arr);
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Inactive" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 0,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr, HOLIDAY_MASTER, $where_arr);
    }
}

if (isset($_POST['action']) && $_POST['action'] == "changestatus") {
    $status = $_POST['status'];
    $id = $_POST['id'];

    if ($status == 1) {
        $st = 0;
    } else {
        $st = 1;
    }

    $arr = array(
        "status" => $st,
    );

    $where_arr = array("id" => $id);
    $result = UpdateData($arr, HOLIDAY_MASTER, $where_arr);
}*/

$where = '';
if (isset($_POST['task']) && $_POST['task'] == "search") {

    if (isset($_POST['drpsearch_year']) && $_POST['drpsearch_year'] != "") {
        $where .= " And HOLIDAY_MASTER.year = '" . $_POST['drpsearch_year'] . "'";
    }

    if (isset($_POST['file_name']) && $_POST['file_name'] != "") {
        $where .= " And HOLIDAY_MASTER.file_name = '" . $_POST['file_name'] . "'";
    }


}

$data = $cobj->fetch_all($where);
?>

    <table class="table table-bordered table-striped table-condensed flip-content" >
        <thead>
        <tr>
            <th class="chb_col" width="1%" >
                <input type="checkbox" name="checkall" onclick="javascript:checkAlluncheckAll(frm);">
            </th>
            <th>
                Year
            </th>
            <th>
                File Download Link
            </th>
            <th width="15%" style="text-align: center;">
                Action
            </th>
        </tr>
        </thead>

        <tbody id="content ui-sortable">

        <?php
        if (count($data) == 0) {
            ?>
            <tr><td colspan="8" align="center">No Result found</td></tr>
            <?php
        } else {
            foreach ($data as $val) {
                ?>
                <tr>
                    <td class="chb_col">
                        <input type="checkbox" name="id[]" value="<?php echo $val->id; ?>">
                    </td>
                    <td>
                        <?php echo $val->year;?>
                    </td>

                    <td>
                        <a href="<?php echo HOLIDAY_URL. $val->file_name; ?>"><?php echo $val->file_name; ?></a>
                    </td>
                    <td class="content_actions" style="text-align: center;">
                        <a href="javascript:;" onclick="javascript:RemoveRecord('<?php echo $val->id; ?>')" title='Delete'>
                            <img src="<?php echo IMAGE_URL; ?>ico/trashcan_gray.png" alt="" /></a> <a href="holiday-edit.php?id=<?php echo $val->id; ?>" class="sepV_a" title="Edit">
                            <img src="<?php echo IMAGE_URL; ?>ico/pencil_gray.png" alt="" />
                        </a>

                    </td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>

<?php
include("../pagination.php");
?>