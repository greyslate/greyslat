<?php

include("../includes/config-frontend.php");

$cobj = new LeaveApplication();



if (isset($_POST['action']) && $_POST['action'] == "add") {

    $flag = false;

    $error_text = '';

    if ($flag == false) {
        $title       = $_REQUEST['txtleavetitle'];
        $desc        = $_REQUEST['txtleavedesc'];
        $type        = $_REQUEST['type_id'];
        $dateTo       = $_REQUEST['txtleavedateTo'];
        $dateFrom       = $_REQUEST['txtleavedateFrom'];

        //$sinfo = $student_obj->getStudent($val->student_id);
        //$student_name = $_REQUEST['txtfname'];
        $arr = array(
            "school_id"     => $_SESSION['institute_id'],
            //"stud_name"     => $_REQUEST['txtstudentname'],
            "title"         => $title,
            "leave_type"         => $type,
            "description"   => $desc,
            "student_id"  => $_REQUEST['student_id'] ? $_REQUEST['student_id'] : NULL ,
            "teacher_id"  => $_SESSION['emp_id'] ? $_SESSION['emp_id'] : NULL ,
            "parent_id" => $_SESSION['emp_id'] ? $_SESSION['emp_id'] : NULL ,
            "leave_date_to"   => $dateTo,
            "leave_date_from" => $dateFrom,
            "posted_on"    => date("Y-m-d H:i:s"),
            "status"        => 0,
        );
       
        if($_SESSION['role_id'] == 2 )
        {
            unset($arr['student_id']);
            unset($arr['parent_id']);
        }
        
        if($_SESSION['role_id'] != 2 )
        {
            unset($arr['teacher_id']);
        }

        $result = InsertData($arr, LEAVE_APPLICATION);

        echo "Yes";
    } 
    else {
        echo $error_text;
    }
}

if (isset($_POST['action']) && $_POST['action'] == "edit") {

    $id             = $_REQUEST['id'];
    $title          = $_REQUEST['txtleavetitle'];
    $desc           = $_REQUEST['txtleavedesc'];
    $dateTo           = $_REQUEST['txtleavedateTo'];
    $dateFrom       = $_REQUEST['txtleavedateFrom'];
    $type        = $_REQUEST['type_id'];


    $arr = array(
        "title"        => $title,
        "description"  => $desc,
        "leave_type"         => $type,
        "leave_date_to"    => $dateTo,
        "leave_date_from" => $dateFrom,
        "status"      =>  isset($_REQUEST['status']) ? 1 : 0,
    );


    $where_arr = array("id" => $id);
    $result = UpdateData($arr, LEAVE_APPLICATION, $where_arr);
    echo "Yes";
}
else {
    echo $error_text;
}


?>