<?php
include("../includes/config-frontend.php");

$email = $_SESSION['email'];
$p_email = $_SESSION['email'];
$cobj = new Assignment();

//$parent_id   = $_REQUEST['parent_id'];

if (isset($_REQUEST['start'])) {
    $_SESSION['start_record'] = $_REQUEST['start'];
}

if ((isset($_REQUEST['action']) && $_REQUEST['action'] == "delete") && (isset($_REQUEST['id']) && $_REQUEST['id'] != "")) {
    $cobj->DeleteAssignment($_REQUEST['id']);
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Delete") {
    if (isset($_POST['id'])) {
        foreach ($_POST['id'] as $val) {
            $cobj->DeleteAssignment($val);
        }
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Active" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 1,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr,ASSIGNMENT_MASTER, $where_arr);
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Inactive" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 0,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr, ASSIGNMENT_MASTER, $where_arr);
    }
}

if (isset($_POST['action']) && $_POST['action'] == "changestatus") {
    $status = $_POST['status'];
    $id = $_POST['id'];

    if ($status == 1) {
        $st = 0;
    } else {
        $st = 1;
    }

    $arr = array(
        "status" => $st,
    );

    $where_arr = array("id" => $id);
    $result = UpdateData($arr, ASSIGNMENT_MASTER, $where_arr);
}
//$cobj = new Assignment();

/*if($_SESSION['email'] == $email){
    $where = "   ";
}*/

/*if( $_SESSION['id']){
    $where = "  And STM.parent_id ='".$_SESSION['id']."' || ASM.emp_id = '".$_SESSION['id']."' ";
}*/

if (isset($_POST['task']) && $_POST['task'] == "search") {


    if (isset($_POST['standard_id']) && $_POST['standard_id'] != "") {
        $where .= " And ASM.grade_id = '" . $_POST['standard_id'] . "'";
    }
    if (isset($_POST['section_id']) && $_POST['section_id'] != "") {
        $where .= " And ASM.section_id = '" . $_POST['section_id'] . "'";
    }
    if (isset($_POST['drpsearch_medium']) && $_POST['drpsearch_medium'] != "") {
        $where .= " And ASM.medium = '" . $_POST['drpsearch_medium'] . "'";
    }
    if (isset($_POST['course_id']) && $_POST['course_id'] != "") {
        $where .= " And ASM.course_id = '" . $_POST['course_id'] . "'";
    }
    if (isset($_POST['txtdate']) && $_POST['txtdate'] != "") {
        $where .= " And ASM.last_date = '" . $_POST['txtdate'] . "'";
    }
}
if($_SESSION['role_id'] == 1 ) {
    $where .= " ";
    $data = $cobj->fetch_all($where);
}
if($_SESSION['role_id'] == 2 ) {
    $where .= " And STM.p_email = '".$_SESSION['email']."' ";
    $data = $cobj->fetch_assignment_list($where);
}
else{
    $where .= "";
    $data = $cobj->fetch_all($where);
}

//$data = $cobj->fetch_assignment_list($where);
//$data = $cobj->fetch_assignment_list($where);
?>

    <table class="table table-bordered table-striped table-condensed flip-content" >
        <thead>
        <tr>
            <th class="chb_col" width="1%" >
                <input type="checkbox" name="checkall" onclick="javascript:checkAlluncheckAll(frm);">
            </th>
            <th>
                Standard
            </th >
            <th>
                Section
            </th >
            <th>
                Medium
            </th >
            <th>
                Course
            </th >
            <th>
                Emp Name
            </th >
            <th>
                Assignment Last Date
            </th >
            <th>
                File Download Link
            </th>
            <th class="center" style="text-align: center;">
                Status
            </th>

        </tr>
        </thead>

        <tbody id="content ui-sortable">

        <?php
        if (count($data) == 0) {
            ?>
            <tr><td colspan="8" align="center">No Result found</td></tr>
            <?php
        } else {
            foreach ($data as $val) {
                $einfo = $emp_obj->getEmployee($val->emp_id);
                $emp_id = $einfo->fname." ".$einfo->lname;

                ?>
                <tr>
                    <td class="chb_col">
                        <input type="checkbox" name="id[]" value="<?php echo $val->id; ?>">
                    </td>
                    <td>
                        <?php echo $val->standard_name;?>
                    </td>
                    <td>
                        <?php echo $val->section_name;?>
                    </td>
                    <td>
                        <?php echo $val->medium;?>
                    </td>
                    <td>
                        <?php echo $val->course_name;?>
                    </td>
                    <td>
                        <?php echo $emp_id;?>
                    </td>
                    <td>
                        <?php echo $val->last_date;?>
                    </td>

                    <td>
                        <a href="<?php echo ASSIGNMENT_URL. $val->file_name; ?>">View File </a>
                    </td>

                    <td id="td_status_<?php echo $val->id; ?>" style="text-align: center;">

                        <a href="javascript:;" onclick="ChangeStatus('<?php echo $val->status; ?>', '<?php echo $val->id; ?>')"

                           title='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>'>

                            <img  src='<?php echo IMAGE_URL; ?><?php
                            echo ($val->status == 1 ? "ico-1.gif" : "ico-0.gif");
                            ?>

                                                                              ' alt='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>'

                                  title='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>' />

                        </a>

                    </td>



                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>

<?php

include("../pagination.php");
?>