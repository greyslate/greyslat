<?php
include("../includes/config-frontend.php");

if (isset($_POST['action']) && $_POST['action'] == "add") {

    $flag = false;

    $error_text = '';

    if ($flag == false) {

        $title      = $_REQUEST['txtapptitle'];
        $student_id = $_REQUEST['student_id'];
        $desc       = $_REQUEST['txtdesc'];
        $date       = $_REQUEST['txtdate'];
        $time       = $_REQUEST['txtapptime'];
        $emp_list   = $_REQUEST['emp_list'];


        $status  = isset($_REQUEST['status']) ? 1 : 0;

        $arr = array(
            "school_id"         => $_SESSION['institute_id'],
            //"parent_id"         => $_SESSION['parent_id'],
            "student_id"        => $_REQUEST['student_id'],
            "title"             => $title,
            "description"       => $desc,
            "app_date"          => $date,
            "app_time"          => $time,

            "posted_on"         => date("Y-m-d H:i:s"),
            "status"            => $status,
        );
        $result = InsertData($arr, APPOINTMENT_MASTER);

        //----- Send Push Notification
        $sql = "Select * from ".STUDENT_DEVICE;
        $db->query($sql);
        $record = $db->fetch_object();
        $device_arr = array();

        foreach($record as $val){
            $device_arr[] = $val->device_id;
        }

        $url = SITE_URL.'api/getdevicetoken.php';
        $fields = array(
            'device_id' => $device_arr,
        );

        $fields_string = '';
        $fields_string = http_build_query($fields);

        //open connection
        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //execute post
        $result = curl_exec($ch);
        //close connection
        curl_close($ch);

        //ho $result;die;
        $result_decode = json_decode($result);

        if(isset($result_decode->device_token)){
            $param = array(
                "message"   => $title,
                "type"      => "Appoinment",
                "school_id" => $_SESSION["db_name"],
            );
            send_android_notification($result_decode->device_token,$param);
        }

        echo "Yes";
    } else {
        echo $error_text;
    }
}

if (isset($_POST['action']) && $_POST['action'] == "edit") {

    $id             = $_REQUEST['id'];
    $title          = $_REQUEST['txtapptitle'];
    $student_id     = $_REQUEST['student_id'];
    $desc           = $_REQUEST['txtdesc'];
    $date           = $_REQUEST['txtdate'];
    $time           = $_REQUEST['txtapptime'];
    $emp_list       = $_REQUEST['emp_list'];

    $status  = isset($_REQUEST['status']) ? 1 : 0;

    $arr = array(
        "title"           => $title,
        "student_id"      => $student_id,
        "description"     => $desc,
        "app_date"        => $date,
        "app_time"        => $time,

        "status"          => $status,
    );
    $where_arr = array("id" => $id);
    $result = UpdateData($arr,APPOINTMENT_MASTER, $where_arr);

    //----- Send Push Notification
    $sql = "Select * from ".STUDENT_DEVICE;
    $db->query($sql);
    $record = $db->fetch_object();
    $device_arr = array();

    foreach($record as $val){
        $device_arr[] = $val->device_id;
    }

    $url = SITE_URL.'api/getdevicetoken.php';
    $fields = array(
        'device_id' => $device_arr,
    );

    $fields_string = '';
    $fields_string = http_build_query($fields);

    //open connection
    $ch = curl_init();

    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //execute post
    $result = curl_exec($ch);
    //close connection
    curl_close($ch);

    //ho $result;die;
    $result_decode = json_decode($result);

    if(isset($result_decode->device_token)){
        $param = array(
            "message"   => $title,
            "type"      => "Appoinment",
            "school_id" => $_SESSION["db_name"],
        );
        send_android_notification($result_decode->device_token,$param);
    }


    echo "Yes";
}
?>