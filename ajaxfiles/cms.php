<?php
    
    include("../includes/config-frontend.php");
    
    if(isset($_REQUEST['start']))
        $_SESSION['start_record'] = $_REQUEST['start'];
    
    
    if((isset($_REQUEST['action']) && $_REQUEST['action'] == "delete") && (isset($_REQUEST['id']) && $_REQUEST['id'] !="")){
        $cobj = new CMS();
        $cobj->DeleteCMS($_REQUEST['id']);
    }
    
    if(isset($_POST['drpAction']) && $_POST['drpAction'] == "Delete"){
        foreach($_POST['id'] as $val){
            $cobj = new CMS();
            $cobj->DeleteCMS($val);
        }
    }
    
    if(isset($_POST['drpAction']) && $_POST['drpAction'] == "Active" && !isset($_POST['task'])){
        
        $cobj = new CMS();
        foreach($_POST['id'] as $val){
            
            $arr = array(
                            "status" => 1,
                        );
            
            $where_arr = array("id" => $val);
            $result = UpdateData($arr, CMS_MASTER, $where_arr);
        }
    }
    
    if(isset($_POST['drpAction']) && $_POST['drpAction'] == "Inactive" && !isset($_POST['task'])){
        $cobj = new CMS();
        
        foreach($_POST['id'] as $val){
            
            
            $arr = array(
                            "status" => 0,
                        );
            
            $where_arr = array("id" => $val);
            $result = UpdateData($arr, CMS_MASTER, $where_arr);
        }
    }
    
    $where = '';
    
    if(isset($_POST['task']) && $_POST['task'] == "search"){
        if(isset($_POST['tbxTitle']) && $_POST['tbxTitle']!= ""){
            $where .= " And name like '".$_POST['tbxTitle']."%'";
        }
        if(isset($_POST['drpsearch_status']) && $_POST['drpsearch_status'] != ""){
            
            $where .= " And status = '".$_POST['drpsearch_status']."'";
        }
    }
    
    $cobj = new CMS();
    $data = $cobj->fetch_all($where);
    
?>

<table class="table table-bordered table-striped table-condensed flip-content" >   
 <thead>
        <tr>
            <th class="chb_col">
                <input type="checkbox" name="checkall" onclick="javascript:checkAlluncheckAll(frm);">
            </th>
            <th>
                Name
            </th>
              <th>
                Alias
            </th>
             <th>
                Content
            </th>
            <th class="center" width="5%" style="text-align: center;">
                Status
            </th>
            <th width="10%" style="text-align: center;">
                Action
            </th>
        </tr>
    </thead>

    <tbody id="cms_tbody">

        <?php 
        
        if(count($data) == 0){
            
        ?>
            <tr><td colspan="10" align="center">No Result found</td></tr>
        <?php
        }
        else{

            foreach($data as $val)
            { 
                ?>
                <tr>
                    <td class="chb_col">
                        <input type="checkbox" name="id[]" value="<?php echo $val->id; ?>">
                    </td>
                    <td>
                        <?php echo $val->name; ?>
                    </td>
                     <td>
                        <?php echo $val->alias; ?>
                    </td>
                     <td>
                        <?php echo $val->content; ?>
                    </td>
                    <td id="td_status_<?php echo $val->id; ?>" style="text-align: center;">

                                                                    <a href="javascript:;" onclick="ChangeStatus('<?php echo $val->status; ?>', '<?php echo $val->id; ?>')"

                                                                       title='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>'>

                                                                        <img  src='<?php echo IMAGE_URL; ?><?php
                                                                        echo ($val->status == 1 ? "ico-1.gif" : "ico-0.gif");
                                                                        ?>

                                                                              ' alt='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>'

                                                                              title='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>' />

                                                                    </a>

                                                                </td>

                    <td class="content_actions" style="text-align: center;">
                        <a href="javascript:;" onclick="javascript:RemoveRecord('<?php echo $val->id; ?>')" title='Delete'>
                            <img src="<?php echo IMAGE_URL; ?>ico/trashcan_gray.png" alt="" /></a> <a href="cms_edit.php?id=<?php echo $val->id; ?>" class="sepV_a" title="Edit">
                            <img src="<?php echo IMAGE_URL; ?>ico/pencil_gray.png" alt="" />
                        </a>
                    </td>

                </tr>

             <?php 

            }

        }

    ?>
    </tbody>
</table>
<?php include("../pagination.php"); ?>