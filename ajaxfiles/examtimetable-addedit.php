<?php
include("../includes/config-frontend.php");

if (isset($_POST['action']) && $_POST['action'] == "add") {

    $flag    = false;

    $title = $_REQUEST['tbxtitle'];
    $start_date = $_REQUEST['start_date'];
    $end_date = $_REQUEST['end_date'];
    $grade_id = $_REQUEST['grade_id'];
    $medium = $_REQUEST['medium'];

    $status  = isset($_REQUEST['status']) ? 1 : 0;



    if ($flag == false) {

        $other_flag = false;

        $NewImageName = '';
        if (isset($_FILES['file_name']['name']) && $_FILES['file_name']['name'] != "") {
            //echo $_FILES['file_name']['type'];die;

            //if(strstr($_FILES['file_name']['type'],"excel") || strstr($_FILES['file_name']['type'],"csv")){
            //==== Image Uploading
            $RandomNum = time();
            $ImageName = str_replace(' ', '-', strtolower($_FILES['file_name']['name']));
            $ImageType = $_FILES['file_name']['type']; //"image/png", image/jpeg etc.
            $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
            $ImageExt = str_replace('.', '', $ImageExt);
            $ImageName = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
            $NewImageName = $ImageName . '-' . $RandomNum . '.' . $ImageExt;
            move_uploaded_file($_FILES["file_name"]["tmp_name"], EXAM_PATH . $NewImageName);
            //}
//            else {
//                $other_flag = true;
//            }
        }


        if($other_flag == false){
            $arr = array(

                "exam_title" => $title,
                "grade_id" => $grade_id,
                "medium" => $medium,
                "start_date" => $start_date,
                "end_date" => $end_date,
                "file_path"     => $_SESSION["db_name"]."/exam_timetable/",
                "file_name"     => $NewImageName,
                "file_path_import"     => $_SESSION["db_name"]."/exam_timetable/",
                "file_name_import"     => $importImageName,
                "status" => 1,
            );

            $result = ImportData($arr, EXAM_TIMETABLE);





            if (isset($_FILES['file_name_import'])) {
                $ok = true;
                $file = $_FILES['file_name_import']['tmp_name'];


                $handle = fopen($file, "r");
                if ($file == NULL) {
                    $other_flag = true;
                    $ok = false;
                }
                else {
                    $count = 0;
                    while(($filesop = fgetcsv($handle, 1000, ",")) !== false)
                    {
                        $count++;
                        if ($count == 1) { continue; }
                        $title = $filesop[0];
                        $grade_id = $filesop[1];
                        $medium = $filesop[2];
                        $exam_date = $filesop[3];
                        $start_time = $filesop[4];
                        $end_time = $filesop[5];



                        if ($ok) {

                            global $db;

                            $sql = $db->query("
            INSERT INTO ".EXAMTIMETABLE_DATA." SET
            exam_title=$title,
            grade_id=$grade_id,
            medium=$medium ,
            exam_date= $exam_date ,
            start_time= $start_time ,
            end_time= $end_time ,
            et_id='" . $result[0]->lid . "'");
                            echo $sql;die;
                        }
                    }

                    if ($sql) {
                        $other_flag = false;
                    } else {
                        $other_flag = true;
                    }
                }

                $importImageName = '';
                if (isset($_FILES['file_name_import']['name']) && $_FILES['file_name_import']['name'] != "") {
                    //echo $_FILES['file_name']['type'];die;


                    //==== Image Uploading
                    $RandomNum = time();
                    $ImageName = str_replace(' ', '-', strtolower($_FILES['file_name_import']['name']));
                    $ImageType = $_FILES['file_name_import']['type']; //"image/png", image/jpeg etc.
                    $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
                    $ImageExt = str_replace('.', '', $ImageExt);
                    if($ImageExt == 'xls' || $ImageExt == 'csv' || $ImageExt == 'xlsx'){

                        $ImageName = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
                        $importImageName = $ImageName . '-' . $RandomNum . '.' . $ImageExt;
                        move_uploaded_file($_FILES["file_name_import"]["tmp_name"], TIMETABLE_PATH . $importImageName);



                    }
                    else {
                        $other_flag = true;
                    }
                }
            }
            echo $sql;
            echo $grade_id;

            //----- Send Push Notification
            $sql = "Select * from ".STUDENT_DEVICE." AS SD 
                       Inner Join ".STUDENT_MASTER." AS SM ON SM.id = SD.student_id
                           Where SM.status = 1 And SM.grade_id = '".$grade_id."' And SM.section_id = '".$section_id."'";
            $db->query($sql);
            $record = $db->fetch_object();
            $device_arr = array();

            foreach($record as $val){
                $device_arr[] = $val->device_id;
            }

            $url = SITE_URL.'api/getdevicetoken.php';
            $fields = array(
                'device_id' => $device_arr,
            );

            $fields_string = '';
            $fields_string = http_build_query($fields);

            //open connection
            $ch = curl_init();

            curl_setopt($ch,CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_POST, count($fields));
            curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //execute post
            $result = curl_exec($ch);
            //close connection
            curl_close($ch);

            //ho $result;die;
            $result_decode = json_decode($result);

            if(isset($result_decode->device_token)){
                $param = array(
                    "message"   => "New Examtimetable is uploaded",
                    "type"      => "Examtimetable",
                    "school_id" => $_SESSION["db_name"],
                );
                send_android_notification($result_decode->device_token,$param);
            }

            echo "Yes";
        }
        else {
            echo "Invalid File Format. Please upload Excel file only.";
        }


    } else {
        echo $error_text;
    }
}

if (isset($_POST['action']) && $_POST['action'] == "edit") {

    $flag       = false;
    $id = $_REQUEST['id'];
    $title = $_REQUEST['tbxtitle'];
    $start_date = $_REQUEST['start_date'];
    $end_date = $_REQUEST['end_date'];
    $grade_id = $_REQUEST['grade_id'];
    $medium = $_REQUEST['medium'];
    $status = $_REQUEST['status'] ? $_REQUEST['status'] : 1;


    $other_flag = false;

    if ($flag == false) {

        $NewImageName = '';
        if (isset($_FILES['file_name']['name']) && $_FILES['file_name']['name'] != "") {
            //if(strstr($_FILES['file_name']['type'],"excel") || strstr($_FILES['file_name']['type'],"csv")){
            @unlink(EXAM_PATH.$_REQUEST['oimg']);
            //==== Image Uploading
            $RandomNum = time();
            $ImageName = str_replace(' ', '-', strtolower($_FILES['file_name']['name']));
            $ImageType = $_FILES['file_name']['type']; //"image/png", image/jpeg etc.
            $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
            $ImageExt = str_replace('.', '', $ImageExt);
            $ImageName = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
            $NewImageName = $ImageName . '-' . $RandomNum . '.' . $ImageExt;
            move_uploaded_file($_FILES["file_name"]["tmp_name"], EXAM_PATH . $NewImageName);
            //}
        }

        $importImageName = '';
        if (isset($_FILES['file_name_import']['name']) && $_FILES['file_name_import']['name'] != "") {
            //echo $_FILES['file_name']['type'];die;


            //==== Image Uploading
            $RandomNum = time();
            $ImageName = str_replace(' ', '-', strtolower($_FILES['file_name_import']['name']));
            $ImageType = $_FILES['file_name_import']['type']; //"image/png", image/jpeg etc.
            $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
            $ImageExt = str_replace('.', '', $ImageExt);

            if($ImageExt == 'xls' || $ImageExt == 'csv' || $ImageExt == 'xlsx'){

                $ImageName = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
                $importImageName = $ImageName . '-' . $RandomNum . '.' . $ImageExt;
                move_uploaded_file($_FILES["file_name_import"]["tmp_name"], EXAM_PATH . $importImageName);
            }
            else {
                $other_flag = true;
            }
        }


        else {
            $NewImageName = $_REQUEST['oimg'];
            $importImageName = $_REQUEST['oimg'];
        }

        if($other_flag == false){
            $arr = array(
                "exam_title" => $title,
                "grade_id" => $grade_id,
                "medium" => $medium,
                "start_date" => $start_date,
                "end_date" => $end_date,
                "file_path"     => $_SESSION["db_name"]."/exam_timetable/",
                "file_name"     => $NewImageName,
                "file_path_import"     => $_SESSION["db_name"]."/eaxm_timetable/",
                "file_name_import"     => $importImageName,
                "status" => $status,
            );

            $where_arr = array("id" => $id);
            $result = UpdateData($arr, EXAM_TIMETABLE,$where_arr);

            //----- Send Push Notification
            $sql = "Select * from ".STUDENT_DEVICE." AS SD 
                       Inner Join ".STUDENT_MASTER." AS SM ON SM.id = SD.student_id
                           Where SM.status = 1 And SM.grade_id = '".$grade_id."' And SM.section_id = '".$section_id."'";
            $db->query($sql);
            $record = $db->fetch_object();
            $device_arr = array();

            foreach($record as $val){
                $device_arr[] = $val->device_id;
            }

            $url = SITE_URL.'api/getdevicetoken.php';
            $fields = array(
                'device_id' => $device_arr,
            );

            $fields_string = '';
            $fields_string = http_build_query($fields);

            //open connection
            $ch = curl_init();

            curl_setopt($ch,CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_POST, count($fields));
            curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //execute post
            $result = curl_exec($ch);
            //close connection
            curl_close($ch);

            //ho $result;die;
            $result_decode = json_decode($result);

            if(isset($result_decode->device_token)){
                $param = array(
                    "message"   => "New ExamTimeTable is uploaded",
                    "type"      => "ExamTimetable",
                    "school_id" => $_SESSION["db_name"],
                );
                send_android_notification($result_decode->device_token,$param);
            }

            echo "Yes";
        }


    } else {
        echo "Invalid File Format. Please upload Excel file only.";
    }

}
?>