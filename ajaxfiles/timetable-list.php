<?php
include("../includes/config-frontend.php");

$cobj = new TimeTable();
if (isset($_REQUEST['start'])) {
    $_SESSION['start_record'] = $_REQUEST['start'];
}

if ((isset($_REQUEST['action']) && $_REQUEST['action'] == "delete") && (isset($_REQUEST['id']) && $_REQUEST['id'] != "")) {
    $cobj->DeleteTimeTable($_REQUEST['id']);
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Delete") {
    if (isset($_POST['id'])) {
        foreach ($_POST['id'] as $val) {
            $cobj->DeleteTimeTable($val);
        }
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Active" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 1,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr, TIME_TABLE, $where_arr);
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Inactive" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 0,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr, TIME_TABLE, $where_arr);
    }
}

if (isset($_POST['action']) && $_POST['action'] == "changestatus") {
    $status = $_POST['status'];
    $id = $_POST['id'];

    if ($status == 1) {
        $st = 0;
    } else {
        $st = 1;
    }

    $arr = array(
        "status" => $st,
    );

    $where_arr = array("id" => $id);
    $result = UpdateData($arr, TIME_TABLE, $where_arr);
}

$where = '';
if (isset($_POST['task']) && $_POST['task'] == "search") {

    if (isset($_POST['drpsearch_status']) && $_POST['drpsearch_status'] != "") {
        $where .= " And TT.status = '" . $_POST['drpsearch_status'] . "'";
    }

    if (isset($_POST['course']) && $_POST['course'] != "") {
        $where .= " And TT.course_id = '" . $_POST['course'] . "'";
    }

    if (isset($_POST['grade']) && $_POST['grade'] != "") {
        $where .= " And TT.grade_id = '" . $_POST['grade'] . "'";
    }

    if (isset($_POST['section']) && $_POST['section'] != "") {
        $where .= " And TT.section_id = '" . $_POST['section'] . "'";
    }
}

$data = $cobj->fetch_all($where);
?>

<table class="table table-bordered table-striped table-condensed flip-content" >
    <thead>
        <tr>
            <th class="chb_col" width="1%" >
                <input type="checkbox" name="checkall" onclick="javascript:checkAlluncheckAll(frm);">
            </th>
            <th>
                Course Name
            </th>
            <th>
                Class With Grade
            </th>
            <th>
                Day
            </th>
            <th>
              Period Start Time
            </th>
            <th>
              Period End Time
            </th>
            <th>
                Teacher
            </th>
            <th class="center" style="text-align: center;">
                View File
            </th>
            <th class="center" width="5%" style="text-align: center;">
                Status
            </th>
            <th style="text-align: center;">
                Action
            </th>
        </tr>
    </thead>

    <tbody id="content ui-sortable">

        <?php
        if (count($data) == 0) {
            ?>
            <tr><td colspan="8" align="center">No Result found</td></tr>
            <?php
        } else {
            foreach ($data as $val) {
                ?>
                <tr>
                    <td class="chb_col">
                        <input type="checkbox" name="id[]" value="<?php echo $val->id; ?>">
                    </td>
                    <td>
                        <?php echo $val->course_name; ?>
                    </td>
                    <td>
                        <?php echo $val->standard_name . " - " . $val->section_name; ?>
                    </td>
                     <td>
                        <?php echo $val->day; ?>
                    </td>
                     <td>
                        <?php echo $val->time_start; ?>
                    </td>
                     <td>
                        <?php echo $val->time_end; ?>
                    </td>
                     <td>
                        <?php echo $val->emp_id; ?>
                    </td>
                    <td style="text-align: center;">
                        <a href="<?php echo TIMETABLE_URL . $val->file_name; ?>" >
                            View File
                        </a>
                    </td>
                    <td id="td_status_<?php echo $val->id; ?>" style="text-align: center;">

                        <a href="javascript:;" onclick="ChangeStatus('<?php echo $val->status; ?>', '<?php echo $val->id; ?>')"

                           title='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>'>

                            <img  src='<?php echo IMAGE_URL; ?><?php
                        echo ($val->status == 1 ? "ico-1.gif" : "ico-0.gif");
                        ?>

                                  ' alt='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>'

                                  title='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>' />

                        </a>

                    </td>
                    <td class="content_actions" style="text-align: center;">
                        <a href="javascript:;" onclick="javascript:RemoveRecord('<?php echo $val->id; ?>')" title='Delete'>
                            <img src="<?php echo IMAGE_URL; ?>ico/trashcan_gray.png" alt="" /></a> <a href="timetable-edit.php?id=<?php echo $val->id; ?>" class="sepV_a" title="Edit">
                            <img src="<?php echo IMAGE_URL; ?>ico/pencil_gray.png" alt="" />
                        </a>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>

<?php
include("../pagination.php");
?>