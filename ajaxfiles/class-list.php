<?php
include("../includes/config-frontend.php");

$cobj = new ClassMaster();
if (isset($_REQUEST['start'])) {
    $_SESSION['start_record'] = $_REQUEST['start'];
}

if ((isset($_REQUEST['action']) && $_REQUEST['action'] == "delete") && (isset($_REQUEST['id']) && $_REQUEST['id'] != "")) {
    $cobj->DeleteClass($_REQUEST['id']);
    $cobj->DeleteClassDetail($_REQUEST['id']);
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Delete") {
    if (isset($_POST['id'])) {
        foreach ($_POST['id'] as $val) {
            $cobj->DeleteClass($val);
            $cobj->DeleteClassDetail($val);
        }
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Active" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 1,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr, CLASS_MASTER, $where_arr);
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Inactive" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 0,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr, CLASS_MASTER, $where_arr);
    }
}

if (isset($_POST['action']) && $_POST['action'] == "changestatus") {
    $status = $_POST['status'];
    $id = $_POST['id'];

    if ($status == 1) {
        $st = 0;
    } else {
        $st = 1;
    }

    $arr = array(
        "status" => $st,
    );

    $where_arr = array("id" => $id);
    $result = UpdateData($arr, CLASS_MASTER, $where_arr);
}

$where = " And CM.school_id = '" . $_SESSION['institute_id'] . "' ";

if (isset($_POST['task']) && $_POST['task'] == "search") {
    if (isset($_POST['tbxTitle']) && $_POST['tbxTitle'] != "") {
        $where .= " And CM.class_name like '%" . $_POST['tbxTitle'] . "%'";
    }
    if (isset($_POST['drpsearch_status']) && $_POST['drpsearch_status'] != "") {
        $where .= " And CM.status = '" . $_POST['drpsearch_status'] . "'";
    }

    if (isset($_POST['course_id']) && $_POST['course_id'] != "") {
        $where .= " And CM.course_id = '" . $_POST['course_id'] . "'";
    }
}

if (isset($_REQUEST['q_mark']) && $_REQUEST['q_mark'] != "") {
    $q = $_REQUEST['q_mark'];
    $where .= " And CM.class_name like '" . $q . "%'";
}

$data = $cobj->fetch_all($where);
?>

<table class="table table-bordered table-striped table-condensed flip-content" >
    <thead>
        <tr>
            <th class="chb_col" width="1%" >
                <input type="checkbox" name="checkall" onclick="javascript:checkAlluncheckAll(frm);">
            </th>
            <th width="10%">
                Class Name
            </th>
            <th width="20%">
                Course Name
            </th>
            <th width="30%">
                Grade with Section
            </th>
            <th class="center" width="5%" style="text-align: center;">
                Status
            </th>
            <th width="10%" style="text-align: center;">
                Action
            </th>
        </tr>
    </thead>

    <tbody id="content ui-sortable">

        <?php
        if (count($data) == 0) {
            ?>
            <tr><td colspan="8" align="center">No Result found</td></tr>
            <?php
        } else {
            foreach ($data as $val) {

                $sql = "Select * From " . CLASS_DETAIL . " As cd 
                                                            INNER JOIN  " . GRADE_MASTER . " As GM on GM.id = cd.grade_id 
                                                            INNER JOIN  " . SECTION_MASTER . " As SM on SM.id = cd.section_id "
                        . " Where cd.class_id = '" . $val->id . "'";

                $db->query($sql);
                $row = $db->fetch_object();

                $arrs = array();
                $i = 0;
                foreach ($row as $value) {
                    $arrs[$i] = $value->standard_name . " - " . $value->section_name;
                    $i++;
                }
                $c_list = implode(", ", $arrs);
                ?>
                <tr>
                    <td class="chb_col">
                        <input type="checkbox" name="id[]" value="<?php echo $val->id; ?>">
                    </td>
                    <td>
                        <?php echo $val->class_name; ?>
                    </td>
                    <td>
                        <?php echo $val->course_name; ?>
                    </td>
                    <td>
                        <?php echo $c_list; ?>
                    </td>

                    <td id="td_status_<?php echo $val->id; ?>" style="text-align: center;">

                        <a href="javascript:;" onclick="ChangeStatus('<?php echo $val->status; ?>', '<?php echo $val->id; ?>')"

                           title='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>'>

                            <img  src='<?php echo IMAGE_URL; ?><?php
                        echo ($val->status == 1 ? "ico-1.gif" : "ico-0.gif");
                        ?>

                                  ' alt='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>'

                                  title='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>' />

                        </a>

                    </td>
                    <td class="content_actions" style="text-align: center;">
                        <a href="javascript:;" onclick="javascript:RemoveRecord('<?php echo $val->id; ?>')" title='Delete'>
                            <img src="<?php echo IMAGE_URL; ?>ico/trashcan_gray.png" alt="" /></a> <a href="class-edit.php?id=<?php echo $val->id; ?>" class="sepV_a" title="Edit">
                            <img src="<?php echo IMAGE_URL; ?>ico/pencil_gray.png" alt="" />
                        </a>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>

<?php
include("../pagination.php");
?>