<?php
    include("../includes/config-frontend.php");
    
    $cobj = new CMS();
    
    if(isset($_POST['action']) && $_POST['action'] == "add"){
        
        $name               = $_POST['name'];
        $alias              = $_POST['alias'];
        $content            = $_POST['content'];
//        $meta_title         = $_POST['meta_title'];
//        $meta_keyword       = $_POST['meta_keyword'];
//        $meta_description   = $_POST['meta_description'];
        //$status             = $_POST['status'];
        
        if($alias == ""){
            $alias = getSEOURL($name);
        }
        else {
            
            $alias = getSEOURL($alias);
        }
        
        $alias_count = $cobj->getAliasCount($alias);
        if($alias_count == 0){
            $arr = array(
                            "name"              => $name,
                            "alias"             => $alias,
                            "content"           => $content,
//                            "meta_title"        => $meta_title,
//                            "meta_keyword"      => $meta_keyword,
//                            "meta_description"  => $meta_description,
                           // "status"            => $status,
                            //"additional_tag"    => $additional_tag,
                    );
            $result = InsertData($arr, CMS_MASTER);

            echo "Yes";
        }
        else {
            echo "Alias is already exits";
        }
        
    }
    
    if(isset($_POST['action']) && $_POST['action'] == "edit"){
        
        $name               = $_POST['name'];
        $alias              = $_POST['alias'];
        $content            = $_POST['content'];
//        $meta_title         = $_POST['meta_title'];
//        $meta_keyword       = $_POST['meta_keyword'];
//        $meta_description   = $_POST['meta_description'];
        $status             = $_POST['status'];
        $id                 = $_POST['id'];
        
        
        if($alias == ""){
            $alias = getSEOURL($name);
        }
        else {
            
            $alias = getSEOURL($alias);
        }
        
        $where = " And id != '".$id."'";
        $alias_count = $cobj->getAliasCount($alias,$where);
        if($alias_count == 0){
        
            $arr = array(
                            "name"              => $name,
                            "alias"             => $alias,
                            "content"           => $content,
//                            "meta_title"        => $meta_title,
//                            "meta_keyword"      => $meta_keyword,
//                            "meta_description"  => $meta_description,
                            "status"            => $status,
                            //"additional_tag"    => $additional_tag,
                    );

            $where_arr =  array("id" => $id);
            $result = UpdateData($arr, CMS_MASTER,$where_arr);
            echo "Yes";
        }
        else {
            echo "Alias is already exits.";
        }
    }
    
    
?>
