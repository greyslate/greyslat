<?php
include("../includes/config-frontend.php");

$cobj = new Attandance();
if (isset($_REQUEST['start'])) {
    $_SESSION['start_record'] = $_REQUEST['start'];
}

if ((isset($_REQUEST['action']) && $_REQUEST['action'] == "delete") && (isset($_REQUEST['id']) && $_REQUEST['id'] != "")) {
    $cobj->DeleteAttandance($_REQUEST['id']);
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Delete") {
    if (isset($_POST['id'])) {
        foreach ($_POST['id'] as $val) {
            $cobj->DeleteAttandance($val);
        }
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Active" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 1,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr, ATTANDANCE_MASTER, $where_arr);
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Inactive" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 0,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr, ATTANDANCE_MASTER, $where_arr);
    }
}

if (isset($_POST['action']) && $_POST['action'] == "changestatus") {
    $status = $_POST['status'];
    $id = $_POST['id'];

    if ($status == 1) {
        $st = 0;
    } else {
        $st = 1;
    }

    $arr = array(
        "status" => $st,
    );

    $where_arr = array("id" => $id);
    $result = UpdateData($arr, ATTANDANCE_MASTER, $where_arr);
}

//$where = " And school_id = '" . $_SESSION['institute_id'] . "' ";

if (isset($_POST['task']) && $_POST['task'] == "search") {
    if (isset($_POST['tbxTitle']) && $_POST['tbxTitle'] != "") {
        $where .= " And (name like '%" . $_POST['tbxTitle'] . "%') ";
    }
    if (isset($_POST['drpsearch_status']) && $_POST['drpsearch_status'] != "") {
        $where .= " And status = '" . $_POST['drpsearch_status'] . "'";
    }
}
/*if( ) {
    $where .= " ";
    $data = $cobj->fetch_all();
}*/
if($_SESSION['role_id'] == 2 || $_SESSION['role_id'] == 1) {
    $where .= " ";
    $data = $cobj->fetch_all();
}
else{
    $where .= "And SM.p_email = '".$_SESSION['email']."' ";
    $data = $cobj->fetch_attandance_list($where);
}
//$data = $cobj->fetch_all($where);
?>

<table class="table table-bordered table-striped table-condensed flip-content" >
    <thead>
        <tr>
            <th class="chb_col" width="1%" >
                <input type="checkbox" name="checkall" onclick="javascript:checkAlluncheckAll(frm);">
            </th>

            <th width="15%">
                Roll No
            </th>
            <th width="15%">
                Date
            </th>
            <th width="15%">
                Present
            </th>
            <th width="10%" style="text-align: center;">
                Action
            </th>
        </tr>
    </thead>

    <tbody id="content ui-sortable">

        <?php
        if (count($data) == 0) {
            ?>
            <tr><td colspan="8" align="center">No Result found</td></tr>
            <?php
        } else {
            foreach ($data as $val) {
                ?>
                <tr>
                    <td class="chb_col">
                        <input type="checkbox" name="id[]" value="<?php echo $val->id; ?>">
                    </td>

                    <td>
                        <?php echo $val->student_roll_no; ?>
                    </td>
                    <td>
                        <?php echo $val->date; ?>
                    </td>
                    <td>
                        <?php echo $val->present; ?>
                    </td>


                    <td class="content_actions" style="text-align: center;">
                        <a href="javascript:;" onclick="javascript:RemoveRecord('<?php echo $val->id; ?>')" title='Delete'>
                            <img src="<?php echo IMAGE_URL; ?>ico/trashcan_gray.png" alt="" />
                        </a>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>

<?php
include("../pagination.php");
?>