<?php

include("../includes/config-frontend.php");

if (isset($_POST['action']) && $_POST['action'] == "add") {

    $flag = false;
    $student_roll_no = $_REQUEST['txtroll'];
    $date = $_REQUEST['txtdate'];
    $present = $_REQUEST['present'];
    //$school_id = $_REQUEST['school_id'];
   

    $NewImageName = '';
    if (isset($_FILES['file_name']['name']) && $_FILES['file_name']['name'] != "") {
        //==== Image Uploading
        $RandomNum = time();
        $ImageName = str_replace(' ', '-', strtolower($_FILES['file_name']['name']));
        $ImageType = $_FILES['file_name']['type']; //"image/png", image/jpeg etc.
        $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
        $ImageExt = str_replace('.', '', $ImageExt);
        $ImageName = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
        $NewImageName = $ImageName . '-' . $RandomNum . '.' . $ImageExt;
        move_uploaded_file($_FILES["file_name"]["tmp_name"], ATTANDANCE_PATH . $NewImageName);
    }

    if ($flag == false) {

        $arr = array(
            //"school_id" => $_SESSION['institute_id'],
            
            "student_roll_no" => $student_roll_no,
            "date" => $date,
            "present" => $present,
            //"school_id" => $school_id,
         
        );
        $result = InsertData($arr, ATTANDANCE_MASTER);
        echo "Yes";
    } else {
        echo $error_text;
    }
}

if (isset($_POST['action']) && $_POST['action'] == "edit") {

    
    $student_roll_no = $_REQUEST['txtroll'];
    $date = $_REQUEST['txtdate'];
    $present = $_REQUEST['present'];
   // $school_id = $_REQUEST['school_id'];
    
    
    

    $NewImageName = '';
    if (isset($_FILES['file_name']['name']) && $_FILES['file_name']['name'] != "") {

        @unlink(ATTANDANCE_PATH . $_REQUEST['oimg']);
        //==== Image Uploading
        $RandomNum = time();
        $ImageName = str_replace(' ', '-', strtolower($_FILES['file_name']['name']));
        $ImageType = $_FILES['file_name']['type']; //"image/png", image/jpeg etc.
        $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
        $ImageExt = str_replace('.', '', $ImageExt);
        $ImageName = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
        $NewImageName = $ImageName . '-' . $RandomNum . '.' . $ImageExt;
        move_uploaded_file($_FILES["file_name"]["tmp_name"], ATTANDANCE_PATH . $NewImageName);
    }
    else {
        $NewImageName = $_REQUEST['oimg'];
    }

    $arr = array(
        
            "student_roll_no" => $student_roll_no,
            "date" => $date,
            "present" => $present,
            //"school_id" => $school_id,
    );

    $where_arr = array("id" => $id);
    $result = UpdateData($arr, ATTANDANCE_MASTER, $where_arr);
    echo "Yes";
}
?>