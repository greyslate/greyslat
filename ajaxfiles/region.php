<?php
    require_once("../includes/config-frontend.php");
    
    global $db;
    
    $type = strtolower(trim($_POST['action']));
    
    if($type == 'getstate'){
        $country_id = $_REQUEST['id'];
        $state_list = FillCombo($state_obj->fetch_state_list($country_id), "id", "state_name", "");
        $state_combo = '<script>$("select").chosen();$("#state_chzn").css("width","440");</script><select onchange="javascript:getCity(this.value);" name="state" id="state" class="input-text required large" title="Please select State">'.$state_list.'</select>';
        echo $state_combo;
    }
    
    if($type == 'getcity'){
        $state_id = $_REQUEST['id'];
        $city_list = FillCombo($city_obj->fetch_city_list($state_id), "id", "city_name", "");
        $city_combo = '<script>$("select").chosen();$("#city_chzn").css("width","440");</script><select name="city" id="city" class="input-text required large" title="Please select City">'.$city_list.'</select>';
        echo $city_combo;
    }
?>