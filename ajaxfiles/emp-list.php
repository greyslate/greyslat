<?php
include("../includes/config-frontend.php");

$cobj = new Employee();
if (isset($_REQUEST['start'])) {
    $_SESSION['start_record'] = $_REQUEST['start'];
}

if ((isset($_REQUEST['action']) && $_REQUEST['action'] == "delete") && (isset($_REQUEST['id']) && $_REQUEST['id'] != "")) {
    $cobj->DeleteEmployee($_REQUEST['id']);
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Delete") {
    if (isset($_POST['id'])) {
        foreach ($_POST['id'] as $val) {
            $cobj->DeleteEmployee($val);
        }
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Active" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 1,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr, EMP_MASTER, $where_arr);
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Inactive" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 0,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr, EMP_MASTER, $where_arr);
    }
}

if (isset($_POST['action']) && $_POST['action'] == "changestatus") {
    $status = $_POST['status'];
    $id = $_POST['id'];

    if ($status == 1) {
        $st = 0;
    } else {
        $st = 1;
    }

    $arr = array(
        "status" => $st,
    );

    $where_arr = array("id" => $id);
    $result = UpdateData($arr, EMP_MASTER, $where_arr);
}

$where = " ";

if (isset($_POST['task']) && $_POST['task'] == "search") {
    if (isset($_POST['tbxTitle']) && $_POST['tbxTitle'] != "") {
        $where .= " And (fname like '%" . $_POST['tbxTitle'] . "%' OR lname like '%" . $_POST['tbxTitle'] . "%')";
    }
    if (isset($_POST['drpsearch_status']) && $_POST['drpsearch_status'] != "") {
        $where .= " And status = '" . $_POST['drpsearch_status'] . "'";
    }
}

$data = $cobj->fetch_all($where);
?>

<table class="table table-bordered table-striped table-condensed flip-content" >
    <thead>
        <tr>
            <th class="chb_col" width="1" >
                <input type="checkbox" name="checkall" onclick="javascript:checkAlluncheckAll(frm);">
            </th>
             <th width="20%">
                School Name
            </th>
             <th width="20%">
                Email 
            </th>
             <th width="20%">
                Phone No
            </th>
            <th width="20%">
                Address
            </th>
            <th width="20%">
                City
            </th>
            <th width="20%">
                State
            </th>
             <th width="20%">
                Subject
            </th>
             <th width="20%">
                Standard
            </th>
            <th width="20%">
                Profile Picture
            </th>
            <th>
                Created On
            </th>
            <th>
                Updated On
            </th>
            <th class="center" width="5%" style="text-align: center;">
                Status
            </th>
            <th width="10%" style="text-align: center;">
                Action
            </th>
        </tr>
    </thead>

    <tbody id="content ui-sortable">

        <?php
        if (count($data) == 0) {
            ?>
            <tr><td colspan="8" align="center">No Result found</td></tr>
            <?php
        } else {
            foreach ($data as $val) {
                ?>
                <tr>
                    <td>
                        <input type="checkbox" name="id[]" value="<?php echo $val->id; ?>">
                    </td>
                    <td>
                        <?php echo $val->school_name; ?>
                    </td>
                    <td>
                        <?php echo $val->email; ?>
                    </td>
                    <td>
                        <?php echo $val->phone; ?>
                    </td>
                    <td>
                        <?php echo $val->address; ?>
                    </td>
                    <td>
                        <?php echo $val->city; ?>
                    </td>
                    <td>
                        <?php echo $val->state; ?>
                    </td>
                     <td>
                        <?php echo $val->fname . " " . $val->lname; ?>
                    </td>
                    <td>
                        <?php echo $val->subject_name; ?>
                    </td>
                   <td>
                        <?php echo $val->standard_name; ?>
                    </td>
                    <td>
                         <?php
                        if (!empty($val->profile_pic)) {
                            ?>
                            <img src="<?php echo EMP_URL . $val->profile_pic; ?>" width="100" />
                            <?php
                        } else {
                            echo "Picture Not Avaialble";
                        }
                        ?>
                    </td>
                    <td>
                        <?php echo $val->created_on; ?>
                    </td>
                    <td>
                        <?php echo $val->updated_on; ?>
                    </td>

                    <td id="td_status_<?php echo $val->id; ?>" style="text-align: center;">

                        <a href="javascript:;" onclick="ChangeStatus('<?php echo $val->status; ?>', '<?php echo $val->id; ?>')"

                           title='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>'>

                            <img  src='<?php echo IMAGE_URL; ?><?php
                        echo ($val->status == 1 ? "ico-1.gif" : "ico-0.gif");
                        ?>

                                  ' alt='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>'

                                  title='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>' />

                        </a>

                    </td>
                    <td class="content_actions" style="text-align: center;">
                        <a href="javascript:;" onclick="javascript:RemoveRecord('<?php echo $val->id; ?>')" title='Delete'>
                            <img src="<?php echo IMAGE_URL; ?>ico/trashcan_gray.png" alt="" /></a> <a href="emp-edit.php?id=<?php echo $val->id; ?>" class="sepV_a" title="Edit">
                            <img src="<?php echo IMAGE_URL; ?>ico/pencil_gray.png" alt="" />
                        </a>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>
<?php
include("../pagination.php");
?>