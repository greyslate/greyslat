<?php

include("../includes/config-frontend.php");

$gallery_id = $_REQUEST['gid'];

$cobj = new GalleryDetail();
if (isset($_REQUEST['start'])){
    $_SESSION['start_record'] = $_REQUEST['start'];
}

if ((isset($_REQUEST['action']) && $_REQUEST['action'] == "delete") && (isset($_REQUEST['id']) && $_REQUEST['id'] != "")) {
    $ginfo = $cobj->getGalleryDetail($_REQUEST['id']);
    @unlink(GALLERY_PATH.$ginfo->image_name);
    
    $cobj->DeleteGalleryDetail($_REQUEST['id']);
    
}


if ((isset($_REQUEST['action']) && $_REQUEST['action'] == "setcoverpic")) {
    
    $udql = $db->query("update ".GALLERY_DETAIL." SET is_cover_pic = 0 where gallery_id = '".$_REQUEST['gid']."'");
    
    $arr = array(
                    "is_cover_pic"  => 1,
                );
    
    $where_arr = array("id" => $_POST['is_cover_pic']);
    $result = UpdateData($arr, GALLERY_DETAIL, $where_arr);
    
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Delete") {
    if (isset($_POST['id'])) {
        foreach ($_POST['id'] as $val) {
            $ginfo = $cobj->getGalleryDetail($val);
            @unlink(GALLERY_PATH.$ginfo->image_name);
            $cobj->DeleteGalleryDetail($val);
        }
    }
}


$where = " And gallery_id = '".$gallery_id."'";

$data = $cobj->fetch_all($where);

?>

<table cellpadding="0" cellspacing="0" border="0" id="tblsortable" class="displaysort mobile_dt tablesorter" style="top: 0px;">
    <thead>
        <tr>
            <th class="chb_col" width="1" >
                <input type="checkbox" name="checkall" onclick="javascript:checkAlluncheckAll(frm);">
            </th>
            <th>
                Gallery
            </th>
            <th class="center" width="15%" style="text-align: center;">
                Is Cover Pic ?
            </th>
            <th width="10%" style="text-align: center;">
                Action
            </th>
        </tr>
    </thead>

    <tbody id="content ui-sortable">

    <?php 

        if(count($data) == 0){
        ?>
            <tr><td colspan="8" align="center">No Result found</td></tr>
        <?php
        }
        else{
            foreach($data as $val){ 
                
                $checked = '';
                if($val->is_cover_pic == 1){
                    $checked = 'checked="checked"';
                }
            ?>
                <tr>
                    <td class="chb_col">
                        <input type="checkbox" name="id[]" value="<?php echo $val->id; ?>">
                    </td>
                    <td>
                        <img src="<?php echo GALLERY_URL.$val->image_name; ?>" width="100px" />
                    </td>

                    <td id="td_status_<?php echo $val->id; ?>" style="text-align: center;">

                        <input <?php echo $checked;?> type="radio" name="is_cover_pic" onclick="javascript:setCoverPic(this.value);" id="is_cover_pic" value="<?php echo $val->id; ?>" >

                    </td>
                    <td class="content_actions" style="text-align: center;">
                        <a href="javascript:;" onclick="javascript:RemoveRecord('<?php echo $val->id; ?>','<?php echo $gallery_id; ?>')" title='Delete'>
                            <img src="<?php echo IMAGE_URL; ?>ico/trashcan_gray.png" alt="" />
                        </a> 
                    </td>
                </tr>
             <?php 
            }
        }
    ?>
    </tbody>
</table>
<input type="hidden" name="gid" value="<?php echo $_REQUEST['gid']; ?>"  />
<?php 
include("../pagination.php"); 
?>