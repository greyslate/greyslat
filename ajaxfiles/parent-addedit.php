<?php
include("../includes/config-frontend.php");

if (isset($_POST['action']) && $_POST['action'] == "add") {

    $flag = false;

    //----------- Check Institute Name is already exits
    $sql = "Select count(*) As cnt From " . PARENT_MASTER . " Where email = '" . $_REQUEST['txtemail'] . "'";
    $db->query($sql);
    $crow = $db->fetch_object(MYSQL_FETCH_SINGLE);

    $error_text = "<ul>";

    if ($crow->cnt > 0) {
        $flag = true;
        $error_text .= '<li>- Email is already exits!</li>';
    }
    $error_text .= '</ul>';

    $NewImageName = '';

    if (isset($_FILES['profile_pic']['name']) && $_FILES['profile_pic']['name'] != "") {
        //==== Image Uploading
        $RandomNum = time();
        $ImageName = str_replace(' ', '-', strtolower($_FILES['profile_pic']['name']));
        $ImageType = $_FILES['profile_pic']['type']; //"image/png", image/jpeg etc.
        $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
        $ImageExt = str_replace('.', '', $ImageExt);
        $ImageName = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
        $NewImageName = $ImageName . '-' . $RandomNum . '.' . $ImageExt;
        move_uploaded_file($_FILES["profile_pic"]["tmp_name"], PARENT_PATH . $NewImageName);
    }


    if ($flag == false) {
        $parent_code = "GS".$_SESSION['institute_id'];
        $arr = array(
            "fname" => $_POST['txtfname'],
            "lname" => $_POST['txtlname'],
            "email" => $_POST['txtemail'],
            "password" => $_POST['txtpassword'],
            "address" => $_POST['txtaddress'],
            "Mobile_No" => $_POST['txtphone'],
            "city" => $_POST['city'],
            "state" => $_POST['state'],
            "profile_pic" => $NewImageName,
            "created_on" => date("Y-m-d H:i:s"),
            "status" => isset($_REQUEST['status']) ? 1 : 0,
        );
        $result = InsertData($arr, PARENT_MASTER);

        $last_inserted_id = $db->sql_inserted_id();
        if ($last_inserted_id >= 10)
            $parent_code = "GS".$_SESSION['institute_id']."P00".$last_inserted_id;
        else
            $parent_code = "GS".$_SESSION['institute_id']."P000".$last_inserted_id;

        $arr = array(
            "parent_code" => $parent_code
        );

        $where_arr = array("id" => $last_inserted_id);
        $result = UpdateData($arr, PARENT_MASTER, $where_arr);
        echo "Yes";
    } else {
        echo $error_text;
    }
}


if (isset($_POST['action']) && $_POST['action'] == "edit") {

    $id = $_REQUEST['id'];

    $sql = "Select count(*) As cnt From " . PARENT_MASTER . " Where id != '" . $_REQUEST['id'] . "' And email = '" . $_REQUEST['txtemail'] . "'";
    $db->query($sql);
    $crow = $db->fetch_object(MYSQL_FETCH_SINGLE);

    if ($crow->cnt > 0) {
        $flag = true;
        $error_text .= '<li>- Email is already exits!</li>';
    }

    $NewImageName = '';

    if (isset($_FILES['profile_pic']['name']) && $_FILES['profile_pic']['name'] != "") {
        //==== Image Uploading
        $RandomNum = time();
        $ImageName = str_replace(' ', '-', strtolower($_FILES['profile_pic']['name']));
        $ImageType = $_FILES['profile_pic']['type']; //"image/png", image/jpeg etc.
        $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
        $ImageExt = str_replace('.', '', $ImageExt);
        $ImageName = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
        $NewImageName = $ImageName . '-' . $RandomNum . '.' . $ImageExt;
        move_uploaded_file($_FILES["profile_pic"]["tmp_name"], PARENT_PATH . $NewImageName);
    }

    if ($crow->cnt == 0) {

        $arr = array(
            "fname" => $_POST['txtfname'],
            "lname" => $_POST['txtlname'],
            "email" => $_POST['txtemail'],
            "address" => $_POST['txtaddress'],
            "Mobile_No" => $_POST['txtphone'],
            "city" => $_POST['city'],
            "state" => $_POST['state'],
            "profile_pic" => $NewImageName,
            "status" => isset($_REQUEST['status']) ? 1 : 0,
        );

        if ($_REQUEST['txtpassword'] != "") {
            $arr["password"] = $_REQUEST['txtpassword'];
        }

        $where_arr = array("id" => $id);
        $result = UpdateData($arr, PARENT_MASTER, $where_arr);
        echo "Yes";
    } else {
        echo "Parent Name is already exits...!";
    }
}
?>