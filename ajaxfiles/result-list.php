<?php
include("../includes/config-frontend.php");

$cobj = new Result();
if (isset($_REQUEST['start'])) {
    $_SESSION['start_record'] = $_REQUEST['start'];
}

if ((isset($_REQUEST['action']) && $_REQUEST['action'] == "delete") && (isset($_REQUEST['id']) && $_REQUEST['id'] != "")) {
    $cobj->DeleteResult($_REQUEST['id']);
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Delete") {
    if (isset($_POST['id'])) {
        foreach ($_POST['id'] as $val) {
            $cobj->DeleteResult($val);
        }
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Active" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 1,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr, RESULT_MASTER, $where_arr);
    }
}

if (isset($_POST['drpAction']) && $_POST['drpAction'] == "Inactive" && !isset($_POST['task'])) {
    foreach ($_POST['id'] as $val) {
        $arr = array(
            "status" => 0,
        );
        $where_arr = array("id" => $val);
        $result = UpdateData($arr, RESULT_MASTER, $where_arr);
    }
}

if (isset($_POST['action']) && $_POST['action'] == "changestatus") {
    $status = $_POST['status'];
    $id = $_POST['id'];

    if ($status == 1) {
        $st = 0;
    } else {
        $st = 1;
    }

    $arr = array(
        "status" => $st,
    );

    $where_arr = array("id" => $id);
    $result = UpdateData($arr, RESULT_MASTER, $where_arr);
}

//$where = '';
if (isset($_POST['task']) && $_POST['task'] == "search") {
    if (isset($_POST['tbxTitle']) && $_POST['tbxTitle'] != "") {
        $where .= " And exam_title like '%" . $_POST['tbxTitle'] . "%'";
    }

   if(isset($_POST['grade_id']) && $_POST['grade_id'] != " " ){
        $where .= " And standard_id = '".$_POST['grade_id']."'";
   }
    if (isset($_POST['course_id']) && $_POST['course_id'] != "") {
        $where .= " And TT.course_id = '" . $_POST['course_id'] . "'";
    }
    if (isset($_POST['section_id']) && $_POST['section_id'] != "") {
        $where .= " And TT.section_id = '" . $_POST['section_id'] . "'";
    }
    if (isset($_POST['student_name']) && $_POST['student_name'] != "") {
        $where .= " And SM.fname = '" . $_POST['student_name'] . "'";
    }

    if (isset($_POST['student_rollno']) && $_POST['student_rollno'] != "") {
        $where .= " And SM.roll_no = '" . $_POST['student_rollno'] . "'";
    }
}

if($_SESSION['role_id'] == 1 || $_SESSION['role_id'] == 2 ) {
    $where .= "  ";
    $data = $cobj->fetch_all($where);
}
else{
    $where .= "And SM.p_email = '".$_SESSION['email']."'";
    $data = $cobj->fetch_all($where);
}
?>
<table class="table table-bordered table-striped table-condensed flip-content" >
    <thead>
        <tr>
            <th class="chb_col" width="1%" >
                <input type="checkbox" name="checkall" onclick="javascript:checkAlluncheckAll(frm);">
            </th>
            <th width="9%">
                Student Id
            </th>
            <th width="15%" >
                Student Name
            </th>
            <th width="15%">
                Exam Title
            </th>
            <th width="15%">
                Course Name
            </th>

            <!-- <th>
                 Date
             </th>-->

            <th width="11%">
                Standard
            </th>
            <th>
                Total Marks
            </th>
            <th>
                Passing Marks
            </th>
            <th>
                Marks Obtained
            </th>



            <th width="10%" style="text-align: center;">
                Action
            </th>
        </tr>
    </thead>

    <tbody id="content ui-sortable">

        <?php
        if (count($data) == 0) {
            ?>
            <tr><td colspan="8" align="center">No Result found</td></tr>
            <?php
        } else {
            foreach ($data as $val) {
                ?>
                <tr>
                    <td class="chb_col">
                        <input type="checkbox" name="id[]" value="<?php echo $val->id; ?>">
                    </td>
                    <td>
                        <?php echo $val->roll_no; ?>
                    </td>
                    <td>
                        <?php echo $val->fname." ".$val->lname; ?>
                    </td>

                    <td>
                        <?php echo $val->exam_title; ?>
                    </td>

                    <td>
                        <?php echo $val->course_name; ?>
                    </td>
                    <!--<td>
                        <?php /*echo $val->exam_date; */?>
                    </td>-->

                    <td>
                        <?php echo $val->standard_name."-".$val->section_name; ?>
                    </td>
                    <td>
                        <?php echo $val->total_marks; ?>
                    </td>
                    <td>
                        <?php echo $val->passing_marks; ?>
                    </td>
                    <td>
                        <?php echo $val->obt_marks; ?>
                    </td>
                    <td class="content_actions" style="text-align: center;">
                        <a href="javascript:;" onclick="javascript:RemoveRecord('<?php echo $val->id; ?>')" title='Delete'>
                            <img src="<?php echo IMAGE_URL; ?>ico/trashcan_gray.png" alt="" /></a> <a href="result-edit.php?id=<?php echo $val->id; ?>" class="sepV_a" title="Edit">
                            <img src="<?php echo IMAGE_URL; ?>ico/pencil_gray.png" alt="" />
                        </a>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>
<?php
include("../pagination.php");
?>