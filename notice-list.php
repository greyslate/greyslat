<?php
include("header.php");

$cobj = new Notice();
$data = $cobj->fetch_all();
?>
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
             <div class="row">
                <div class="col-md-12">
                    <div class="portlet box red ">
                        <div class="portlet-title">
                            <div class="caption">
                                Search
                            </div>
                        </div>
                        <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                            <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmSearch" id="frmSearch">
                                <div class="form-body">
                                   <div class="form-group">
                                        <label class="control-label col-md-3">Title<span class="required" aria-required="true">
                                             * </span></label>
                                        <div class="col-md-9">
                                            <input type="text" id="tbxTitle" name="tbxTitle" placeholder="Name" class="form-control required"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Status<span class="required" aria-required="true">
                                             * </span></label>
                                        <div class="col-md-9">
                                            <select id="drpsearch_status" class="form-control required" name="drpsearch_status" >
                                                <option value="">---Select---</option>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                               <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn green" name="btnSearch" id="btnSearch" type="submit" onclick="return Search_Data();"><i class="fa fa-check"></i> Search</button>
                                            <button type="button" type="reset" id="btnReset" class="btn default">Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>       
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Manage Notice <small>view notice</small>
                    </h3>
                    <div class="row">
                        <form id="frmtable" name="frm" method="post"  enctype="multipart/form-data" >
                            <div class="col-md-12">
                                <div class="right" style="float:right;">
                                    <a href="notice-add.php" class="btn btn-primary">
                                        Add New <i class="fa fa-plus"></i>
                                    </a>
                                    <a href="notice-export.php" class="btn red">Export Data</a>
                                </div>
                                <br><br><br>

                                <!-- BEGIN BORDERED TABLE PORTLET-->
                                <div class="portlet box red">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cogs"></i>Manage Notice
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div id="page_listing">
                                            <table class="table table-bordered table-striped table-condensed flip-content" >
                                                <thead>
                                                    <tr>
                                                        <th class="chb_col" width="1%" >
                                                            <input type="checkbox" name="checkall" onclick="javascript:checkAlluncheckAll(frm);">
                                                        </th>
                                                        <th width="20%">
                                                            Notice Title
                                                        </th>
                                                        <th width="20%">
                                                            Notice Date
                                                        </th>
                                                         <th width="20%">
                                                            Notice Location
                                                        </th>
                                                        <th width="20%">
                                                            Created On
                                                        </th>
                                                        <th width="20%">
                                                            Last Updated On
                                                        </th>

                                                        <th class="center" style="text-align: center;">
                                                            Status
                                                        </th>
                                                        <th width="10%" style="text-align: center;">
                                                            Action
                                                        </th>
                                                    </tr>
                                                </thead>

                                                <tbody id="content ui-sortable">

                                                    <?php
                                                    if (count($data) == 0) {
                                                        ?>
                                                        <tr><td colspan="8" align="center">No Result found</td></tr>
                                                        <?php
                                                    } else {
                                                        foreach ($data as $val) {
                                                            ?>
                                                            <tr>
                                                                <td class="chb_col">
                                                                    <input type="checkbox" name="id[]" value="<?php echo $val->id; ?>">
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->notice_title; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->notice_date; ?>
                                                                </td>
                                                                 <td>
                                                                    <?php echo $val->notice_location; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->created_on; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->updated_on; ?>
                                                                </td>

                                                                <td id="td_status_<?php echo $val->id; ?>" style="text-align: center;">

                                                                    <a href="javascript:;" onclick="ChangeStatus('<?php echo $val->status; ?>', '<?php echo $val->id; ?>')"

                                                                       title='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>'>

                                                                        <img  src='<?php echo IMAGE_URL; ?><?php
                                                                        echo ($val->status == 1 ? "ico-1.gif" : "ico-0.gif");
                                                                        ?>

                                                                              ' alt='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>'

                                                                              title='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>' />

                                                                    </a>

                                                                </td>
                                                                <td class="content_actions" style="text-align: center;">
                                                                    <a href="javascript:;" onclick="javascript:RemoveRecord('<?php echo $val->id; ?>')" title='Delete'>
                                                                        <img src="<?php echo IMAGE_URL; ?>ico/trashcan_gray.png" alt="" /></a> <a href="notice-edit.php?id=<?php echo $val->id; ?>" class="sepV_a" title="Edit">
                                                                        <img src="<?php echo IMAGE_URL; ?>ico/pencil_gray.png" alt="" />
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                            <?php include("pagination.php"); ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- END BORDERED TABLE PORTLET-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>

<?php
include("footer.php");
?>
<script src="<?php echo JS_URL; ?>script/notice.js" type="text/javascript" charset="utf-8"></script>