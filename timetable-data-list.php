<?php
include("header.php");

if (!isset($_GET['id'])) {
    header("location: timetable-list.php");
    exit(0);
}
$id = $_GET['id'];
//$val = $timetabledata_obj->getTimeTableData($tt_id);

$cobj = new TimeTableData();
$where = "And tt_id = '".$id."'  ";
$data = $cobj->fetch_all($where);
$grade_list = FillCombo($grade_obj->fetch_grade_list(), "id", "standard_name", "");
$course_list = FillCombo($course_obj->fetch_course_list(), "id", "course_name", "");
$section_list = FillCombo($section_obj->fetch_section_list(), "id", "section_name", "");
$emp_list = FillCombo($emp_obj->fetch_Employee_list(),"id", "fname", "");
?>
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
               <!-- <div class="col-md-12">
                    <div class="portlet box red ">
                        <div class="portlet-title">
                            <div class="caption">
                                Search
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <!--<form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmSearch" id="frmSearch">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Title<span class="required" aria-required="true">
                                        * </span></label>
                                        <div class="col-md-9">
                                            <input type="text" id="tbxTitle" name="tbxTitle" placeholder="Name" class="form-control required"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Grade/Standard <span class="required" aria-required="true">* </span></label>
                                        <div class="col-md-9">
                                            <select name="grade_id" id="grade_id" class="form-control required">
                                                 /*/*echo $grade_list; */*/?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Medium<span class="required" aria-required="true">
                                        * </span></label>
                                        <div class="col-md-9">
                                            <select id="medium" class="form-control required" name="medium" >
                                                <option value="">----SELECT----</option>
                                                <option value="English">English</option>
                                                <option value="Gujarati">Gujarati</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Date <span class="required" aria-required="true">* </span></label>
                                        <div class="col-md-9">
                                            <input id="start_date" name="start_date" readonly="" class="form-control required date-picker" placeholder="YYYY-MM-DD">
                                            <input id="end_date" name="end_date" readonly="" class="form-control required date-picker" placeholder="YYYY-MM-DD">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Status<span class="required" aria-required="true">
                                        * </span></label>
                                        <div class="col-md-9">
                                            <select id="drpsearch_status" class="form-control required" name="drpsearch_status" >
                                                <option value="">---Select---</option>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn green" name="btnSearch" id="btnSearch" type="submit" onclick="return Search_Data();"><i class="fa fa-check"></i> Search</button>
                                            <button type="button" type="reset" id="btnReset" class="btn default">Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN PAGE HEADER-->
                        <h3 class="page-title">
                            View Exam Timetable
                        </h3>
                        <div class="row">
                            <form id="frmtable" name="frm" method="post"  enctype="multipart/form-data" >
                                <div class="col-md-12">

                                    <br><br>

                                    <!-- BEGIN BORDERED TABLE PORTLET-->
                                    <div class="portlet box red">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-cogs"></i>Manage Exam Timetable
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div id="page_listing">
                                                <table class="table table-bordered table-striped table-condensed flip-content" >
                                                    <thead>
                                                    <tr>
                                                        <th class="chb_col" width="1%" >
                                                            <input type="checkbox" name="checkall" onclick="javascript:checkAlluncheckAll(frm);">
                                                        </th>
                                                        <th width="20%">
                                                            Day
                                                        </th>
                                                        <th>
                                                            Start Time
                                                        </th>

                                                        <th>
                                                            End Time
                                                        </th>

                                                        <th>
                                                            Employee Name
                                                        </th>
                                                        <th>
                                                            Course Name
                                                        </th>
                                                        <th>
                                                            Medium
                                                        </th>
                                                        <th>
                                                            Standard
                                                        </th>
                                                        <th>
                                                            Section
                                                        </th>

                                                    </tr>
                                                    </thead>

                                                    <tbody id="content ui-sortable">

                                                    <?php
                                                    if (count($data) == 0) {
                                                        ?>
                                                        <tr><td colspan="8" align="center">No Result found</td></tr>
                                                        <?php
                                                    } else {
                                                        foreach ($data as $val) {
                                                            ?>
                                                            <tr>
                                                                <td class="chb_col">
                                                                    <input type="checkbox" name="id[]" value="<?php echo $val->id; ?>">
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->day; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->time_start; ?>
                                                                </td>

                                                                <td>
                                                                    <?php echo $val->time_end; ?>
                                                                </td>

                                                                <td>
                                                                    <?php echo $val->fname; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->course_name; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->medium; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->standard_name; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->section_name; ?>
                                                                </td>



                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                                <?php include("pagination.php"); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END BORDERED TABLE PORTLET-->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
    </div>

    <?php
    include("footer.php");
    ?>
    <script src="<?php echo JS_URL; ?>script/examtimetable.js" type="text/javascript" charset="utf-8"></script>