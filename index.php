<?php
include("header.php");
?>
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Dashboard <small>Quick Overview</small>
                    </h3>
                    <div class="row">
                        
                        <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-coffee"></i>Bordered Table
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-bordered table-hover">
                                        <tbody>
                                            <tr>
                                                <td align="left" width="15%">
                                                    Institute Code
                                                </td>
                                                <td align="left">
                                                    <?php
                                                    echo "GS-" . $_SESSION['institute_id'];
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="15%">
                                                    Institute Name
                                                </td>
                                                <td align="left">
                                                    <?php
                                                    echo $_SESSION['institute_name'];
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Address
                                                </td>
                                                <td align="left">
                                                    <?php
                                                    echo $_SESSION['institute_address'];
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Country
                                                </td>
                                                <td align="left">
                                                   <?php
                                                    $country_row = $country_obj->getCountry($_SESSION['institute_country']);
                                                    $country_name = $_SESSION['institute_country'];
                                                    echo $country_name;
                                                    ?>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    State
                                                </td>
                                                <td align="left">
                                                    <?php 
                                                    $state_row = $state_obj->getState($_SESSION['institute_state']);
                                                    $state_name = $_SESSION['institute_state'];
                                                    echo $state_name;

                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    City
                                                </td>
                                                <td align="left">
                                                    <?php
                                                    $city_row = $city_obj->getCity($_SESSION['institute_city']);
                                                    $city_name = $_SESSION['institute_city'];
                                                    echo $city_name;
                                                    ?>
                                                </td>
                                            </tr>
                                           <tr>
                                                <td align="left">
                                                    Postal Code
                                                </td>
                                                <td align="left">
                                                    <?php
                                                    echo $_SESSION['institute_postalcode'];
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Mobile No
                                                </td>
                                                <td align="left">
                                                    <?php
                                                    echo $_SESSION['institute_phone'];
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Type
                                                </td>
                                                <td align="left">
                                                    <?php
                                                    echo $_SESSION['institute_type'];
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                   Institute Email
                                                </td>
                                                <td align="left">
                                                    <?php
                                                    echo $_SESSION['institute_email'];
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Login Name
                                                </td>
                                                <td align="left">
                                                    <?php
                                                    echo $_SESSION['login_name'];
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Reg. On
                                                </td>
                                                <td align="left">
                                                    <?php
                                                    echo $_SESSION['institute_reg'];
                                                    ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>

</div>

<?php
include("footer.php");
?>    

