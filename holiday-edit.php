<?php
include("header.php");
if (!isset($_GET['id'])) {
    header("location: holiday-list.php");
    exit(0);
}
$id = $_GET['id'];
$val = $holiday_obj->getHoliday($id);
?>
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Holiday List <small> upload holiday list file</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo SITE_URL; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="holiday-list.php">Holiday List</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="portlet box red ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Upload Holiday List
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmedit" id="frmedit">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                You have some form errors. Please check below.
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Year</label>
                                                <div class="col-md-9">
                                                    <select id="drpsearch_year" class="form-control required" name="drpsearch_year" >
                                                        <option   value="">---Select---</option>
                                                        <option <?php if($val->year == "2016-17") { echo "selected"; } ?>  value="2016-17">2016-17</option>
                                                        <option <?php if($val->year == "2017-18") { echo "selected"; } ?>  value="2017-18">2017-18</option>
                                                        <option <?php if($val->year == "2018-19") { echo "selected"; } ?>  value="2018-19">2018-19</option>
                                                        <option <?php if($val->year == "2019-20") { echo "selected"; } ?>  value="2019-20">2019-20</option>
                                                        <option <?php if($val->year == "2020-21") { echo "selected"; } ?>  value="2020-21">2020-21</option>
                                                        <option <?php if($val->year == "2021-22") { echo "selected"; } ?>  value="2021-22">2021-22</option>
                                                        <option <?php if($val->year == "2022-23") { echo "selected"; } ?>  value="2022-23">2022-23</option>
                                                        <option <?php if($val->year == "2023-24") { echo "selected"; } ?>  value="2023-24">2023-24</option>
                                                        <option <?php if($val->year == "2024-25") { echo "selected"; } ?>  value="2024-25">2024-25</option>
                                                        <option <?php if($val->year == "2025-26") { echo "selected"; } ?>  value="2025-26">2025-26</option>
                                                        <option <?php if($val->year == "2026-27") { echo "selected"; } ?>  value="2026-27">2026-27</option>
                                                        <option <?php if($val->year == "2027-28") { echo "selected"; } ?>  value="2027-28">2027-28</option>
                                                        <option <?php if($val->year == "2028-29") { echo "selected"; } ?>  value="2028-29">2028-29</option>
                                                        <option <?php if($val->year == "2029-30") { echo "selected"; } ?>  value="2029-30">2029-30</option>
                                                        <option <?php if($val->year == "2030-31") { echo "selected"; } ?>  value="2030-31">2030-31</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Upload File</label>
                                                <div class="col-md-9">
                                                    <input type="file" id="file_name" name="file_name" class="form-control"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"></label>
                                                <div class="col-md-9">


                                                        <a href="<?php echo HOLIDAY_URL . $val->file_name; ?>" target="_blank">View File</a>


                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Last Updated On</label>
                                                <div class="col-md-9">
                                                    <?php
                                                    echo $val->updated_on;
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input type="hidden" name="action" value="edit">
                                                    <input type="hidden" name="id" value="<?php echo $val->id; ?>" />
                                                    <input type="hidden" name="oimg" value="<?php echo $val->file_name; ?>" />
                                                    <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>
<?php
include("footer.php");
?>
<script src="<?php echo JS_URL; ?>script/holiday.js" type="text/javascript" charset="utf-8"></script>
