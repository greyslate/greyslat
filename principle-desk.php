<?php
include("header.php");
$val = $principle_obj->getPrincipleDesk(1);
?>

<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Principle Desk <small> update principle information</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo SITE_URL; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Principle Desk</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="portlet box red ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Principle Desk
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmedit" id="frmedit">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                You have some form errors. Please check below.
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Name <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input value="<?php echo $val->principle_name; ?>" type="text" id="txtname" name="txtname" placeholder="Principle Name" class="form-control required"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Email <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input value="<?php echo $val->email_id; ?>" type="text" id="txtemail" name="txtemail" placeholder="Email" class="form-control required"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Phone No <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input value="<?php echo $val->phone_no; ?>" type="text" id="txtphone" name="txtphone" placeholder="Phone No" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Upload File <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input type="file" id="file_name" name="file_name" class="form-control"/>
                                                </div>
                                            </div>
                                            <?php
                                            if (file_exists(PRINCIPLE_DESK_PATH . $val->file_name) && !empty($val->file_name)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"></label>
                                                    <div class="col-md-9">

                                                        <img width="100" src="<?php echo PRINCIPLE_DESK_URL . $val->file_name; ?>" />

                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Description <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <textarea id="txtdesc" name="txtdesc" placeholder="Description" class="form-control required"><?php echo $val->description; ?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Last Updated On</label>
                                                <div class="col-md-9">
                                                    <?php
                                                    echo $val->updated_on;
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input type="hidden" name="action" value="edit">
                                                    <input type="hidden" name="id" value="1" />
                                                    <input type="hidden" name="oimg" value="<?php echo $val->file_name; ?>" /> 
                                                    <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>

<?php
include("footer.php");
?>
<script src="<?php echo JS_URL; ?>script/principle_desk.js" type="text/javascript" charset="utf-8"></script>
