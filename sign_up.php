<?php
include("includes/config.php");

    if (isset($_POST["reg"]) && $_POST['reg'] == 'Parent') {

        include_once("ajaxfiles/parent-addedit.php");
    }
    else {
        include_once("ajaxfiles/emp-addedit.php");
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Greyslate - Admin Panel</title>
    <link rel="stylesheet" href="<?php echo CSS_URL; ?>loadingwindow.css" />
    <script type="text/javascript">
        var base_url = "<?php echo SITE_URL; ?>";
    </script>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="<?php echo THEME_URL; ?>global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo THEME_URL; ?>global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo THEME_URL; ?>global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo THEME_URL; ?>global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?php echo THEME_URL; ?>admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="<?php echo THEME_URL; ?>global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="<?php echo THEME_URL; ?>global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo THEME_URL; ?>admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo THEME_URL; ?>admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="<?php echo THEME_URL; ?>admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>

</head>
<!-- BEGIN FORM-->
<div class="page-container">

        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title" align="center">
                        Registration
                    </h3>

                    <div class="col-md-12">
                        <div class="portlet box red ">
                            <div class="portlet-title">
                                <div class="caption" >
                                            Registration Form
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmadd" id="frmadd">

                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                You have some form errors. Please check below.
                                            </div>

                                            <div  align="center">

                                               <label>  Register As:</label>
                                                <input type="radio" name="reg" id="2"  value="Teacher" checked />Teacher
                                                <input type="radio" name="reg" id="3" value="Parent" />Parent
                                               </div>


                                           <!-- <div class="form-group">

                                                <label class="control-label col-md-3">School Name <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">

                                                    <input type="text" id="txtschoolname" name="txtschoolname" placeholder="School Name" class="form-control required"/>
                                                </div>
                                            </div>-->

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Email <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="email" id="txtemail" name="txtemail" placeholder="abc@example.com" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Password <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="password" id="txtpassword" name="txtpassword" placeholder="Password" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Re-type Password <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="password" id="txtcpassword" name="txtcpassword" placeholder="Re-type Password" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Phone No  <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtphone" name="txtphone" placeholder="Enter 10 digit" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Address <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <textarea name="txtaddress" id="txtaddress" class="form-control required" placeholder="Address"></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">City <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="city" name="city" placeholder="city" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">State<span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="state" name="state" placeholder="state" class="form-control required"/>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3">First Name <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtfname" name="txtfname" placeholder="First Name" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Last Name <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtlname" name="txtlname" placeholder="Last Name" class="form-control required"/>
                                                </div>
                                            </div>

                                           <!-- <div class="form-group">
                                                <label class="control-label col-md-3">Subject<span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtsubjectname" name="txtsubjectname" placeholder="Division" class="form-control"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Standard<span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtstandardname" name="txtstandardname" placeholder="Standard" class="form-control"/>
                                                </div>
                                            </div>-->

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Profile Pic<span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="file" id="profile_pic" name="profile_pic" class="form-control required" placeholder="Upload only png jpeg file">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input type="hidden" name="action" value="add">
                                                    <button type="submit" class="btn green"><i class="fa fa-check"></i>Submit</button>
                                                    <button type="button" onclick="location.href='login.php'" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
            <!-- END PAGE CONTENT-->

<script src="<?php echo THEME_URL; ?>global/plugins/respond.min.js"></script>
<script src="<?php echo THEME_URL; ?>global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo THEME_URL; ?>global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo THEME_URL; ?>global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?php echo THEME_URL; ?>global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo THEME_URL; ?>global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo THEME_URL; ?>global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo THEME_URL; ?>global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo THEME_URL; ?>global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo THEME_URL; ?>global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo THEME_URL; ?>admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo THEME_URL; ?>admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="<?php echo THEME_URL; ?>admin/pages/scripts/login.js" type="text/javascript"></script>
<script src="<?php echo JS_URL; ?>jquery-base64-master/jquery.base64.js" type="text/javascript"></script>
<script src="<?php echo JS_URL; ?>jquery-base64-master/jquery.base64.min.js" type="text/javascript"></script>
<script src="<?php echo JS_URL; ?>loading.js" type="text/javascript"></script>
<script src="<?php echo JS_URL; ?>script/login.js" type="text/javascript"></script>

<!-- END PAGE LEVEL SCRIPTS -->


<?php
include("footer.php");
header("Location: login.php");
?>

<script src="<?php echo JS_URL; ?>script/sign_up.js" type="text/javascript"></script>
</html>