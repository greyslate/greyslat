<?php
class ClassMaster {
    function fetch_all($where = ''){
        global $db;
        $sql = "SELECT count(*) as cnt FROM class_master AS CM
                    Inner join course_master AS CCM on CCM.id = CM.course_id   
                        Where 1=1 ".$where;
        $db->query($sql);
        $db->next_record();
        $this->total_record = $db->f("cnt");
        $db->free();
        if ($_SESSION['start_record'] >= $this->total_record || $_SESSION['page_size'] >= $this->total_record)
            $_SESSION['start_record'] = 0;
        $sql = "Select *,CM.status As status,CM.id As id From ".CLASS_MASTER." As CM
                    Inner join course_master AS CCM on CCM.id = CM.course_id   
                    Where 1=1 ".$where." Order By CM.id desc 
                        LIMIT " . $_SESSION['start_record'] . ", " . $_SESSION['page_size'];
        $db->query($sql);
        return $db->fetch_object();
    }

    function DeleteClass($id){
        global $db;
        $sql = "Delete From ".CLASS_MASTER." Where id = '".$id."'";
        $db->query($sql);
    }
    
    function DeleteClassDetail($id){
        global $db;
        $sql = "Delete From ".CLASS_DETAIL." Where class_id = '".$id."'";
        $db->query($sql);
    }
    
    function getClass($id){
        global $db;
        $sql = "Select * From ".CLASS_MASTER." Where school_id = '".$_SESSION['institute_id']."' And id = '".$id."' ";
        $db->query($sql);
        return $db->fetch_object(MYSQL_FETCH_SINGLE);
    }
    
    function getClassDetail($class_id){
        global $db;
        $sql = "Select * From ".CLASS_DETAIL." Where class_id = '".$class_id."' ";
        $db->query($sql);
        return $db->fetch_object();
    }
    
    function fetch_grade_list($where = ''){
        global $db;
        $sql = "Select * From ".CLASS_MASTER." 
                    Where school_id = '".$_SESSION['institute_id']."' And status = 1 ".$where." Order By id desc ";
        $db->query($sql);
        return $db->fetch_object();
    }
}
?>