<?php
class Assignment {
    function fetch_all($where = ''){
        global $db;
        $sql = "SELECT count(*) as cnt FROM ".ASSIGNMENT_MASTER."  AS ASM
                    Inner join ".COURSE_MASTER." AS CM on CM.id = ASM.course_id   
                        Inner join ".STANDARD_MASTER." AS GM on GM.id = ASM.grade_id
                            Inner join ".SECTION_MASTER." AS SM on SM.id = ASM.section_id
                                Inner join ".EMP_MASTER." AS EM on EM.id = ASM.emp_id
                                    Where 1=1 ".$where;
        $db->query($sql);
        $db->next_record();
        $this->total_record = $db->f("cnt");
        $db->free();
        if ($_SESSION['start_record'] >= $this->total_record || $_SESSION['page_size'] >= $this->total_record)
            $_SESSION['start_record'] = 0;
        $sql = "Select *,ASM.status As status,ASM.id As id From ".ASSIGNMENT_MASTER." As ASM
                    Inner join ".COURSE_MASTER." AS CM on CM.id = ASM.course_id   
                        Inner join ".STANDARD_MASTER." AS GM on GM.id = ASM.grade_id
                         Inner join ".SECTION_MASTER." AS SM on SM.id = ASM.section_id
                            Inner join ".EMP_MASTER." AS EM on EM.id = ASM.emp_id
                                Where 1=1 ".$where." Order By ASM.id desc 
                                    LIMIT " . $_SESSION['start_record'] . ", " . $_SESSION['page_size'];
        $db->query($sql);
        return $db->fetch_object();
    }

    function DeleteAssignment($id){
        global $db;
        $sql = "Delete From ".ASSIGNMENT_MASTER." Where id = '".$id."'";
        $db->query($sql);
    }

    function getAssignment($id){
        global $db;
        $sql = "Select * From ".ASSIGNMENT_MASTER." Where id = '".$id."' ";
        $db->query($sql);
        return $db->fetch_object(MYSQL_FETCH_SINGLE);
    }

    function fetch_assignment_list($where = ''){
        global $db;
        /*$sql = "SELECT *  FROM ".ASSIGNMENT_MASTER."  AS ASM
                    Inner join ".COURSE_MASTER." AS CM on CM.id = ASM.course_id   
                        Inner join ".STANDARD_MASTER." AS GM on GM.id = ASM.grade_id
                            Inner join ".SECTION_MASTER." AS SM on SM.id = ASM.section_id
                                Inner join ".EMP_MASTER." AS EM on EM.id = ASM.emp_id
                                    Inner join ".STUDENT_MASTER." As STM on STM.grade_id = ASM.grade_id
                                        Where 1=1 ".$where;*/
        $sql="SELECT *,CM.id AS course_id, CM.course_name AS course_name, GM.id AS grade_id,GM.standard_name AS standard_name,SM.section_name AS section_name,ASM.medium AS medium, STM.grade_id AS grade_id,PM.id AS parent_id FROM ".ASSIGNMENT_MASTER." AS ASM 
                Inner join ".COURSE_MASTER." AS CM on CM.id = ASM.course_id 
                  Inner join ".STANDARD_MASTER." AS GM on GM.id = ASM.grade_id 
                    Inner join ".SECTION_MASTER." AS SM on SM.id = ASM.section_id 
                      Inner join ".EMP_MASTER." AS EM on EM.id = ASM.emp_id 
                        Inner join ".STUDENT_MASTER." As STM on STM.grade_id = ASM.grade_id 
                          Inner join ".PARENT_MASTER." As PM on PM.id = STM.parent_id 
                            Where 1=1 ".$where;
        $db->query($sql);
        return $db->fetch_object();
    }

}
?>