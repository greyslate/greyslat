<?php
class LeaveType {
    function fetch_all($where = ''){
        global $db;
        $sql = "SELECT count(*) as cnt FROM " . LEAVE_TYPE . " Where 1=1 ".$where;
        $db->query($sql);
        $db->next_record();
        $this->total_record = $db->f("cnt");
        $db->free();
        if ($_SESSION['start_record'] >= $this->total_record || $_SESSION['page_size'] >= $this->total_record)
            $_SESSION['start_record'] = 0;
        $sql = "Select * From ".LEAVE_TYPE." 
                    Where 1=1 ".$where." Order By type_id asc 
                        LIMIT " . $_SESSION['start_record'] . ", " . $_SESSION['page_size'];
        $db->query($sql);
        return $db->fetch_object();
    }


    function getLeaveType($id){
        global $db;
        $sql = "Select * From ".LEAVE_TYPE." Where type_id = '".$id."' ";
        $db->query($sql);
        return $db->fetch_object(MYSQL_FETCH_SINGLE);
    }
    
}
?>