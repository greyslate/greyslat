<?php
class HolidayData {
    function fetch_all($where = ''){
        global $db;
        $sql = "SELECT count(*) as cnt FROM ".HOLIDAY_DATA."  AS TD
                    /*Inner join ".COURSE_MASTER." AS CM on CM.id = TD.course_id   
                        Inner join ".GRADE_MASTER." AS GM on GM.id = TD.grade_id
                            Inner join ".SECTION_MASTER." AS SM on SM.id = TD.section_id
                                Inner join ".EMP_MASTER." As EM on EM.id = TD.emp_id*/
                                    Inner join ".HOLIDAY_MASTER." As HM on HM.id = TD.h_id
                                Where 1=1 ".$where;
        $db->query($sql);
        $db->next_record();
        $this->total_record = $db->f("cnt");
        $db->free();
        if ($_SESSION['start_record'] >= $this->total_record || $_SESSION['page_size'] >= $this->total_record)
            $_SESSION['start_record'] = 0;
        $sql = "Select *,TD.id As id, GM.standard_name From ".HOLIDAY_DATA." As TD
                   /* Inner join ".COURSE_MASTER." AS CM on CM.id = TD.course_id   
                        Inner join ".GRADE_MASTER." AS GM on GM.id = TD.grade_id
                            Inner join ".SECTION_MASTER." AS SM on SM.id = TD.section_id
                                Inner join ".EMP_MASTER." As EM on EM.id = TD.emp_id*/
                                    Inner join ".HOLIDAY_MASTER." As HM on HM.id = TD.h_id
                                Where 1=1 ".$where." Order By TD.id desc 
                                    LIMIT " . $_SESSION['start_record'] . ", " . $_SESSION['page_size'];
        $db->query($sql);
        return $db->fetch_object();
    }

    function DeleteHolidayData($id){
        global $db;
        $sql = "Delete From ".HOLIDAY_DATA." Where id = '".$id."'";
        $db->query($sql);
    }

   /* function DeleteTimeTableDataDetail($id){
        global $db;
        $sql = "Delete From ".CLASS_DETAIL." Where class_id = '".$id."'";
        $db->query($sql);
    }*/

    function getHolidayeData($id){
        global $db;
        $sql = "Select * From ".HOLIDAY_DATA." Where id = '".$id."' ";
        $db->query($sql);
        return $db->fetch_object(MYSQL_FETCH_SINGLE);
    }

}
?>