<?php
class Announcement {
    function fetch_all($where = ''){
        global $db;
        $sql = "SELECT count(*) as cnt FROM ".ANNOUNCEMENT_MASTER."  AS AM
                    Inner join ".COURSE_MASTER." AS CM on CM.id = AM.course_id   
                        Inner join ".GRADE_MASTER." AS GM on GM.id = AM.grade_id
                                Where 1=1 ".$where;
        $db->query($sql);
        $db->next_record();
        $this->total_record = $db->f("cnt");
        $db->free();
        if ($_SESSION['start_record'] >= $this->total_record || $_SESSION['page_size'] >= $this->total_record)
            $_SESSION['start_record'] = 0;
        $sql = "Select *,AM.status As status,AM.id As id From ".ANNOUNCEMENT_MASTER." As AM
                    Inner join ".COURSE_MASTER." AS CM on CM.id = AM.course_id   
                        Inner join ".GRADE_MASTER." AS GM on GM.id = AM.grade_id
                                Where 1=1 ".$where." Order By AM.id desc 
                                    LIMIT " . $_SESSION['start_record'] . ", " . $_SESSION['page_size'];
        $db->query($sql);
        return $db->fetch_object();
    }

    function DeleteAnnouncement($id){
        global $db;
        $sql = "Delete From ".ANNOUNCEMENT_MASTER." Where id = '".$id."'";
        $db->query($sql);
    }
        
    function getAnnouncement($id){
        global $db;
        $sql = "Select * From ".ANNOUNCEMENT_MASTER." Where id = '".$id."' ";
        $db->query($sql);
        return $db->fetch_object(MYSQL_FETCH_SINGLE);
    }
       
}
?>