<?php
class ExamTimeTable {
    function fetch_all($where = ''){
        global $db;
        $sql = "SELECT count(*) as cnt FROM ".EXAM_TIMETABLE."  AS TT
                        Inner join ".GRADE_MASTER." AS GM on GM.id = TT.grade_id
                            Inner join ".EXAMTIMETABLE_DATA." As ETD on ETD.et_id = TT.id
                                Inner join ".COURSE_MASTER." AS CM on CM.id = ETD.course_id
                            Where 1=1 ".$where;
        $db->query($sql);
        $db->next_record();
        $this->total_record = $db->f("cnt");
        $db->free();
        if ($_SESSION['start_record'] >= $this->total_record || $_SESSION['page_size'] >= $this->total_record)
            $_SESSION['start_record'] = 0;
        $sql = "Select *,TT.status As status,TT.id As eid,TT.id As id From ".EXAM_TIMETABLE." As TT
                        Inner join ".GRADE_MASTER." AS GM on GM.id = TT.grade_id
                            Inner join ".EXAMTIMETABLE_DATA." As ETD on ETD.et_id = TT.id
                                Inner join ".COURSE_MASTER." AS CM on CM.id = ETD.course_id
                                Where 1=1 ".$where." Order By TT.id desc 
                                    LIMIT " . $_SESSION['start_record'] . ", " . $_SESSION['page_size'];
        $db->query($sql);
        return $db->fetch_object();
    }

    function DeleteExamTimeTable($id){
        global $db;
        $sql = "Delete From ".EXAM_TIMETABLE." Where id = '".$id."'";
        $db->query($sql);
    }
    
    function DeleteExamTimeTableDetail($id){
        global $db;
        $sql = "Delete From ".CLASS_DETAIL." Where class_id = '".$id."'";
        $db->query($sql);
    }
    
    function getExamTimeTable($id){
        global $db;
        $sql = "Select * From ".EXAM_TIMETABLE." Where id = '".$id."' ";
        $db->query($sql);
        return $db->fetch_object(MYSQL_FETCH_SINGLE);
    }

    function fetch_exam_list($where = '')
    {
        global $db;
        $sql = "Select * From " . EXAM_TIMETABLE . " 
                    Where status = 1 " . $where . " Order By exam_title ";
        $db->query($sql);
        return $db->fetch_object();
    }
}
?>