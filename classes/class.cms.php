<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of class
 *
 * @author Welcome
 */
class CMS {

    function fetch_all($where = '') {
        global $db;

        $sql = "SELECT count(*) as cnt FROM " . CMS_MASTER . " Where 1=1 " . $where;

        $db->query($sql);
        $db->next_record();
        $this->total_record = $db->f("cnt");
        $db->free();

        if ($_SESSION['start_record'] >= $this->total_record || $_SESSION['page_size'] >= $this->total_record)
            $_SESSION['start_record'] = 0;


        $sql = "Select * From " . CMS_MASTER . " 
                    Where 1=1 " . $where . " Order By id desc 
                        LIMIT " . $_SESSION['start_record'] . ", " . $_SESSION['page_size'];

        $db->query($sql);
        return $db->fetch_object();
    }

    function DeleteCMS($id) {
        global $db;

        $sql = "Delete From " . CMS_MASTER . " Where id = '" . $id . "'";
        $db->query($sql);
    }
    function getAliasCount($title, $where = '') {
        global $db;

        $sql = "Select COUNT(*) AS cnt From " . CMS_MASTER . " Where alias = '" . $title . "' " . $where;
        $db->query($sql);
        $record = $db->fetch_object(MYSQL_FETCH_SINGLE);
        return $record->cnt;
    }

    function getCMSPage($id) {
        global $db;
        $sql = "Select * From " . CMS_MASTER . " Where id = '" . $id . "' ";
        $db->query($sql);
        return $db->fetch_object(MYSQL_FETCH_SINGLE);
    }

    function getMaxId() {
        global $db;

        $sql = "Select MAX(id) As id From " . CMS_MASTER;
        $db->query($sql);
        return $db->fetch_object(MYSQL_FETCH_SINGLE);
    }

}
