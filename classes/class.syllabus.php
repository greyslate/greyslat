<?php
class Syllabus {
    function fetch_all($where = ''){
        global $db;
        $sql = "SELECT count(*) as cnt FROM ".SYLLABUS_MASTER."  AS SM
                    Inner join ".COURSE_MASTER." AS CM on CM.id = SM.course_id   
                        Inner join ".GRADE_MASTER." AS GM on GM.id = SM.grade_id
                                Where 1=1 ".$where;
        $db->query($sql);
        $db->next_record();
        $this->total_record = $db->f("cnt");
        $db->free();
        if ($_SESSION['start_record'] >= $this->total_record || $_SESSION['page_size'] >= $this->total_record)
            $_SESSION['start_record'] = 0;
        $sql = "Select *,SM.status As status,SM.id As id From ".SYLLABUS_MASTER." As SM
                    Inner join ".COURSE_MASTER." AS CM on CM.id = SM.course_id   
                        Inner join ".GRADE_MASTER." AS GM on GM.id = SM.grade_id
                                Where 1=1 ".$where." Order By SM.id desc 
                                    LIMIT " . $_SESSION['start_record'] . ", " . $_SESSION['page_size'];
        $db->query($sql);
        return $db->fetch_object();
    }

    function DeleteSyllabus($id){
        global $db;
        $sql = "Delete From ".SYLLABUS_MASTER." Where id = '".$id."'";
        $db->query($sql);
    }
        
    function getSyllabus($id){
        global $db;
        $sql = "Select * From ".SYLLABUS_MASTER." Where id = '".$id."' ";
        $db->query($sql);
        return $db->fetch_object(MYSQL_FETCH_SINGLE);
    }
       
}
?>