<?php
class Document{
    function fetch_all($where = ''){
        global $db;
        $sql = "SELECT count(*) as cnt FROM " . DOCUMENT_MASTER . " Where 1=1 ".$where;
        $db->query($sql);
        $db->next_record();
        $this->total_record = $db->f("cnt");
        $db->free();
        if ($_SESSION['start_record'] >= $this->total_record || $_SESSION['page_size'] >= $this->total_record)
            $_SESSION['start_record'] = 0;
        $sql = "Select * From ".DOCUMENT_MASTER." 
                    Where 1=1 ".$where." Order By id desc 
                        LIMIT " . $_SESSION['start_record'] . ", " . $_SESSION['page_size'];
        $db->query($sql);
        return $db->fetch_object();
    }

    function DeleteDocument($id){
        global $db;
        $sql = "Delete From ".DOCUMENT_MASTER." Where id = '".$id."'";
        $db->query($sql);
    }

    function getDocument($id){
        global $db;
        $sql = "Select * From ".DOCUMENT_MASTER." Where id = '".$id."' ";
        $db->query($sql);
        return $db->fetch_object(MYSQL_FETCH_SINGLE);
    }
    
}
?>