<?php
class Result {
    function fetch_all($where = ''){
        global $db;
        $sql = "SELECT count(*) as cnt FROM ".RESULT_MASTER."  AS TT
                    Inner join ".COURSE_MASTER." AS CM on CM.id = TT.course_id   
                    Inner join ".GRADE_MASTER." AS GM on GM.id = TT.standard_id
                    Inner join ".STUDENT_MASTER." AS SM on SM.id = TT.student_id
                    Inner join ".PARENT_MASTER." AS PM on PM.email = SM.p_email
                    Inner join ".EXAM_TIMETABLE." AS ET on ET.id = TT.exam_id
                    Inner join ".SECTION_MASTER." AS SE on SE.id = TT.section_id
                    Where 1=1 ".$where;
        $db->query($sql);
        $db->next_record();
        $this->total_record = $db->f("cnt");
        $db->free();
        if ($_SESSION['start_record'] >= $this->total_record || $_SESSION['page_size'] >= $this->total_record)
            $_SESSION['start_record'] = 0;
        $sql = "Select *,TT.id As id,SM.lname As lname,SM.fname As fname From ".RESULT_MASTER." As TT
                    Inner join ".COURSE_MASTER." AS CM on CM.id = TT.course_id   
                    Inner join ".GRADE_MASTER." AS GM on GM.id = TT.standard_id
                    Inner join ".STUDENT_MASTER." AS SM on SM.id = TT.student_id
                    Inner join ".PARENT_MASTER." AS PM on PM.email = SM.p_email
                    Inner join ".EXAM_TIMETABLE." AS ET on ET.id = TT.exam_id
                    Inner join ".SECTION_MASTER." AS SE on SE.id = TT.section_id
                    Where 1=1 ".$where." Order By TT.id desc 
                    LIMIT " . $_SESSION['start_record'] . ", " . $_SESSION['page_size'];
        $db->query($sql);
        return $db->fetch_object();
    }

    function DeleteResult($id){
        global $db;
        $sql = "Delete From ".RESULT_MASTER." Where id = '".$id."'";
        $db->query($sql);
    }


    function getResult($id){
        global $db;
        $sql = "Select * From ".RESULT_MASTER." Where id = '".$id."' ";
        $db->query($sql);
        return $db->fetch_object(MYSQL_FETCH_SINGLE);
    }


}
?>