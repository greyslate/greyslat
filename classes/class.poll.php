<?php
class Poll {
    function fetch_all($where = ''){
        global $db;
        $sql = "SELECT count(*) as cnt FROM ".POLL_MASTER."    
                        Where 1=1 ".$where;
        $db->query($sql);
        $db->next_record();
        $this->total_record = $db->f("cnt");
        $db->free();
        if ($_SESSION['start_record'] >= $this->total_record || $_SESSION['page_size'] >= $this->total_record)
            $_SESSION['start_record'] = 0;
        $sql = "Select * From ".POLL_MASTER." 
                    Where 1=1 ".$where." Order By id desc 
                        LIMIT " . $_SESSION['start_record'] . ", " . $_SESSION['page_size'];
        $db->query($sql);
        return $db->fetch_object();
    }

    function DeletePoll($id){
        global $db;
        $sql = "Delete From ".POLL_MASTER." Where id = '".$id."'";
        $db->query($sql);
    }
    
    function DeletePollDetail($id){
        global $db;
        $sql = "Delete From ".POLL_DETAIL." Where poll_id = '".$id."'";
        $db->query($sql);
    }
    
    function getPoll($id){
        global $db;
        $sql = "Select * From ".POLL_MASTER." Where id = '".$id."' ";
        $db->query($sql);
        return $db->fetch_object(MYSQL_FETCH_SINGLE);
    }
    
    function getPollDetail($id){
        global $db;
        $sql = "Select * From ".POLL_DETAIL." Where poll_id = '".$id."' ";
        $db->query($sql);
        return $db->fetch_object();
    }
    
    function getPollDetailInfo($id){
        global $db;
        $sql = "Select * From ".POLL_DETAIL." Where id = '".$id."' ";
        $db->query($sql);
        return $db->fetch_object(MYSQL_FETCH_SINGLE);
    }
}
?>