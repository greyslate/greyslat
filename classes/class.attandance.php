<?php
class Attandance {
    function fetch_all($where = ''){
        global $db;
        $sql = "SELECT count(*) as cnt FROM " . ATTANDANCE_MASTER . " Where 1=1 ".$where;
        $db->query($sql);
        $db->next_record();
        $this->total_record = $db->f("cnt");
        $db->free();
        if ($_SESSION['start_record'] >= $this->total_record || $_SESSION['page_size'] >= $this->total_record)
            $_SESSION['start_record'] = 0;
        $sql = "Select * From ".ATTANDANCE_MASTER." 
                    Where 1=1 ".$where." Order By id desc 
                        LIMIT " . $_SESSION['start_record'] . ", " . $_SESSION['page_size'];
        $db->query($sql);
        return $db->fetch_object();
    }

    function DeleteAttandance($id){
        global $db;
        $sql = "Delete From ".ATTANDANCE_MASTER." Where id = '".$id."'";
        $db->query($sql);
    }

    function getAttandance($id){
        global $db;
        $sql = "Select * From ".ATTANDANCE_MASTER." Where id = '".$id."' ";
        $db->query($sql);
        return $db->fetch_object(MYSQL_FETCH_SINGLE);
    }

    function fetch_attandance_list($where = ''){
        global $db;
        $sql = "Select * From ".ATTANDANCE_MASTER." As AM 
                    Inner Join ".STUDENT_MASTER." As SM on SM.roll_no = AM.student_roll_no 
                        Where 1=1 ".$where;
        $db->query($sql);
        return $db->fetch_object();
    }

}
?>