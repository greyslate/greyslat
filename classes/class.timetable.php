<?php
class TimeTable {
    function fetch_all($where = ''){
        global $db;
        $sql = "SELECT count(*) as cnt FROM ".TIME_TABLE."  AS TT
                        Inner join ".GRADE_MASTER." AS GM on GM.id = TT.grade_id
                            Inner join ".SECTION_MASTER." AS SM on SM.id = TT.section_id
                                Inner join ".TIMETABLE_DATA." AS TD on TD.tt_id = TT.id
                                Where 1=1 ".$where;
        $db->query($sql);
        $db->next_record();
        $this->total_record = $db->f("cnt");
        $db->free();
        if ($_SESSION['start_record'] >= $this->total_record || $_SESSION['page_size'] >= $this->total_record)
            $_SESSION['start_record'] = 0;
        $sql = "Select *,TT.status As status,TT.id As tid From ".TIME_TABLE." As TT
                        Inner join ".GRADE_MASTER." AS GM on GM.id = TT.grade_id
                            Inner join ".SECTION_MASTER." AS SM on SM.id = TT.section_id
                                Inner join ".TIMETABLE_DATA." AS TD on TD.tt_id = TT.id
                                Where 1=1 ".$where." Order By TT.id desc 
                                    LIMIT " . $_SESSION['start_record'] . ", " . $_SESSION['page_size'];
        $db->query($sql);
        return $db->fetch_object();
    }

    function DeleteTimeTable($id){
        global $db;
        $sql = "Delete From ".TIME_TABLE." Where id = '".$id."'";
        $db->query($sql);
    }
    
    function DeleteTimeTableDetail($id){
        global $db;
        $sql = "Delete From ".CLASS_DETAIL." Where class_id = '".$id."'";
        $db->query($sql);
    }
    
    function getTimeTable($id){
        global $db;
        $sql = "Select * From ".TIME_TABLE." Where id = '".$id."' ";
        $db->query($sql);
        return $db->fetch_object(MYSQL_FETCH_SINGLE);
    }
       
}
?>