<?php
class ExamTimeTableData {
    function fetch_all($where = ''){
        global $db;
        $sql = "SELECT count(*) as cnt FROM ".EXAMTIMETABLE_DATA."  AS ETD
                    
                        Inner join ".GRADE_MASTER." AS GM on GM.id = ETD.grade_id
                            Inner join ".COURSE_MASTER." AS CM on CM.id = ETD.course_id
                                    Inner join ".EXAM_TIMETABLE." As TT on TT.id = ETD.et_id
                                Where 1=1 ".$where;
        $db->query($sql);
        $db->next_record();
        $this->total_record = $db->f("cnt");
        $db->free();
        if ($_SESSION['start_record'] >= $this->total_record || $_SESSION['page_size'] >= $this->total_record)
            $_SESSION['start_record'] = 0;
        $sql = "Select *,ETD.ed_id As id, GM.standard_name From ".EXAMTIMETABLE_DATA." As ETD
                     
                        Inner join ".GRADE_MASTER." AS GM on GM.id = ETD.grade_id
                                Inner join ".COURSE_MASTER." AS CM on CM.id = ETD.course_id
                                    Inner join ".EXAM_TIMETABLE." As TT on TT.id = ETD.et_id
                                Where 1=1 ".$where." Order By ETD.ed_id desc 
                                    LIMIT " . $_SESSION['start_record'] . ", " . $_SESSION['page_size'];
        $db->query($sql);
        return $db->fetch_object();
    }

    function DeleteExamTimeTableData($id){
        global $db;
        $sql = "Delete From ".EXAMTIMETABLE_DATA." Where ed_id = '".$id."'";
        $db->query($sql);
    }

    function DeleteExamTimeTableDataDetail($id){
        global $db;
        $sql = "Delete From ".CLASS_DETAIL." Where class_id = '".$id."'";
        $db->query($sql);
    }

    function getExamTimeTableData($id){
        global $db;
        $sql = "Select * From ".EXAMTIMETABLE_DATA." Where ed_id = '".$id."' ";
        $db->query($sql);
        return $db->fetch_object(MYSQL_FETCH_SINGLE);
    }

}
?>