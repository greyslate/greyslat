/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function changePassword() {
    
   

    var error = false;
    var errortext=''
    var $inputs = $('#frmchangepassword :input');
    $inputs.each(function(){
        if($(this).hasClass('required')){
            if($(this).val()=='' && $(this).attr('name') != "content"){//alert(here1);
                error = true;
                errortext += '<li>- '+$(this).attr('title')+'</li>';
                $(this).closest('.desc_div').show();
            }
        }
    });
    
    
    if($("#txtnewpassword").val() != "" && $("#txtcnewpassword").val() != ""){
        if($("#txtnewpassword").val() != $("#txtcnewpassword").val()){
            error = true;
            errortext += '<li>- Password Mismatch</li>';
        }
    }
    
   if(error == true) {
        $("#failmsg").html('<ul><li>Please correct the following errors</li>'+errortext+'</ul>'); 
        $("#failmsg").show();									
        //$("#error_div").attr("tabindex",-1).focus();
        $("html, body").animate({ scrollTop: 0 }, 500);
        $( "#failmsg" ).fadeIn( 3000, function() {
            $( "#failmsg" ).fadeOut( 5000 );
        });
        return false;
   }
   else {

        var param = $("#frmchangepassword").serialize();

        $.ajax({
            url: base_url+'ajaxfiles/change-password.php',
            type: 'POST',
            data: param+"&type=changepassword",
            success: function(data){
                if (data != 'Yes'){
                    $("#failmsg").html("Old Password is wrong");
                    $("#failmsg").show();
                    $( "#failmsg" ).fadeIn( 3000, function() {
                        $( "#failmsg" ).fadeOut( 5000 );
                    });
                }
                else{
                    $("#divMsg").html("Password has been change."); 
                    $("#divMsg").show();
                    $("html, body").animate({ scrollTop: 0 }, 500);
                    $( "#divMsg" ).fadeIn( 3000, function() {
                        $( "#divMsg" ).fadeOut( 5000 );
                    });
                    
                    $("#txtoldpassword").val("");
                    $("#txtnewpassword").val("");
                    $("#txtcnewpassword").val("");
                }
            }
        });
        
    }
}


$(document).ready(function()
{



    $(".sl_link").click(function(event) {

        $('#divMsg').hide();

        $("#error_msg").hide();

        $('.l_pane').slideToggle('normal').toggleClass('dn');

        $('.sl_link,.lb_ribbon').children('span').toggle();

        event.preventDefault();

    });



    // Forgot password

    $("#btnForgotPwd").click(function(event) {

        var error = false;

        var tbxEmailAddress = $("#tbxEmailAddress").val();

        if (tbxEmailAddress == '')
            error = true;

        var atpos = tbxEmailAddress.indexOf("@");
        var dotpos = tbxEmailAddress.lastIndexOf(".");
        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= tbxEmailAddress.length){

            error = true;

        }

        if (error == true){
            $("#error_msg").html("Please enter valid email address");
            $("#error_msg").show();
            return false;
        }

        var param = $("#frmUserForgetPwd").serialize();

        $.ajax({
            url: base_url+'ajaxfiles/ajaxlogin.php',
            type: 'POST',
            data: param+"&type=forgotpassword",
            success: function(data){

                $("#bodyid").hideLoading();
                if (data == ''){
                    $("#error_msg").html("Invalid Username/Password");
                    $("#error_msg").show();
                }
                else{
                    $("#divMsg").html("Password has been sent to your email."); 
                    $("#divMsg").show();
                    $("html, body").animate({ scrollTop: 0 }, 500);
                    $( "#divMsg" ).fadeIn( 3000, function() {
                        $( "#divMsg" ).fadeOut( 5000 );
                    });
                }
            }
        });
    });
});