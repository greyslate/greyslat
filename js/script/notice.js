// JavaScript Document
$(document).ready(function (e) {
    $('.date-picker').datepicker({
        rtl: Metronic.isRTL(),
        orientation: "left",
        autoclose: true,
        format: 'yyyy-mm-dd',
    });

    // Search box toggle 
    $(".bAct_toggle").click(function () {
        $(this).parent().next().slideToggle();
        var htmlsrc = $(this).find("img");
        if (htmlsrc.hasClass('bAct_minus') == true) {
            htmlsrc.removeClass('bAct_minus');
            htmlsrc.addClass('bAct_plus');
        }
        else {
            htmlsrc.removeClass('bAct_plus');
            htmlsrc.addClass('bAct_minus');
        }
    });

    $("#btnReset").click(function () {
//        $('#grade').val('').trigger('liszt:updated');
//        $('#course').val('').trigger('liszt:updated');
//        $('#section').val('').trigger('liszt:updated');
        $(".filterclass .active").removeClass("active");
    });

    $("#btnReset").click(function () {
        $("#bodyid").showLoading();
        $.ajax({
            url: base_url + 'ajaxfiles/notice-list.php',
            type: 'PeST',
            data: '',
            success: function (data) {
                $("#bodyid").hideLoading();
                $('#page_listing').html(data);
            }
        });
    });

    var error1 = $('.alert-danger', $("#frmadd"));
    var success1 = $('.alert-success', $("#frmadd"));

    jQuery.extend(jQuery.validator.messages, {
        equalTo: "Please enter the same password again.",
    });

    $("#frmadd").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden input
        rules: {
            txtpassword: {
                minlength: 6,
            },
            txtcpassword: {
                equalTo: "#txtpassword",
                minlength: 6,
            }

        },
        messages: {
            txtcpasword: {
                equalTo: "Please enter the same password as above",
            },
        },
        invalidHandler: function (event, validator) { //display error alert on form submit              
            success1.hide();
            error1.show();
            Metronic.scrollTo(error1, -200);
        },
        highlight: function (element) { // hightlight error inputs
            $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            $("#bodyid").showLoading();
            error1.hide();

            $.ajax({
                url: base_url + 'ajaxfiles/notice-addedit.php',
                type: "POST",
                data: new FormData(form),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    $("#bodyid").hideLoading();
                    if (data.trim() == 'Yes') {
                        toastr.success("Record has been saved successfully.", "Success!");
                        $("html, body").animate({scrollTop: 0}, 500);
                        setTimeout(function () {
                            window.location.href = "notice-list.php"; //will redirect to your blog page (an ex: blog.html)
                        }, 1000);
                    }
                    else {
                        toastr.error(data, "Failed!");
                        $("html, body").animate({scrollTop: 0}, 500);
                        return false;
                    }
                },
                error: function () {
                }
            });
        }
    });


    var error1 = $('.alert-danger', $("#frmedit"));
    var success1 = $('.alert-success', $("#frmedit"));

    $("#frmedit").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden input
        rules: {
            txtpassword: {
                minlength: 6,
            },
            txtcpassword: {
                equalTo: "#txtpassword",
                minlength: 6,
            }
        },
        messages: {
            txtcpasword: {
                equalTo: "Please enter the same password as above",
            },
        },
        invalidHandler: function (event, validator) { //display error alert on form submit              
            success1.hide();
            error1.show();
            Metronic.scrollTo(error1, -200);
        },
        highlight: function (element) { // hightlight error inputs
            $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            $("#bodyid").showLoading();
            error1.hide();

            $.ajax({
                url: base_url + 'ajaxfiles/notice-addedit.php',
                type: "POST",
                data: new FormData(form),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    $("#bodyid").hideLoading();
                    if (data.trim() == 'Yes') {
                        toastr.success("Record has been saved successfully.", "Success!");
                        $("html, body").animate({scrollTop: 0}, 500);
                        setTimeout(function () {
                            window.location.href = "notice-list.php"; //will redirect to your blog page (an ex: blog.html)
                        }, 1000);
                    }
                    else {
                        toastr.error(data, "Failed!");
                        $("html, body").animate({scrollTop: 0}, 500);
                        return false;
                    }
                },
                error: function () {
                }
            });
        }
    });

});

function ChangeStatus(val, id) {
    $("#bodyid").showLoading();

    var q_mark = $("#q_mark").val();

    $.ajax({
        url: base_url + 'ajaxfiles/notice-list.php',
        type: 'POST',
        data: {action: "changestatus", id: id, status: val, q_mark: q_mark},
        success: function (data) {
            $("#bodyid").hideLoading();
            $('#page_listing').html(data);
            toastr.success("Record has been updated successfully", "Status Updated!")
            $("html, body").animate({scrollTop: 0}, 500);
        }
    });
}

function RemoveRecord(id) {

    var res_cnf = confirm("Are you sure you want to delete this record ?");

    var q_mark = $("#q_mark").val();

    if (res_cnf == true) {
        $("#bodyid").showLoading();

        $.ajax({
            url: base_url + 'ajaxfiles/notice-list.php',
            type: 'POST',
            data: {action: "delete", id: id, q_mark: q_mark},
            success: function (data) {
                $("#bodyid").hideLoading();
                $('#page_listing').html(data);

                $("html, body").animate({scrollTop: 0}, 500);
                toastr.success("Record has been deleted successfully", "Record Deleted!")

            }
        });
    }
}


function custAction(action, actiongobutton) {

    $("#checkgobutton").val("Go");
    if (action == "Action") {
        alert("Please select action");
        return false;
    }

    var param = $("#frmtable").serialize();

    if ($("#frmtable input:checkbox:checked").length == 0) {
        alert("Please select atleast one record");
        return false;
    }

    var dis_msg_txt;

    if (action == "Delete") {
        dis_msg_txt = "deleted";
        var res_cnf = confirm("Are you sure you want to Delete this record ?");
    }

    if (action == "Active") {
        dis_msg_txt = "activated";
        var res_cnf = confirm("Are you sure you want Activate this record ?");
    }

    if (action == "Inactive") {
        dis_msg_txt = "inactivated";
        var res_cnf = confirm("Are you sure you want Deactivate this record ?");
    }

//    if($("#tbxTitle").val() != ""){}
//        param += "&tbxTitle="+$("#tbxTitle").val()+"&task=search";

    if (res_cnf == true) {
        $("#bodyid").showLoading();
        $.ajax({
            url: base_url + 'ajaxfiles/notice-list.php',
            type: 'POST',
            data: param,
            success: function (data) {
                $("#bodyid").hideLoading();
                $('#page_listing').html(data);

                if (dis_msg_txt == "deleted") {
                    $("#divMsg").show();
                    $("html, body").animate({scrollTop: 0}, 500);
                    $("#divMsg").html("Record has been deleted successfully");
                }
                else if (dis_msg_txt == "inactivated" || dis_msg_txt == "activated") {
                    $("#divMsg").show();
                    $("html, body").animate({scrollTop: 0}, 500);
                    $("#divMsg").html("Record has been updated successfully");
                }
            }
        });
    }
}

function Search_Data() {
    $("#bodyid").showLoading();
    var form_data = $("#frmtable , #frmSearch").serialize();
    form_data = form_data + "&task=search";
    $.ajax({
        url: base_url + 'ajaxfiles/notice-list.php',
        type: 'POST',
        data: form_data, //{task:task, search_term:search_term , search_status:search_status,pageval:pageval,limit_val:limit_val} ,
        success: function (data) {
            $("#page_listing").html(data);
            $("#checkgobutton").val("");
            $("#drpAction").val('').trigger("liszt:updated");
            $("#bodyid").hideLoading();
        }
    });
    return false;
}

function Pagination(pageno) {
    $("#bodyid").showLoading();

    var form_data = $("#frmtable , #frmSearch").serialize();
    form_data = form_data + "&task=search";
    form_data = form_data + "&start=" + pageno;

    $.ajax({
        url: base_url + 'ajaxfiles/notice-list.php',
        type: 'POST',
        data: form_data,
        success: function (data) {
            $("#page_listing").html(data);
            $("#bodyid").hideLoading();
        }
    });
    return false;
}

function setPage(page) {

    $("#bodyid").showLoading();

    var form_data = $("#frmtable , #frmSearch").serialize();
    form_data = form_data + "&task=search";
    form_data = form_data + "&pagesize=" + page;


    $.ajax({
        url: base_url + 'ajaxfiles/notice-list.php',
        type: 'POST',
        data: form_data,
        success: function (data) {
            $("#page_listing").html(data);
            $("#bodyid").hideLoading();
        }
    });
    return false;
}


function resetEvent(val) {
    $("#bodyid").showLoading();
    $("#q_mark").val("");
    $.ajax({
        url: base_url + 'ajaxfiles/notice-list.php',
        type: 'POST',
        data: '',
        success: function (data) {
            $("#page_listing").html(data);
            $("#bodyid").hideLoading();
        }
    });
    return false;
}

function setEvent() {
    $(".filterclass .active").removeClass("active");
    $(".anc").addClass("active");
}