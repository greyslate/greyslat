// JavaScript Document
var $j = jQuery.noConflict();
$j(document).ready(function (e) {
    $j('.date-picker').datepicker({
        rtl: Metronic.isRTL(),
        orientation: "left",
        autoclose: true,
        format: 'dd/mm/yyyy',
    });

    // Search box toggle 
    $j(".bAct_toggle").click(function () {
        $j(this).parent().next().slideToggle();
        var htmlsrc = $j(this).find("img");
        if (htmlsrc.hasClass('bAct_minus') == true) {
            htmlsrc.removeClass('bAct_minus');
            htmlsrc.addClass('bAct_plus');
        }
        else {
            htmlsrc.removeClass('bAct_plus');
            htmlsrc.addClass('bAct_minus');
        }
    });

    $j("#btnReset").click(function () {
        setClass();
        $j("#tbxTitle").val('');
        $j(".filterstudent .active").removeClass("active");
    });

    $j("#btnReset").click(function () {
        $j("#bodyid").showLoading();
        setClass();
        $j.ajax({
            url: base_url + 'ajaxfiles/student-list.php',
            type: 'POST',
            data: '',
            success: function (data) {
                $j("#bodyid").hideLoading();
                $j('#page_listing').html(data);
            }
        });
    });

    var error1 = $j('.alert-danger', $j("#frmadd"));
    var success1 = $j('.alert-success', $j("#frmadd"));

    $j("#frmadd").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden input

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success1.hide();
            error1.show();
            Metronic.scrollTo(error1, -200);
        },
        highlight: function (element) { // hightlight error inputs
            $j(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $j(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            $j("#bodyid").showLoading();
            error1.hide();

            $j.ajax({
                url: base_url + 'ajaxfiles/student-addedit.php',
                type: "POST",
                data: new FormData(form),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    $j("#bodyid").hideLoading();
                    if (data.trim() == 'Yes') {
                        toastr.success("Record has been saved successfully.", "Success!");
                        $j("html, body").animate({scrollTop: 0}, 500);
                        setTimeout(function () {
                            window.location.href = "student-list.php"; //will redirect to your blog page (an ex: blog.html)
                        }, 1000);
                    }
                    else {
                        toastr.error(data, "Failed!");
                        $j("html, body").animate({scrollTop: 0}, 500);
                        return false;
                    }
                },
                error: function () {
                }
            });
        }
    });


    var error1 = $j('.alert-danger', $j("#frmedit"));
    var success1 = $j('.alert-success', $j("#frmedit"));

    $j("#frmedit").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden input

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success1.hide();
            error1.show();
            Metronic.scrollTo(error1, -200);
        },
        highlight: function (element) { // hightlight error inputs
            $j(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $j(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            $j("#bodyid").showLoading();
            error1.hide();

            $j.ajax({
                url: base_url + 'ajaxfiles/student-addedit.php',
                type: "POST",
                data: new FormData(form),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    $j("#bodyid").hideLoading();
                    if (data.trim() == 'Yes') {
                        toastr.success("Record has been saved successfully.", "Success!");
                        $j("html, body").animate({scrollTop: 0}, 500);
                        setTimeout(function () {
                            window.location.href = "student-list.php"; //will redirect to your blog page (an ex: blog.html)
                        }, 1000);
                    }
                    else {
                        toastr.error(data, "Failed!");
                        $j("html, body").animate({scrollTop: 0}, 500);
                        return false;
                    }
                },
                error: function () {
                }
            });
        }
    });


});

function ChangeStatus(val, id) {
    $j("#bodyid").showLoading();

    var q_mark = $j("#q_mark").val();

    $j.ajax({
        url: base_url + 'ajaxfiles/student-list.php',
        type: 'POST',
        data: {action: "changestatus", id: id, status: val, q_mark: q_mark},
        success: function (data) {
            $j("#bodyid").hideLoading();
            $j('#page_listing').html(data);
            toastr.success("Record has been updated successfully", "Status Updated!")
            $j("html, body").animate({scrollTop: 0}, 500);
        }
    });
}

function RemoveRecord(id) {

    var res_cnf = confirm("Are you sure you want to delete this record ?");

    var q_mark = $j("#q_mark").val();

    if (res_cnf == true) {
        $j("#bodyid").showLoading();

        $j.ajax({
            url: base_url + 'ajaxfiles/student-list.php',
            type: 'POST',
            data: {action: "delete", id: id, q_mark: q_mark},
            success: function (data) {
                $j("#bodyid").hideLoading();
                $j('#page_listing').html(data);

                $j("html, body").animate({scrollTop: 0}, 500);
                toastr.success("Record has been deleted successfully", "Record Deleted!")

            }
        });
    }
}


function custAction(action, actiongobutton) {

    $j("#checkgobutton").val("Go");
    if (action == "Action") {
        alert("Please select action");
        return false;
    }

    var param = $j("#frmtable").serialize();

    if ($j("#frmtable input:checkbox:checked").length == 0) {
        alert("Please select atleast one record");
        return false;
    }

    var dis_msg_txt;

    if (action == "Delete") {
        dis_msg_txt = "deleted";
        var res_cnf = confirm("Are you sure you want to Delete this record ?");
    }

    if (action == "Active") {
        dis_msg_txt = "activated";
        var res_cnf = confirm("Are you sure you want Activate this record ?");
    }

    if (action == "Inactive") {
        dis_msg_txt = "inactivated";
        var res_cnf = confirm("Are you sure you want Deactivate this record ?");
    }

//    if($j("#tbxTitle").val() != ""){}
//        param += "&tbxTitle="+$j("#tbxTitle").val()+"&task=search";

    if (res_cnf == true) {
        $j("#bodyid").showLoading();
        $j.ajax({
            url: base_url + 'ajaxfiles/student-list.php',
            type: 'POST',
            data: param,
            success: function (data) {
                $j("#bodyid").hideLoading();
                $j('#page_listing').html(data);

                if (dis_msg_txt == "deleted") {
                    $j("#divMsg").show();
                    $j("html, body").animate({scrollTop: 0}, 500);
                    $j("#divMsg").html("Record has been deleted successfully");
                }
                else if (dis_msg_txt == "inactivated" || dis_msg_txt == "activated") {
                    $j("#divMsg").show();
                    $j("html, body").animate({scrollTop: 0}, 500);
                    $j("#divMsg").html("Record has been updated successfully");
                }
            }
        });
    }
}

function Search_Data() {
    $j("#bodyid").showLoading();
    var form_data = $j("#frmtable , #frmSearch").serialize();
    form_data = form_data + "&task=search";
    $j.ajax({
        url: base_url + 'ajaxfiles/student-list.php',
        type: 'POST',
        data: form_data, //{task:task, search_term:search_term , search_status:search_status,pageval:pageval,limit_val:limit_val} ,
        success: function (data) {
            $j("#page_listing").html(data);
            $j("#checkgobutton").val("");
            $j("#drpAction").val('').trigger("liszt:updated");
            $j("#bodyid").hideLoading();
        }
    });
    return false;
}

function Pagination(pageno) {
    $j("#bodyid").showLoading();

    var form_data = $j("#frmtable , #frmSearch").serialize();
    form_data = form_data + "&task=search";
    form_data = form_data + "&start=" + pageno;

    $j.ajax({
        url: base_url + 'ajaxfiles/student-list.php',
        type: 'POST',
        data: form_data,
        success: function (data) {
            $j("#page_listing").html(data);
            $j("#bodyid").hideLoading();
        }
    });
    return false;
}

function setPage(page) {

    $j("#bodyid").showLoading();

    var form_data = $j("#frmtable , #frmSearch").serialize();
    form_data = form_data + "&task=search";
    form_data = form_data + "&pagesize=" + page;


    $j.ajax({
        url: base_url + 'ajaxfiles/student-list.php',
        type: 'POST',
        data: form_data,
        success: function (data) {
            $j("#page_listing").html(data);
            $j("#bodyid").hideLoading();
        }
    });
    return false;
}


function setStudent(val) {
    $j("#bodyid").showLoading();

    $j("#q_mark").val(val);

    var form_data = $j("#frmtable , #frmSearch").serialize();
    form_data = form_data + "&task=char";

    $j.ajax({
        url: base_url + 'ajaxfiles/student-list.php',
        type: 'POST',
        data: form_data,
        success: function (data) {
            $j("#page_listing").html(data);
            $j("#bodyid").hideLoading();
        }
    });
    return false;
}


function resetStudent(val) {
    $j("#bodyid").showLoading();
    $j("#q_mark").val("");
    $j.ajax({
        url: base_url + 'ajaxfiles/student-list.php',
        type: 'POST',
        data: '',
        success: function (data) {
            $j("#page_listing").html(data);
            $j("#bodyid").hideLoading();
        }
    });
    return false;
}

function setClass() {
    $j(".filterstudent .active").removeClass("active");
    $j(".anc").addClass("active");
}