// JavaScript Document
$(document).ready(function (e) {
    // All select box will convert chosen selectbox style

    var error1 = $('.alert-danger', $("#frmedit"));
    var success1 = $('.alert-success', $("#frmedit"));

    $("#frmedit").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden input

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success1.hide();
            error1.show();
            Metronic.scrollTo(error1, -200);
        },
        highlight: function (element) { // hightlight error inputs
            $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            $("#bodyid").showLoading();
            error1.hide();

            $.ajax({
                url: base_url + 'ajaxfiles/profile-edit.php',
                type: "POST",
                data: new FormData(form),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    $("#bodyid").hideLoading();
                    if (data.trim() == 'Yes') {
                        toastr.success("Record has been saved successfully.", "Success!");
                        $("html, body").animate({scrollTop: 0}, 500);
                        setTimeout(function () {
                            window.location.href = "index.php"; //will redirect to your blog page (an ex: blog.html)
                        }, 1000);
                    }
                    else {
                        toastr.error(data, "Failed!");
                        $("html, body").animate({scrollTop: 0}, 500);
                        return false;
                    }
                },
                error: function () {
                }
            });
        }
    });

});