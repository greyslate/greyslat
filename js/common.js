/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function checkAlluncheckAll(frm) {
    with (frm) {
        if (checkall.checked == true) {
            chkAll(frm);
        }
        else {
            UnchkAll(frm);
        }
    }
}


function UnchkAll(form) {
    with (form) {
        len = elements.length;
        var i = 0;
        Checked = false;
        for (i = 0; i < len; i++)
        {
            if (elements[i].type == 'checkbox') {
                elements[i].checked = false;
            }
        }
        return false;
    }
}

function chkAll(form) {
    with (form) {
        len = elements.length;
        var i = 0;
        Checked = false;
        for (i = 0; i < len; i++) {
            if (elements[i].type == 'checkbox') {
                elements[i].checked = true;
            }
        }
        return false;
    }
}

function chkSelectedRecord(form) {
    len = form.elements.length;
    var i = 0;
    var Found = false;
    for (i = 0; i < len; i++) {
        if (form.elements[i].type == 'checkbox') {
            if (form.elements[i].checked == true) {
                Found = true;
                break;
            }
        }
    }
    if (Found == false) {
        alert("Please select at least one record.");
        return false;
    }
    return true;
}


function page_view_toggel() {
    if ($('.myrs-pagination-maxvalue-list').css('display') == "block") {
        $('.myrs-pagination-maxvalue-list').css('display', 'none');
    }
    else {
        $('.myrs-pagination-maxvalue-list').css('display', 'block');
    }
}

function verifyMail(obj)
{
    if (obj.value.length != 0)
    {
        var str = obj.value;
        var reg1 = /(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/; // not valid
        var reg2 = /^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?)$/; // valid
        if (!reg1.test(str) && reg2.test(str))
        {
            // return true;
        } else
        {
            alert("\"" + str + "\" is an invalid e-mail!");
            obj.focus();
            return false;
        }
    }
}


function verifyMail1(obj)
{
    if (obj.length != 0)
    {
        var str = obj;
        var reg1 = /(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/; // not valid
        var reg2 = /^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?)$/; // valid
        if (!reg1.test(str) && reg2.test(str))
        {
            // return true;
        }
        else
        {
            return false;
        }
    }
}