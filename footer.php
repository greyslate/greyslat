<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        2016 &copy; Greyslate.
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo THEME_URL; ?>global/plugins/respond.min.js"></script>
<script src="<?php echo THEME_URL; ?>global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo THEME_URL; ?>global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo THEME_URL; ?>global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo THEME_URL; ?>global/plugins/bootstrap/js/bootstrap.js" type="text/javascript"></script>
<script src="<?php echo THEME_URL; ?>global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo THEME_URL; ?>global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo THEME_URL; ?>global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo THEME_URL; ?>global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo THEME_URL; ?>global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo THEME_URL; ?>global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo THEME_URL; ?>global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="<?php echo THEME_URL; ?>global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo THEME_URL; ?>admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo THEME_URL; ?>admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="<?php echo THEME_URL; ?>admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="<?php echo JS_URL; ?>loading.js" type="text/javascript"></script>  
<script src="<?php echo JS_URL; ?>common.js" type="text/javascript"></script>  
<!--[if lt IE 9]>
    <link rel="stylesheet" href="<?php echo JS_URL; ?>ie.css" />
    <script src="<?php echo JS_URL; ?>html5.js" type="text/javascript"></script>
<![endif]-->
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="lib/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="lib/fancybox/source/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="lib/fancybox/source/jquery.fancybox.css?v=2.1.4" media="screen" />


<script type="text/javascript" src="<?php echo LIB_URL; ?>ckeditor/js/ckeditor/ckeditor.js"></script>
<script src="<?php echo LIB_URL; ?>ckeditor/sample.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo THEME_URL; ?>global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo THEME_URL; ?>global/plugins/jquery-validation/js/additional-methods.min.js"></script>
<script src="<?php echo PLUGIN_URL; ?>bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
<script src="<?php echo JS_URL; ?>ui-toastr.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo THEME_URL; ?>global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?php echo THEME_URL; ?>global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo THEME_URL; ?>global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>

<script>
    toastr.options = {
        "closeButton": true,
        "preventDuplicates": true,
        "debug": false,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "4000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
    }
</script>
<script type="text/javascript" src="<?php echo THEME_URL; ?>global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
    });
</script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('.fancybox').fancybox({
            'width': 370,
            'height': 'auto',
            'autoScale': false,
            'autoDimensions': false,
            'scrolling': 'no',
            'transitionIn': 'none',
            'transitionOut': 'none',
            'overflow': 'hidden',
            'padding': 10,
            'type': 'ajax',
            'title': '',
        });
    });
</script>
</body>
</html>

