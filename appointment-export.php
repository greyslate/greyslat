<?php

    include("includes/config-frontend.php");
    $objPHPExcel = new PHPExcel();
    $sql = "Select * From ".APPOINTMENT_MASTER." AS AM
                INNER JOIN ".STUDENT_MASTER." AS SM on SM.id = AM.student_id 
                order by AM.id desc";
    $db->query($sql);

    $rows = $db->fetch_object();

    $i = 0;
    $main_arr = array();
    if(count($rows) > 0){
        foreach($rows as $val){
           
            $main_arr[$i]["title"]      = $val->title;
            $main_arr[$i]["desc"]       = $val->description;
            $main_arr[$i]["fname"]     = $val->fname;
            $main_arr[$i]["app_date"]     = $val->app_date;
            $main_arr[$i]["app_time"]     = $val->app_time;
            $main_arr[$i]["posted_on"] = $val->posted_on;
            $main_arr[$i]["updated_on"] = $val->updated_on;
           
            $i++;
        }
    }


    $rowCount = 1;
    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "Title");
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Description");
    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Student Name");
    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, "Appointment Date");
    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, "Appointment Time");
    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "Posted On");
    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, "Updated On");
   


    $rowCount = 2;
    for($i=0;$i<count($main_arr);$i++){
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $main_arr[$i]['title']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $main_arr[$i]['desc']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $main_arr[$i]['fname']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $main_arr[$i]['app_date']);
         $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $main_arr[$i]['app_time']);

        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $main_arr[$i]['posted_on']);
         $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $main_arr[$i]['updated_on']);
    

        $rowCount++;
        //pr($objPHPExcel); 
    }

    header('Content-Type: application/vnd.openxmlformats-   officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.date("d-m-Y").'-appopintment.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

?>