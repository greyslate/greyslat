<?php
include("header.php");


$cobj = new Assignment();
//$id = $_SESSION['id'];
//$emp_id= $_SESSION['emp_id'];
//$parent_id = $_SESSION['parent_id'];
//$emp_id = $_SESSION['emp_id'];
$email = $_SESSION['email'];
$p_email = $_SESSION['email'];
//$where = " And school_id = '" . $_SESSION['institute_id'] . "' ";
if($_SESSION['role_id'] == 1 ) {
    $where .= " ";
    $data = $cobj->fetch_all($where);
}
if($_SESSION['role_id'] == 2 ) {
    $where .= " And STM.p_email = '".$_SESSION['email']."' ";
    $data = $cobj->fetch_assignment_list($where);
}
else{
    $where .= "";
    $data = $cobj->fetch_all($where);
}




$grade_list = FillCombo($grade_obj->fetch_grade_list(), "id", "standard_name", "");
$course_list = FillCombo($course_obj->fetch_course_list(), "id", "course_name", "");
$section_list = FillCombo($section_obj->fetch_section_list(), "id", "section_name", "");
$emp_list = FillCombo($emp_obj->fetch_Employee_list(),"id", "fname", "");
?>
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box red ">
                        <div class="portlet-title">
                            <div class="caption">
                                Search
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmSearch" id="frmSearch">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Standard<span class="required" aria-required="true">
                                             * </span></label>
                                        <div class="col-md-9"><select name="standard_id" id="standard_id" class="form-control  required">
                                                <?php echo $grade_list; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Section<span class="required" aria-required="true">
                                             * </span></label>
                                        <div class="col-md-9"><select name="section_id" id="section_id" class="form-control  required">
                                                <?php echo $section_list; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Medium<span class="required" aria-required="true">
                                             * </span></label>
                                        <div class="col-md-9">
                                            <select id="drpsearch_medium" class="form-control required" name="drpsearch_medium" >
                                                <option value="">---Select---</option>
                                                <option value="English">English</option>
                                                <option value="Gujarati">Gujarati</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Course<span class="required" aria-required="true">
                                             * </span></label>
                                        <div class="col-md-9"><select name="course_id" id="course_id" class="form-control  required">
                                                <?php echo $course_list; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Assignment date<span class="required" aria-required="true">
                                             * </span></label>
                                        <div class="col-md-9">
                                            <input id="txtdate" name="txtdate" readonly="" class="form-control required date-picker" placeholder="YYYY-MM-DD">
                                        </div>
                                    </div>

                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn green" name="btnSearch" id="btnSearch" type="submit" onclick="return Search_Data();"><i class="fa fa-check"></i> Search</button>
                                            <button type="reset" type="reset" id="btnReset" class="btn default">Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN PAGE HEADER-->
                        <h3 class="page-title">
                            Manage Assignments <small>view assignments</small>
                        </h3>
                        <div class="row">
                            <form id="frmtable" name="frm" method="post"  enctype="multipart/form-data" >
                                <div class="col-md-12">

                                    <br>

                                    <!-- BEGIN BORDERED TABLE PORTLET-->
                                    <div class="portlet box red">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-cogs"></i>Manage Assignments
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div id="page_listing">
                                                <table class="table table-bordered table-striped table-condensed flip-content" >
                                                    <thead>
                                                    <tr>
                                                        <th class="chb_col" width="1" >
                                                            <input type="checkbox" name="checkall" onclick="javascript:checkAlluncheckAll(frm);">
                                                        </th>
                                                        <th>
                                                            Standard
                                                        </th >
                                                        <th>
                                                            Section
                                                        </th >
                                                        <th>
                                                            Medium
                                                        </th >
                                                        <th>
                                                            Course
                                                        </th >
                                                        <th>
                                                            Emp Name
                                                        </th >
                                                        <th>
                                                            Assignment Last Date
                                                        </th >
                                                        <th>
                                                            File Download Link
                                                        </th>
                                                        <th class="center" style="text-align: center;">
                                                            Status
                                                        </th>
                                                        <th width="10%" style="text-align: center;">
                                                            Action
                                                        </th>
                                                    </tr>
                                                    </thead>

                                                    <tbody id="content ui-sortable">

                                                    <?php
                                                    if (count($data) == 0) {
                                                        ?>
                                                        <tr><td colspan="8" align="center">No Result found</td></tr>
                                                        <?php
                                                    } else {
                                                        foreach ($data as $val) {


                                                                $einfo = $emp_obj->getEmployee($val->emp_id);
                                                                $emp_id = $einfo->fname." ".$einfo->lname;

                                                            ?>
                                                            <tr>
                                                                <td class="chb_col">
                                                                    <input type="checkbox" name="id[]" value="<?php echo $val->id; ?>">
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->standard_name;?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->section_name;?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->medium;?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->course_name;?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $emp_id;?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->last_date;?>
                                                                </td>

                                                                <td >
                                                                    <a href="<?php echo ASSIGNMENT_URL. $val->file_name; ?>">Viwe File</a>
                                                                </td>

                                                                <td id="td_status_<?php echo $val->id; ?>" style="text-align: center;">

                                                                    <a href="javascript:;" onclick="ChangeStatus('<?php echo $val->status; ?>', '<?php echo $val->id; ?>')"

                                                                       title='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>'>

                                                                        <img  src='<?php echo IMAGE_URL; ?><?php
                                                                        echo ($val->status == 1 ? "ico-1.gif" : "ico-0.gif");
                                                                        ?>

                                                                              ' alt='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>'

                                                                              title='<?php echo ($val->status == 1 ? "Active" : "Inactive"); ?>' />

                                                                    </a>

                                                                </td>

                                                                <td class="content_actions" style="text-align: center;">
                                                                    <a href="javascript:;" onclick="javascript:RemoveRecord('<?php echo $val->id; ?>')" title='Delete'>
                                                                        <img src="<?php echo IMAGE_URL; ?>ico/trashcan_gray.png" alt="" /></a> <a href="assignment-edit.php?id=<?php echo $val->id; ?>" class="sepV_a" title="Edit">
                                                                        <img src="<?php echo IMAGE_URL; ?>ico/pencil_gray.png" alt="" />
                                                                    </a>

                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                                <?php
                                                include("pagination.php"); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END BORDERED TABLE PORTLET-->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
    </div>

    <?php
    include("footer.php");
    ?>
    <script src="<?php echo JS_URL; ?>script/assignment.js" type="text/javascript" charset="utf-8"></script>