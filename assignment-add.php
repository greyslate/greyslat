<?php
include("header.php");
$val = $assign_obj->getAssignment(1);
//$val = $holiday_obj->getHoliday(1);
$grade_list = FillCombo($grade_obj->fetch_grade_list(), "id", "standard_name", "");
$course_list = FillCombo($course_obj->fetch_course_list(), "id", "course_name", "");
$section_list = FillCombo($section_obj->fetch_section_list(), "id", "section_name", "");
$emp_list = FillCombo($emp_obj->fetch_Employee_list(),"id", "fname", "");
?>
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Assignment List <small> upload assignment list file</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo SITE_URL; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="assignment-list.php">Assignment List</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="portlet box red ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Upload Assignment List
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmedit" id="frmedit">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                You have some form errors. Please check below.
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Standard <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9"><select name="standard_id" id="standard_id" class="form-control  required">
                                                    <?php echo $grade_list; ?>
                                                </select>
                                            </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Section <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9"><select name="section_id" id="section_id" class="form-control  required">
                                                        <?php echo $section_list; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Medium <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input  type="text" id="txtMedium" name="txtMedium" placeholder="Medium" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Course <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9"><select name="course_id" id="course_id" class="form-control  required">
                                                    <?php echo $course_list; ?>
                                                </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Reporting Emp ID <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <select class="form-control required" name="emp_id" id="emp_id">
                                                        <?php echo $emp_list; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Upload File<span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input type="file" id="file_name" name="file_name" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Assignment Last date <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input id="txtdate" name="txtdate" readonly="" class="form-control required date-picker" placeholder="YYYY-MM-DD">
                                                </div>
                                            </div>

                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <input type="hidden" name="action" value="add">
                                                        <input type="hidden" name="id" value="1" />
                                                        <input type="hidden" name="oimg" value="<?php echo $val->file_name; ?>" />
                                                        <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>
<?php
include("footer.php");
?>
<link rel="stylesheet" href="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.css">
<script src="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.js"></script>
<script src="<?php echo JS_URL; ?>script/assignment.js" type="text/javascript" charset="utf-8"></script>
