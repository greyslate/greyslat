<?php
include("header.php");

if(!isset($_GET['id'])){
    header("location: student-list.php");
    exit(0);
}

$id= $_GET['id'];
$val = $student_obj->getStudentDetail($id);

$grade_list = FillCombo($grade_obj->fetch_grade_list(), "id", "standard_name", "");
$course_list = FillCombo($course_obj->fetch_course_list(), "id", "course_name", "");
$section_list = FillCombo($section_obj->fetch_section_list(), "id", "section_name", "");

//$data = $cobj->fetch_all($where);
?>

<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                       View Student Detail <small> View Student Detail</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo SITE_URL; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="student-list.php">Poll</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">View Student Details</a>
                            </li>
                        </ul>
                    </div>
                     <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Manage Student <small>view student details</small>
                    </h3>
                    <div class="row">
                        <div class="portlet box red">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cogs"></i>Manage Student
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-bordered table-hover">
                                        <tbody>
                                         <tr>
                                                <td align="left" width="15%">
                                                    Roll No
                                                </td>
                                                <td align="left">
                                                    <?php echo $val->roll_no; ?>
                                                </td>
                                            </tr>
                                        <tr>
                                                <td align="left" width="15%">
                                                   Profile Pic
                                                </td>
                                                <td align="left">
                                                   <?php 
                                                                        if(!empty($val->profile_pic)){
                                                                            ?>
                                                                            <img src="<?php echo STUDENT_URL.$val->profile_pic; ?>" width="100" />
                                                                            <?php
                                                                        }
                                                                        else{
                                                                            echo "Picture Not Avaialble";
                                                                        }
                                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="15%">
                                                    Student Name
                                                </td>
                                                <td align="left">
                                                    <?php echo $val->fname . " " . $val->lname; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="15%">
                                                    Gender
                                                </td>
                                                <td align="left">
                                                    <?php echo $val->gender; ?>
                                                </td>
                                            </tr> 
                                            <tr>
                                                <td align="left" width="15%">
                                                    DOB
                                                </td>
                                                <td align="left">
                                                    <?php echo $val->dob; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="15%">
                                                    Email
                                                </td>
                                                <td align="left">
                                                    <?php echo $val->p_email; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="15%">
                                                    Address
                                                </td>
                                                <td align="left">
                                                    <?php echo $val->address; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="15%">
                                                    Contact
                                                </td>
                                                <td align="left">
                                                    <?php echo $val->p_contact; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="15%">
                                                    Course
                                                </td>
                                                <td align="left">
                                                    <?php echo $val->course_id; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="15%">
                                                    Grade
                                                </td>
                                                <td align="left">
                                                    <?php echo $val->grade_id; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="15%">
                                                    Section
                                                </td>
                                                <td align="left">
                                                    <?php echo $val->section_id; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="15%">
                                                    Parent
                                                </td>
                                                <td align="left">
                                                    <?php echo $val->parent_id; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="15%">
                                                    Created On
                                                </td>
                                                <td align="left">
                                                    <?php echo $val->created_on; ?>
                                                </td>
                                            </tr> 
                                            <tr>
                                                <td>
                                                    <button type="button" onclick="location.href = 'student-list.php'" class="btn default">Go Back</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>

</div>

<?php
include("footer.php");
?>    