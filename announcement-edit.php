<?php
include("header.php");

if (!isset($_GET['id'])) {
    header("location: announcement-list.php");
    exit(0);
}

$id = $_GET['id'];
$val = $ann_obj->getAnnouncement($id);

$section_arr = explode(",", $val->section_id);

$grade_list = FillCombo($grade_obj->fetch_grade_list(), "id", "standard_name", $val->grade_id);
$course_list = FillCombo($course_obj->fetch_course_list(), "id", "course_name", $val->course_id);
$section_list = FillMultiCombo($section_obj->fetch_section_list(), "id", "section_name", $section_arr);
?>

<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Edit Announcement <small> update announcement title, date & description</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo SITE_URL; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="announcement-list.php">Announcement</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Edit Announcement</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="portlet box red ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Edit Announcement
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmadd" id="frmadd">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                You have some form errors. Please check below.
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Title <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input value="<?php echo $val->title; ?>" type="text" id="txttitle" name="txttitle" placeholder="Announcement Title" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Description <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <textarea id="txtdesc" name="txtdesc" class="form-control required" placeholder="Description"><?php echo $val->description; ?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Date <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input id="txtdate" name="txtdate" value="<?php echo $val->ann_date; ?>" readonly="" class="form-control required date-picker" placeholder="YYYY-MM-DD">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Course Name <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <select name="course_id" id="course_id" class="form-control required">
                                                        <?php echo $course_list; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Grade/Standard <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <select name="grade_id" id="grade_id" class="form-control required">
                                                        <?php echo $grade_list; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Section <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <select multiple="" name="section_id[]" id="section_id" class="form-control required select2">
                                                        <?php echo $section_list; ?>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input type="hidden" name="action" value="edit">
                                                    <input type="hidden" name="id" value="<?php echo $val->id; ?>">
                                                    <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
                                                    <button type="button" onclick="location.href = 'announcement-list.php'" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>
<?php
include("footer.php");
?>
<script src="<?php echo JS_URL; ?>script/announcement.js" type="text/javascript" charset="utf-8"></script>