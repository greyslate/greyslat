<?php

    include("includes/config-frontend.php");
    $objPHPExcel = new PHPExcel();
    $sql = "Select * From ".NOTICE_MASTER." order by id desc";
    $db->query($sql);

    $rows = $db->fetch_object();

    $i = 0;
    $main_arr = array();
    if(count($rows) > 0){
        foreach($rows as $val){
            $main_arr[$i]["notice"]     = $val->notice_title;
            $main_arr[$i]["desc"]       = $val->notice_desc;
            $main_arr[$i]["date"]     = $val->notice_date;
            $main_arr[$i]["location"]     = $val->notice_location;
            $main_arr[$i]["created_on"] = $val->created_on;
            $main_arr[$i]["updated_on"] = $val->updated_on;
            $i++;
        }
    }


    $rowCount = 1;
    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "Notice Title");
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Notice Description");
    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Notice Date");
    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, "Notice Location");
    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, "Created On");
    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "Updated On");


    $rowCount = 2;
    for($i=0;$i<count($main_arr);$i++){
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $main_arr[$i]['notice']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $main_arr[$i]['desc']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $main_arr[$i]['date']);

        

        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $main_arr[$i]['location']);
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $main_arr[$i]['created_on']);
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $main_arr[$i]['updated_on']);

        $rowCount++;
        //pr($objPHPExcel); 
    }

    header('Content-Type: application/vnd.openxmlformats-   officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.date("d-m-Y").'-notice.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

?>