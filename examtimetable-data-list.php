<?php
include("header.php");

if (!isset($_GET['id'])) {
    header("location: timetable-list.php");
    exit(0);
}
$id = $_GET['id'];
//$val = $timetabledata_obj->getTimeTableData($tt_id);

$cobj = new ExamTimeTableData();
$where = "And et_id = '".$id."'  ";
$data = $cobj->fetch_all($where);
$grade_list = FillCombo($grade_obj->fetch_grade_list(), "id", "standard_name", "");
/*$course_list = FillCombo($course_obj->fetch_course_list(), "id", "course_name", "");
$section_list = FillCombo($section_obj->fetch_section_list(), "id", "section_name", "");
$emp_list = FillCombo($emp_obj->fetch_Employee_list(),"id", "fname", "");*/
?>
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">

                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN PAGE HEADER-->
                        <h3 class="page-title">
                            View Exam Timetable
                        </h3>
                        <div class="row">
                            <form id="frmtable" name="frm" method="post"  enctype="multipart/form-data" >
                                <div class="col-md-12">

                                    <br><br>

                                    <!-- BEGIN BORDERED TABLE PORTLET-->
                                    <div class="portlet box red">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-cogs"></i>Manage Exam Timetable
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div id="page_listing">
                                                <table class="table table-bordered table-striped table-condensed flip-content" >
                                                    <thead>
                                                    <tr>
                                                        <th class="chb_col" width="1%" >
                                                            <input type="checkbox" name="checkall" onclick="javascript:checkAlluncheckAll(frm);">
                                                        </th>
                                                        <th width="20%">
                                                            Exam Title
                                                        </th>
                                                        <th>
                                                            Standard
                                                        </th>

                                                        <th>
                                                            Medium
                                                        </th>

                                                        <th>
                                                            Exam Date
                                                        </th>
                                                        <th>
                                                            Start Time
                                                        </th>
                                                        <th>
                                                            End Time
                                                        </th>


                                                    </tr>
                                                    </thead>

                                                    <tbody id="content ui-sortable">

                                                    <?php
                                                    if (count($data) == 0) {
                                                        ?>
                                                        <tr><td colspan="8" align="center">No Result found</td></tr>
                                                        <?php
                                                    } else {
                                                        foreach ($data as $val) {
                                                            ?>
                                                            <tr>
                                                                <td class="chb_col">
                                                                    <input type="checkbox" name="id[]" value="<?php echo $val->id; ?>">
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->exam_title; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->standard_name; ?>
                                                                </td>

                                                                <td>
                                                                    <?php echo $val->medium; ?>
                                                                </td>

                                                                <td>
                                                                    <?php echo $val->exam_date; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->start_time; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $val->end_time; ?>
                                                                </td>



                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                                <?php include("pagination.php"); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END BORDERED TABLE PORTLET-->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
    </div>

    <?php
    include("footer.php");
    ?>
    <script src="<?php echo JS_URL; ?>script/examtimetable.js" type="text/javascript" charset="utf-8"></script>