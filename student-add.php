<?php
include("header.php");

$parent_list = FillCombo($parent_obj->fetch_parent_list(), "id", "fname", "");

$grade_list = FillCombo($grade_obj->fetch_grade_list(), "id", "standard_name", "");
$course_list = FillCombo($course_obj->fetch_course_list(), "id", "course_name", "");
$section_list = FillCombo($section_obj->fetch_section_list(), "id", "section_name", "");

?>


<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Add Student <small> add student basic & personal information</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo SITE_URL; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="student-list.php">Student</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Add Student</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Add Student
                                    </div>
                                    <div class="tools">
                                        (*) required fields.
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="horizontal-form" method="post" name="frmadd" id="frmadd" enctype="multipart/form-data">
                                        <div class="form-body">
                                            <h3 class="form-section">Person Info</h3>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Roll No <span class="required" aria-required="true">* </span></label>
                                                        <input type="text" name="txtrollno" id="txtrollno" class="form-control" placeholder="Roll No">

                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">First Name <span class="required" aria-required="true">* </span></label>
                                                        <input type="text" name="txtfname" id="txtfname" class="form-control" placeholder="First Name">

                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            Middle Name
                                                            <span class="required" aria-required="true">* </span>
                                                        </label>
                                                        <input type="text" id="txtmname" name="txtmname" class="form-control" placeholder="Middle Name">

                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                                <!--/span-->
                                                <div class="row">
                                                 <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Last Name <span class="required" aria-required="true">* </span></label>
                                                        <input type="text" id="txtlname" name="txtlname" class="form-control" placeholder="Last Name">
                                                    </div>
                                                </div>
                                                 <!--/span-->



                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Parent <span class="required" aria-required="true">*</span></label>
                                                        <input type="text" id="parent_id" name="parent_id" class="form-control" placeholder="Parent Name">
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Email <span class="required" aria-required="true">* </span></label>
                                                        <input type="email" id="txtpemail" name="txtpemail" class="form-control required" placeholder="Email">
                                                    </div>
                                                </div>

                                                </div>

                                                <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Phone <span class="required" aria-required="true">* </span></label>
                                                        <input type="text" id="txtphone1" name="txtphone1" class="form-control required" placeholder="Phone No">
                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Gender</label>
                                                        <select class="form-control" name="gender" id="gender">
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Date of Birth <span class="required" aria-required="true">* </span></label>
                                                        <input type="text" id="txtdob" name="txtdob" readonly="" class="form-control date-picker required" placeholder="dd/mm/yyyy">
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Profile Pic</label>
                                                        <input type="file" id="file_name" name="file_name" class="form-control" placeholder="Upload Pic">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->

                                            <!--/row-->
                                            <h3 class="form-section">Other Information</h3>
                                            <div class="row">
                                                <div class="col-md-12 ">
                                                    <div class="form-group">
                                                        <label class="control-label">Address <span class="required" aria-required="true">*</span></label>
                                                        <input type="text" class="form-control required" name="txtaddress" id="txtaddress">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Course <span class="required" aria-required="true">*</span></label>
                                                        <select class="form-control required" name="course_id" id="course_id">
                                                            <?php echo $course_list; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Grade <span class="required" aria-required="true">*</span></label>
                                                        <select class="form-control required" name="grade_id" id="grade_id">
                                                            <?php echo $grade_list; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Section <span class="required" aria-required="true">*</span></label>
                                                        <select class="form-control required" name="section_id" id="section_id">
                                                            <?php echo $section_list; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="form-actions right">
                                            <input type="hidden" name="action" value="add">
                                            <button type="button" onclick="location.href = 'student-list.php'" class="btn default">Cancel</button>
                                            <button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
                                        </div>

                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>


<?php
include("footer.php");
?>
<script src="<?php echo JS_URL; ?>script/student.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-1.8.0.min.js"></script>
<script type="text/javascript">
$(function(){
$(".parent_id").keyup(function()
{
    var search_keyword_value = $(this).val();
    var dataString = 'parent_id='+ search_keyword_value;
    if(search_keyword_value!='')
    {
        $.ajax({
            type: "POST",
            //url: "search.php",
            data: dataString,
            cache: false,
            success: function(html)
                {
                    $("#result").html(html).show();
                }
        });
    }
    return false;
});

$("#result").live("click",function(e){
    var $clicked = $(e.target);
    var $name = $clicked.find('.fname').html();
    var decoded = $("<div/>").html($name).text();
    $('#parent_id').val(decoded);
});

$(document).live("click", function(e) {
    var $clicked = $(e.target);
    if (! $clicked.hasClass("parent_id")){
        $("#result").fadeOut();
    }
});

$('#parent_id').click(function(){
    $("#result").fadeIn();
});
});
</script>
