<?php
include("header.php");
?>

<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Add CMS
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo SITE_URL; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="cms.php">CMS</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Add CMS</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="portlet box red ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Add CMS
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmadd" id="frmadd">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                You have some form errors. Please check below.
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Name <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtcmstitle" name="name" placeholder="CMS Title" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Alias <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtcmsalias" name="alias" placeholder="CMS Alias" class="form-control required"/>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Content <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <textarea id="txtcmscontent" name="content" class="form-control required" placeholder="Content"></textarea>
                                                </div>
                                            </div>
                                        
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input type="hidden" name="action" value="add">
                                                    <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
                                                    <button type="button" onclick="location.href = 'cms.php'" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>

<?php
include("footer.php");
?>
<script src="<?php echo JS_URL; ?>script/cms.js" type="text/javascript" charset="utf-8"></script>

