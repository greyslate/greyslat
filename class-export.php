<?php

    include("includes/config-frontend.php");
    $objPHPExcel = new PHPExcel();
   

    $sql =  "Select * From " . CLASS_DETAIL . " As cd 
                  INNER JOIN  " . GRADE_MASTER . " As GM on GM.id = cd.grade_id 
                  INNER JOIN  " . SECTION_MASTER . " As SM on SM.id = cd.section_id
                  INNER JOIN ".CLASS_MASTER." AS CM on CM.id = cd.class_id
                  
                  order by cd.id desc";


    $db->query($sql);

    $rows = $db->fetch_object();

    $i = 0;
    $main_arr = array();
    if(count($rows) > 0){
        foreach($rows as $val){
            
            

            $main_arr[$i]["class"]     = $val->class_name;
           // $main_arr[$i]["course"]     = $val->course_name;
            
            $main_arr[$i]["grade"]    = $val->standard_name." ".$val->section_name;
            
            $main_arr[$i]["created_on"] = $val->created_on;
            $i++;
        }
    }


    $rowCount = 1;
    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "Class Name");
   // $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Course Name");
    
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Section with Grade");

    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Created On");


    $rowCount = 2;
    for($i=0;$i<count($main_arr);$i++){
       $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $main_arr[$i]['class']);
      //  $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $main_arr[$i]['course']);
     
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $main_arr[$i]['grade']);

        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $main_arr[$i]['created_on']);

        $rowCount++;
        //pr($objPHPExcel); 
    }

    header('Content-Type: application/vnd.openxmlformats-   officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.date("d-m-Y").'-class.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

?>