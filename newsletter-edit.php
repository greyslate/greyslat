<?php
include("header.php");

if(!isset($_GET['id'])){
    header("location: newsletter-list.php");
    exit(0);
}

$id = $_GET['id'];
$cobj = new Newsletter();

$crow = $cobj->getNewsletter($id);


?>

<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Edit Newsletter <small> edit Newsletter title, description</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo SITE_URL; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="newsletter-list.php">Newsletter</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Edit Newsletter</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="portlet box red ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Edit Newsletter
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmedit" id="frmedit">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                You have some form errors. Please check below.
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Newsletter Title <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <input value="<?php echo $crow->title;?>" type="text" id="txtnewslettertitle" name="txtnewslettertitle" placeholder="Newsletter Title" class="form-control required"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label col-md-3">Description <span class="required" aria-required="true">* </span></label>
                                                <div class="col-md-9">
                                                    <textarea id="txtneswletterdesc" name="txtneswletterdesc" class="form-control required" placeholder="Description"><?php echo $crow->description;?></textarea>
                                                </div>
                                            </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input type="hidden" name="action" value="edit">
                                                     <input type="hidden" name="id" value="<?php echo $val->id; ?>">
                                                    <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
                                                    <button type="button" onclick="location.href = 'newsletter-list.php'" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>

<?php
include("footer.php");
?>
<script src="<?php echo JS_URL; ?>script/newsletter.js" type="text/javascript" charset="utf-8"></script>
