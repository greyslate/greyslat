<?php
include("header.php");

if(!isset($_GET['id'])){
    header("location: poll-list.php");
    exit(0);
}

$poll_id= $_GET['id'];


$pollinfo = $poll_obj->getPoll($poll_id);

$poll_title = $pollinfo->question_title;


$sql = "Select poll_detail_id,count(*) As counter "
        . "From poll_report "
        . "where poll_id = '".$poll_id."' group by poll_detail_id order by poll_detail_id";

$query = $db->query($sql);

$rows = $db->fetch_object();

$total_count = count($rows);

$str = '';
if($total_count > 0){
    foreach($rows as $tval){
        $total = $tval->counter;
        $p_o_id = $tval->poll_detail_id;
        
        $pinfo = $poll_obj->getPollDetailInfo($p_o_id);
        
        $main_per = ($total*100)/$total_count;
        
        if(count($pinfo) > 0){
            $str .= '["'.$pinfo->option_title.'", '.$main_per.'],'; 
        }
               
    }
}


?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1.1", {packages:["bar"]});
      google.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
          ['Options', 'Percentage'],
          <?php echo $str; ?>
        ]);

        var options = {
          title: '<?php echo $poll_title; ?>',
          width: '100%',
          legend: { position: 'none' },
          chart: { title: '<?php echo $poll_title; ?>',
                   subtitle: 'Result based on percentage' },
          bars: 'horizontal', // Required for Material Bar Charts.
          axes: {
            x: {
              0: { side: 'top', label: 'Percentage'} // Top x-axis.
            }
          },
          bar: { groupWidth: "90%" }
        };

        var chart = new google.charts.Bar(document.getElementById('top_x_div'));
        chart.draw(data, options);
      };
    </script>
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                       View Poll Report <small> View Poll information</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo SITE_URL; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="poll-list.php">Poll</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">View Poll Report</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        View Poll Report
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmadd" id="frmadd" enctype="multipart/form-data">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                You have some form errors. Please check below.
                                            </div>
                                          <div class="form-group">
                                            
                                             <?php
                                                if($total_count > 0){
                                            ?>
                                            <div id="top_x_div" style="width: 100%; text-align: center; height: 500px;"></div>
                                            <?php
                                                }
                                                else {
                                                    echo "No Poll Result.";
                                                }
                                            ?>
                                            </div>
                                        </div>
                                    
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="action" value="add">
                                                <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
                                                <button type="button" onclick="location.href = 'poll-list.php'" class="btn default">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                               
                             <!-- END FORM-->
                             </form>
                                </div>
                            
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>

<?php
include("footer.php");
?>
<script src="<?php echo JS_URL; ?>script/poll.js" type="text/javascript" charset="utf-8"></script>
