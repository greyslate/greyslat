<?php

    include("includes/config-frontend.php");
    $objPHPExcel = new PHPExcel();
    
    $sql = "Select * From ".EMP_MASTER." order by id desc";

    $db->query($sql);

    $rows = $db->fetch_object();

    $i = 0;
    $main_arr = array();
    if(count($rows) > 0){
        foreach($rows as $val){
            $main_arr[$i]["school_name"]     = $val->school_name;
            $main_arr[$i]["email"]     = $val->email;
            $main_arr[$i]["phone"]    = $val->phone;
            $main_arr[$i]["address"]    = $val->address;
            $main_arr[$i]["city"]    = $val->city;
            $main_arr[$i]["state"]    = $val->state;
            $main_arr[$i]["teacher_name"]     = $val->fname. " " .$val->lname;
            $main_arr[$i]["subject"]    = $val->subject_name;
            $main_arr[$i]["standad"]    = $val->standard_name;
            $main_arr[$i]["created_on"] = $val->created_on;
            $i++;
        }
    }


    $rowCount = 1;
    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "School Name");
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Email");
    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Phone");
    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, "Address");
    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, "City");
    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "State");
    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, "Teacher Name");
    $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, "Subject");
    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, "Standard");
    $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, "Created On");


    $rowCount = 2;
    for($i=0;$i<count($main_arr);$i++){
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $main_arr[$i]['school_name']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $main_arr[$i]['email']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $main_arr[$i]['phone']);
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $main_arr[$i]["address"]);
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $main_arr[$i]["city"]);
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount,  $main_arr[$i]["state"]);
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $main_arr[$i]["teacher_name"]);
        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $main_arr[$i]["subject"] );
        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $main_arr[$i]["standad"]);
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $main_arr[$i]['created_on']);

        $rowCount++;
        //pr($objPHPExcel); 
    }

    header('Content-Type: application/vnd.openxmlformats-   officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.date("d-m-Y").'-emp.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

?>