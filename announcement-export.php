<?php

    include("includes/config-frontend.php");
    $objPHPExcel = new PHPExcel();
    //--- Fetch All Grade
    $secarr = array();
    $sectionlist = $section_obj->fetch_section_list();

    if(count($sectionlist) > 0){
        foreach($sectionlist as $gval){
            $secarr[$gval->id] = $gval->section_name;
        }
    }


    $sql = "Select * From ".ANNOUNCEMENT_MASTER." AS AM
                INNER JOIN ".COURSE_MASTER." AS CM on CM.id = AM.course_id
                INNER JOIN ".STANDARD_MASTER." AS SM on SM.id = AM.grade_id
                order by AM.id desc";


    $db->query($sql);

    $rows = $db->fetch_object();

    $i = 0;
    $main_arr = array();
    if(count($rows) > 0){
        foreach($rows as $val){

            $slist = explode(",",$val->section_id);
            $section_arr = array();
            foreach($slist as $vals){
                $section_arr[] = isset($secarr[$vals]) ? $secarr[$vals] : "-";
            }

            $section = implode(",", $section_arr);

            $main_arr[$i]["course"]     = $val->course_name;
            $main_arr[$i]["grade"]      = $val->standard_name;
            $main_arr[$i]["section"]    = $section;
            $main_arr[$i]["title"]      = $val->title;
            $main_arr[$i]["desc"]       = $val->description;
            $main_arr[$i]["ann_date"]   = $val->ann_date;
            $main_arr[$i]["created_on"] = $val->created_on;
            $i++;
        }
    }


    $rowCount = 1;
    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "Course Name");
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Grade");
    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Section");

    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, "Title");

    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, "Description");
    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "Announcement Date");
    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, "Created On");


    $rowCount = 2;
    for($i=0;$i<count($main_arr);$i++){
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $main_arr[$i]['course']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $main_arr[$i]['grade']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $main_arr[$i]['section']);

        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $main_arr[$i]['title']);

        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $main_arr[$i]['desc']);
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $main_arr[$i]['ann_date']);
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $main_arr[$i]['created_on']);

        $rowCount++;
        //pr($objPHPExcel); 
    }

    header('Content-Type: application/vnd.openxmlformats-   officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.date("d-m-Y").'-announcement.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

?>