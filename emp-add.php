<?php
include("header.php");

$country_list = FillCombo($country_obj->fetch_country_list(), "id", "countrry_name", "");
//$city_list = FillCombo($city_obj->fetch_city_list(), "id", "city_name", "");

?>
<!-- include js and css -->
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Manage Teacher <small>teacher basic & personal information</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo SITE_URL; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="emp-list.php">Teacher</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Add Teacher</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="portlet box red ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Add Teacher
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal form-bordered form-label-stripped" method="post" name="frmadd" id="frmadd">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                You have some form errors. Please check below.
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">School Name <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtschoolname" name="txtschoolname" placeholder="School Name" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Email <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="email" id="txtemail" name="txtemail" placeholder="abc@example.com" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Password <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="password" id="txtpassword" name="txtpassword" placeholder="Password" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Re-type Password <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="password" id="txtcpassword" name="txtcpassword" placeholder="Re-type Password" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Phone No </label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtphone" name="txtphone" placeholder="Enter 10 digit" class="form-control"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Address <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                  <textarea name="txtaddress" id="txtaddress" class="form-control required" placeholder="Address"></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">City <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="city" name="city" placeholder="city" class="form-control required"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">State<span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="state" name="state" placeholder="state" class="form-control required"/>
                                                </div>
                                            </div>

                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3">First Name <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtfname" name="txtfname" placeholder="First Name" class="form-control required"/>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Last Name <span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtlname" name="txtlname" placeholder="Last Name" class="form-control required"/>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Subject<span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtsubjectname" name="txtsubjectname" placeholder="Division" class="form-control"/>
                                                </div>
                                            </div>

                                             <div class="form-group">
                                                <label class="control-label col-md-3">Standard<span class="required" aria-required="true">
                                                        * </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" id="txtstandardname" name="txtstandardname" placeholder="Standard" class="form-control"/>
                                                </div>
                                            </div>
                                             
                                             <div class="form-group">
                                                    <label class="control-label col-md-3">Profile Pic<span class="required" aria-required="true">
                                                        * </span></label>
                                                        <div class="col-md-9">
                                                            <input type="file" id="profile_pic" name="profile_pic" class="form-control required" placeholder="Upload only png jpeg file">
                                                        </div>
                                             </div>
                                         </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input type="hidden" name="action" value="add">
                                                    <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
                                                    <button type="button" onclick="location.href='emp-list.php'" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>
<?php
include("footer.php");
?>

<script src="<?php echo JS_URL; ?>script/emp.js" type="text/javascript" charset="utf-8"></script>
